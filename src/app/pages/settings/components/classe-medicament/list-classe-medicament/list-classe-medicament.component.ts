import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { ClasseMedicament } from 'src/app/core/models/classe-medicament';

@Component({
  selector: 'app-list-classe-medicament',
  templateUrl: './list-classe-medicament.component.html',
  styleUrls: ['./list-classe-medicament.component.scss']
})
export class ListClasseMedicamentComponent implements OnInit {

  faTrashAlt = faTrashAlt;

  @Input() classeMedicaments: ClasseMedicament[]/* | string*/;
  @Input() inputSearchText: string;

  @Output('editClasseMedicaEmitter') editClasseMedicaEmitter = new EventEmitter<ClasseMedicament>();
  @Output('deleteClasseMedicaEmitter') deleteClasseMedicaEmitter = new EventEmitter<ClasseMedicament>();


  constructor() { }

  ngOnInit() {
  }

  editClasseMedicament(classeMedicament: ClasseMedicament) {
    this.editClasseMedicaEmitter.emit(classeMedicament);
  }

  deleteClasseMedicament(classeMedicament: ClasseMedicament) {
    this.deleteClasseMedicaEmitter.emit(classeMedicament);
    // this.laboEmitter.emit(labo);
  }

}
