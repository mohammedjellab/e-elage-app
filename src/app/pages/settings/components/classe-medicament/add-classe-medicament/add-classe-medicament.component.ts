import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { ClasseMedicament } from 'src/app/core/models/classe-medicament';

@Component({
  selector: 'app-add-classe-medicament',
  templateUrl: './add-classe-medicament.component.html',
  styleUrls: ['./add-classe-medicament.component.scss']
})
export class AddClasseMedicamentComponent implements OnInit {

  @Output('addClasseMedicaEmitter') addClasseMedicaEmitter = new EventEmitter<ClasseMedicament>();

  faPlus = faPlus;
  faClose = faTimes;

  addClasseMedicamentForm: FormGroup;


  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.addClasseMedicamentForm = this.formBuilder.group({
      libelle: ['', Validators.required],
    });
  }

  add() {
    if (this.addClasseMedicamentForm.invalid) {
      return;
    }
    console.log('Add Classe Medicament Form Value: -------- ', this.addClasseMedicamentForm.value);
    this.addClasseMedicaEmitter.emit(this.addClasseMedicamentForm.value);

  }
}
