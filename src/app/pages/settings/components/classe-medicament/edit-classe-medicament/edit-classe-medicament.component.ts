import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { ClasseMedicament } from 'src/app/core/models/classe-medicament';

@Component({
  selector: 'app-edit-classe-medicament',
  templateUrl: './edit-classe-medicament.component.html',
  styleUrls: ['./edit-classe-medicament.component.scss']
})
export class EditClasseMedicamentComponent implements OnInit {

  @Output('editClasseMedicaEmitter') editClasseMedicaEmitter = new EventEmitter<ClasseMedicament>();


  // This is how to get Data from BsModalRef give the same schema in parentComponent
  initialState = { classeMedicament: {} };

  faPlus = faPlus;
  faClose = faTimes;

  editClasseMedicamentForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    let { classeMedicament }: any = this.initialState;
    this.editClasseMedicamentForm = this.formBuilder.group({
      id: ['', Validators.required],
      libelle: ['', Validators.required]
    });

    this.editClasseMedicamentForm.controls['id'].setValue(classeMedicament.id);
    this.editClasseMedicamentForm.controls['libelle'].setValue(classeMedicament.libelle);
    
    console.log('ModalRef: ', this.bsModalRef);
  }

  edit() {
    if (this.editClasseMedicamentForm.invalid) {
      return;
    }

    console.log('Edit Classe Medicament Form Value: -------- ', this.editClasseMedicamentForm.value);
    this.editClasseMedicaEmitter.emit(this.editClasseMedicamentForm.value);
  }
}
