import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Medicament } from 'src/app/core/models/medicament';

@Component({
  selector: 'app-list-medicament',
  templateUrl: './list-medicament.component.html',
  styleUrls: ['./list-medicament.component.scss']
})
export class ListMedicamentComponent implements OnInit {

  faTrashAlt = faTrashAlt;

  @Input() medicaments: Medicament[] = [];
  @Input() inputSearchText: string;

  @Output('editMediEmitter') editMediEmitter = new EventEmitter<Medicament>();
  @Output('deleteMediEmitter') deleteMediEmitter = new EventEmitter<Medicament>();


  constructor() { }

  ngOnInit() {
    console.log('NgOnInit --------------- List Medicament ----- ', this.medicaments);
  }

  /*ngAfterViewInit() {
    console.log('NgAfterViewInit --------------- List Medicament ----- ', this.medicaments);
  }*/

  editMedi(medi: Medicament) {
    this.editMediEmitter.emit(medi);
  }

  deleteMedi(medi: Medicament) {
    this.deleteMediEmitter.emit(medi);
    // this.mediEmitter.emit(medi);
  }

}
