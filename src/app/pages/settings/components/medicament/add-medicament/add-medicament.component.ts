import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { Dci } from 'src/app/core/models/dci';
import { Medicament } from 'src/app/core/models/medicament';
import * as $ from 'jquery';

@Component({
  selector: 'app-add-medicament',
  templateUrl: './add-medicament.component.html',
  styleUrls: ['./add-medicament.component.scss']
})
export class AddMedicamentComponent implements OnInit {

  @Output('addMediEmitter') addMediEmitter = new EventEmitter<Medicament>();

  initialState = { dcis: [] };

  faPlus = faPlus;
  faClose = faTimes;

  addMedicamentForm: FormGroup;


  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef) { }

  ngOnInit() {
   
    console.log('Initial State: ', this.initialState.dcis);
    this.addMedicamentForm = this.formBuilder.group({
      // Select
      // laboratoire: ['', Validators.required],
      dcis: ['', Validators.required],
      // classeMedicale: ['', Validators.required],
      // famille: ['', Validators.required],
      // Input
      nomCommercial: ['', Validators.required],
      designation: ['', Validators.required],
      posologie: ['', Validators.required],
      forme: ['', Validators.required],
      duree: ['', Validators.required],
      quantite: ['', Validators.required],
      // rembourse: ['', Validators.required],
      dosage: ['', Validators.required],
      presentation: ['', Validators.required],
      consigne: ['', Validators.required],
      prixVente: ['', Validators.required],
      prixAchat: ['', Validators.required],
      codeBarre: ['', Validators.required],
      prixBRemboursementPPM: ['', Validators.required],
      prixHospitalier: ['', Validators.required],
      baseRemboursementPPV: ['', Validators.required],
      prixBaseRemboursement: ['', Validators.required],
      type: ['', Validators.required],
      surdosage: ['', Validators.required],
      codeAtc: ['', Validators.required],
      natureProduit: ['', Validators.required],
      // statut: ['', Validators.required],
      //TextArea
      utilisation: ['', Validators.required],
      indications: ['', Validators.required],
      contreIndications: ['', Validators.required],
      /*effetsIndesirables: ['', Validators.required],
      conditionsPrescription: ['', Validators.required],
      substancePsychoactive: ['', Validators.required],
      risquePotentiel: ['', Validators.required]*/
    });
  }

  add() {
    /*if (this.addMedicamentForm.invalid) {
      return;
    }*/
    console.log('Add Medicament Form Value: -------- ', this.addMedicamentForm.value);
    console.log('DCIS: ----- ', this.initialState.dcis);
    let {dcis} = this.initialState;
    let test = [dcis[0], dcis[1]];
    this.addMedicamentForm.controls['dcis'].patchValue(test);
    //this.addMedicamentForm.controls['dcis'].patchValue(dcis[1]);
    this.addMediEmitter.emit(this.addMedicamentForm.value);

  }

}
