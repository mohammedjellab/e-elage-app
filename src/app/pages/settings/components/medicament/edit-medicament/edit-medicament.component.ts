import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { Medicament } from 'src/app/core/models/medicament';

@Component({
  selector: 'app-edit-medicament',
  templateUrl: './edit-medicament.component.html',
  styleUrls: ['./edit-medicament.component.scss']
})
export class EditMedicamentComponent implements OnInit {

  @Output('editMediEmitter') editMediEmitter = new EventEmitter<Medicament>();


  // This is how to get Data from BsModalRef give the same schema in parentComponent
  initialState = { medicament: {}, dcis: [] };

  faPlus = faPlus;
  faClose = faTimes;

  editMedicamentForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    let { medicament }: any = this.initialState;
    this.editMedicamentForm = this.formBuilder.group({
      id: ['', Validators.required],
      // Select
      // laboratoire: ['', Validators.required],
      dcis: ['', Validators.required],
      // classeMedicale: ['', Validators.required],
      // famille: ['', Validators.required],
      // Input
      nomCommercial: ['', Validators.required],
      designation: ['', Validators.required],
      posologie: ['', Validators.required],
      forme: ['', Validators.required],
      duree: ['', Validators.required],
      quantite: ['', Validators.required],
      // rembourse: ['', Validators.required],
      dosage: ['', Validators.required],
      presentation: ['', Validators.required],
      consigne: ['', Validators.required],
      prixVente: ['', Validators.required],
      prixAchat: ['', Validators.required],
      codeBarre: ['', Validators.required],
      prixBRemboursementPPM: ['', Validators.required],
      prixHospitalier: ['', Validators.required],
      baseRemboursementPPV: ['', Validators.required],
      prixBaseRemboursement: ['', Validators.required],
      type: ['', Validators.required],
      surdosage: ['', Validators.required],
      codeAtc: ['', Validators.required],
      natureProduit: ['', Validators.required],
      // statut: ['', Validators.required],
      //TextArea
      utilisation: ['', Validators.required],
      indications: ['', Validators.required],
      contreIndications: ['', Validators.required],
      /*effetsIndesirables: ['', Validators.required],
      conditionsPrescription: ['', Validators.required],
      substancePsychoactive: ['', Validators.required],
      risquePotentiel: ['', Validators.required]*/
    });

    this.editMedicamentForm.controls['id'].setValue(medicament.id);
    this.editMedicamentForm.controls['designation'].setValue(medicament.designation);
    this.editMedicamentForm.controls['nomCommercial'].setValue(medicament.nomCommercial);
    this.editMedicamentForm.controls['posologie'].setValue(medicament.posologie);
    this.editMedicamentForm.controls['forme'].setValue(medicament.forme);
    this.editMedicamentForm.controls['duree'].setValue(medicament.duree);
    this.editMedicamentForm.controls['quantite'].setValue(medicament.quantite);
    this.editMedicamentForm.controls['dosage'].setValue(medicament.dosage);
    this.editMedicamentForm.controls['presentation'].setValue(medicament.presentation);
    this.editMedicamentForm.controls['consigne'].setValue(medicament.consigne);
    this.editMedicamentForm.controls['prixVente'].setValue(medicament.prixVente);
    this.editMedicamentForm.controls['prixAchat'].setValue(medicament.prixAchat);
    this.editMedicamentForm.controls['codeBarre'].setValue(medicament.codeBarre);
    this.editMedicamentForm.controls['prixBRemboursementPPM'].setValue(medicament.prixBRemboursementPPM);
    this.editMedicamentForm.controls['prixHospitalier'].setValue(medicament.prixHospitalier);
    this.editMedicamentForm.controls['baseRemboursementPPV'].setValue(medicament.baseRemboursementPPV);
    this.editMedicamentForm.controls['type'].setValue(medicament.type);
    this.editMedicamentForm.controls['surdosage'].setValue(medicament.surdosage);
    this.editMedicamentForm.controls['codeAtc'].setValue(medicament.codeAtc);
    this.editMedicamentForm.controls['natureProduit'].setValue(medicament.natureProduit);
    this.editMedicamentForm.controls['utilisation'].setValue(medicament.utilisation);
    this.editMedicamentForm.controls['indications'].setValue(medicament.indications);
    this.editMedicamentForm.controls['contreIndications'].setValue(medicament.contreIndications);
  }

  edit() {
    if (this.editMedicamentForm.invalid) {
      return;
    }
    console.log('Edit Medicament Form Value: -------- ', this.editMedicamentForm.value);
    this.editMediEmitter.emit(this.editMedicamentForm.value);
  }
}
