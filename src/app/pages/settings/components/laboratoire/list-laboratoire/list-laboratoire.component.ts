import { Laboratoire } from 'src/app/core/models/laboratoire';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-laboratoire',
  templateUrl: './list-laboratoire.component.html',
  styleUrls: ['./list-laboratoire.component.scss']
})
export class ListLaboratoireComponent implements OnInit {

  faTrashAlt = faTrashAlt;

  @Input() laboratoires: Laboratoire[]/* | string*/;
  @Input() inputSearchText: string = '';

  @Output('editLaboEmitter') editLaboEmitter = new EventEmitter<Laboratoire>();
  @Output('deleteLaboEmitter') deleteLaboEmitter = new EventEmitter<Laboratoire>();


  constructor() { }

  ngOnInit() {
  }

  editLabo(labo: Laboratoire) {
    this.editLaboEmitter.emit(labo);
  }

  deleteLabo(labo: Laboratoire) {
    this.deleteLaboEmitter.emit(labo);
    // this.laboEmitter.emit(labo);
  }

}
