import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { Laboratoire } from 'src/app/core/models/laboratoire';
import { LaboratoireService } from 'src/app/core/services/laboratoire.service';

@Component({
  selector: 'app-edit-laboratoire',
  templateUrl: './edit-laboratoire.component.html',
  styleUrls: ['./edit-laboratoire.component.scss']
})
export class EditLaboratoireComponent implements OnInit {

  @Output('editLaboEmitter') editLaboEmitter = new EventEmitter<Laboratoire>();
  // @Output('onCloseEditEmitter') onCloseEditEmitter = new EventEmitter<any>();


  // This is how to get Data from BsModalRef give the same schema in parentComponent
  initialState = { labo: {} };

  faPlus = faPlus;
  faClose = faTimes;

  editLaboForm: FormGroup;

  /*typeOfAnalyses = [{ name: "Hémostase", status: false }, { name: "Crèatorrhée", status: false },
  { name: "Cytogénétique", status: false }, { name: "Cytologie B", status: false },
  { name: "Examens sanguins", status: false }, { name: "Examens Urinaires" }];
  types: Array<string> = [];*/

  constructor(private formBuilder: FormBuilder, private laboService: LaboratoireService, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    let { labo }: any = this.initialState;
    this.editLaboForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      tel: ['', Validators.required],
      adresse: ['', Validators.required],
      // types: ['', Validators.required]
    });

    this.editLaboForm.controls['id'].setValue(labo.id);
    this.editLaboForm.controls['name'].setValue(labo.name);
    this.editLaboForm.controls['tel'].setValue(labo.tel);
    this.editLaboForm.controls['adresse'].setValue(labo.adresse);

    console.log('ModalRef: ', this.bsModalRef);
    // console.log('This Labo ------- ', this.initialState);
  }


  edit() {
    if (this.editLaboForm.invalid) {
      return;
    }

    console.log('Edit Labo Form Value: -------- ', this.editLaboForm.value);
    // this.bsModalRef.hide();
    this.editLaboEmitter.emit(this.editLaboForm.value);
  }

  /*close(event) {
    this.bsModalRef.hide();
    // console.log('Close Event on Edit ------ ', event.target.attributes.id);
    // this.onCloseEditEmitter.emit('close');
    // this.onCloseEditEmitter.emit({event: 'close', target: event.target.attributes.id});
    // this.bsModalRef.hide();
  }*/
  
}
