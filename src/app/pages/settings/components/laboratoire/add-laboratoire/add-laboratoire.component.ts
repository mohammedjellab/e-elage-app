import { BsModalRef } from 'ngx-bootstrap';
import { LaboratoireService } from 'src/app/core/services/laboratoire.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Laboratoire } from 'src/app/core/models/laboratoire';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-laboratoire',
  templateUrl: './add-laboratoire.component.html',
  styleUrls: ['./add-laboratoire.component.scss']
})
export class AddLaboratoireComponent implements OnInit {

  @Output('addLaboEmitter') addLaboEmitter = new EventEmitter<Laboratoire>();

  faPlus = faPlus;
  faClose = faTimes;

  addLaboForm: FormGroup;

  /*typeOfAnalyses = [{ name: "Hémostase", status: false }, { name: "Crèatorrhée", status: false },
  { name: "Cytogénétique", status: false }, { name: "Cytologie B", status: false },
  { name: "Examens sanguins", status: false }, { name: "Examens Urinaires" }];
  types: Array<string> = [];*/

  constructor(private formBuilder: FormBuilder, private laboService: LaboratoireService, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.addLaboForm = this.formBuilder.group({
      name: ['', Validators.required],
      tel: ['', Validators.required],
      adresse: ['', Validators.required],
      // types: ['', Validators.required]
    });
  }

  add() {
    if (this.addLaboForm.invalid) {
      return;
    }
    console.log('Add Labo Form Value: -------- ', this.addLaboForm.value);
    this.addLaboEmitter.emit(this.addLaboForm.value);


    /*const labo: Laboratoire = {
      name: this.addLaboForm.controls['name'].value,
      tel: this.addLaboForm.controls['tel'].value,
      adresse: this.addLaboForm.controls['adresse'].value,
      types: [this.addLaboForm.controls['types'].value]
    };

    this.laboService.add(labo).subscribe(res => {
      console.log('Res: ------- ', res);
      this.bsModalRef.hide();
    });*/

  }

  /*close() {
    this.addLaboForm.reset();
    console.log('Close Event on Add ------');
    this.bsModalRef.hide();
  }

  toggle(type, event) {
    console.log('Event -------- ', event);
    event.srcElement.parentElement.classList.toggle('unselect-item');
    console.log('Type: ', type);
    type.status = !type.status;
    this.types.push(type.name);
    this.addLaboForm.controls['types'].setValue(this.types);
    // this.typeOfAnalyses.find(type).status = !type.status;
  }*/
}
