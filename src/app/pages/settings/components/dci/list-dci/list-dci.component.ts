import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Dci } from 'src/app/core/models/dci';

@Component({
  selector: 'app-list-dci',
  templateUrl: './list-dci.component.html',
  styleUrls: ['./list-dci.component.scss']
})
export class ListDciComponent implements OnInit {

  faTrashAlt = faTrashAlt;

  @Input() dcis: Dci[]/* | string*/;
  @Input() inputSearchText: string = '';

  @Output('editDciEmitter') editDciEmitter = new EventEmitter<Dci>();
  @Output('deleteDciEmitter') deleteDciEmitter = new EventEmitter<Dci>();


  constructor() { }

  ngOnInit() {
  }

  editDci(dci: Dci) {
    this.editDciEmitter.emit(dci);
  }

  deleteDci(dci: Dci) {
    this.deleteDciEmitter.emit(dci);
    // this.laboEmitter.emit(labo);
  }

}
