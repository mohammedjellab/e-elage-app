import { Dci } from 'src/app/core/models/dci';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { DciService } from 'src/app/core/services/dci.service';

@Component({
  selector: 'app-edit-dci',
  templateUrl: './edit-dci.component.html',
  styleUrls: ['./edit-dci.component.scss']
})
export class EditDciComponent implements OnInit {

  @Output('editDciEmitter') editDciEmitter = new EventEmitter<Dci>();
  // @Output('onCloseEditEmitter') onCloseEditEmitter = new EventEmitter<any>();


  // This is how to get Data from BsModalRef give the same schema in parentComponent
  initialState = { dci: {} };

  faPlus = faPlus;
  faClose = faTimes;

  editDciForm: FormGroup;

  /*typeOfAnalyses = [{ name: "Hémostase", status: false }, { name: "Crèatorrhée", status: false },
  { name: "Cytogénétique", status: false }, { name: "Cytologie B", status: false },
  { name: "Examens sanguins", status: false }, { name: "Examens Urinaires" }];
  types: Array<string> = [];*/

  constructor(private formBuilder: FormBuilder, private dciService: DciService, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    let { dci }: any = this.initialState;
    this.editDciForm = this.formBuilder.group({
      id: ['', Validators.required],
      libelle: ['', Validators.required]
    });

    this.editDciForm.controls['id'].setValue(dci.id);
    this.editDciForm.controls['libelle'].setValue(dci.libelle);
    
    console.log('ModalRef: ', this.bsModalRef);
    // console.log('This Dci ------- ', this.initialState);
  }


  edit() {
    if (this.editDciForm.invalid) {
      return;
    }

    console.log('Edit Dci Form Value: -------- ', this.editDciForm.value);
    // this.bsModalRef.hide();
    this.editDciEmitter.emit(this.editDciForm.value);
  }

  /*close(event) {
    this.bsModalRef.hide();
    // console.log('Close Event on Edit ------ ', event.target.attributes.id);
    // this.onCloseEditEmitter.emit('close');
    // this.onCloseEditEmitter.emit({event: 'close', target: event.target.attributes.id});
    // this.bsModalRef.hide();
  }*/

}
