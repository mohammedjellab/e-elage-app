import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap';
import { Dci } from 'src/app/core/models/dci';
import { DciService } from 'src/app/core/services/dci.service';

@Component({
  selector: 'app-add-dci',
  templateUrl: './add-dci.component.html',
  styleUrls: ['./add-dci.component.scss']
})
export class AddDciComponent implements OnInit {

  @Output('addDciEmitter') addDciEmitter = new EventEmitter<Dci>();

  faPlus = faPlus;
  faClose = faTimes;

  addDciForm: FormGroup;


  constructor(private formBuilder: FormBuilder, private dciService: DciService, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.addDciForm = this.formBuilder.group({
      libelle: ['', Validators.required],
    });
  }

  add() {
    if (this.addDciForm.invalid) {
      return;
    }
    console.log('Add Dci Form Value: -------- ', this.addDciForm.value);
    this.addDciEmitter.emit(this.addDciForm.value);

  }
}
