import { AddClasseMedicamentComponent } from './../../components/classe-medicament/add-classe-medicament/add-classe-medicament.component';
import { EditClasseMedicamentComponent } from './../../components/classe-medicament/edit-classe-medicament/edit-classe-medicament.component';
import { Component, OnInit } from '@angular/core';
import { ClasseMedicament } from 'src/app/core/models/classe-medicament';
import { ClasseMedicamentService } from 'src/app/core/services/classe-medicament.service';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-classe-medicament',
  templateUrl: './classe-medicament.component.html',
  styleUrls: ['./classe-medicament.component.scss']
})
export class ClasseMedicamentComponent implements OnInit {

  faPlus = faPlus;
  classeMedicaments$: Observable<ClasseMedicament[]>; /* | string>;*/
  bsModalRef: BsModalRef;
  searchText: string;

  constructor(private classeMedicaService: ClasseMedicamentService, private modalService: BsModalService) { }

  ngOnInit() {
    console.log('NgOnInit ----------------- ClasseMedicament');
    this.getClasseMedicas();
  }

  getClasseMedicas() {
    this.classeMedicaments$ = this.classeMedicaService.getAll();
    console.log('ClasseMedicas: ', this.classeMedicaments$);
  }


  openAddClasseMedicaModal() {
    this.bsModalRef = this.modalService.show(AddClasseMedicamentComponent, { class: 'modal-xl' });
    this.bsModalRef.content.addClasseMedicaEmitter.subscribe(res => {
      console.log('Subs Add Classe Medicament Emitter: ', res);
      this.classeMedicaService.add(res).toPromise().then(res => {
        console.log('Add response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getClasseMedicas();
      });
    });
  }

  openEditClasseMedicaModal(params) {
    console.log('Edit ClasseMedica Emitter -------- ', params);
    const initialState: ModalOptions = {
      initialState: {
        classeMedicament: params
      }
    };

    this.bsModalRef = this.modalService.show(EditClasseMedicamentComponent, { initialState, class: 'modal-xl' });
    // this.bsModalRef.content.closeBtnName = 'Fermer';
    this.bsModalRef.content.editClasseMedicaEmitter.subscribe(res => {
      console.log('Subs Edit ClasseMedica Emitter: ', res);
      this.classeMedicaService.update(res).toPromise().then(res => {
        console.log('Edit response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getClasseMedicas();
      });
    });
  }

  deleteClasseMedica(params) {
    console.log('Delete ClasseMedica Emitter -------- ', params);
    if(confirm('Êtes-vous sur de vouloir supprimer cette dci ?')) {
      this.classeMedicaService.delete(params).subscribe(res => {
        console.log('--------- Subs Delete Classe Médicament ------------ ', res);
        this.getClasseMedicas();
      });
    }
    
  }
}
