import { ModalOptions } from 'ngx-bootstrap/modal';
import { Laboratoire } from './../../../../core/models/laboratoire';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { LaboratoireService } from 'src/app/core/services/laboratoire.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AddLaboratoireComponent } from '../../components/laboratoire/add-laboratoire/add-laboratoire.component';
import { EditLaboratoireComponent } from '../../components/laboratoire/edit-laboratoire/edit-laboratoire.component';


@Component({
  selector: 'app-laboratoire',
  templateUrl: './laboratoire.component.html',
  styleUrls: ['./laboratoire.component.scss']
})
export class LaboratoireComponent implements OnInit {


  faPlus = faPlus;
  /*loadedData: boolean = false;*/
  laboratoires$: Observable<Laboratoire[]>; /* | string>;*/
  bsModalRef: BsModalRef;
  searchText: string;

  constructor(private laboratoireService: LaboratoireService, private modalService: BsModalService) { }

  ngOnInit() {
    console.log('NgOnInit ----------------- Laboratoire');
    this.getLaboratoires();
  }

  getLaboratoires() {
    this.laboratoires$ = this.laboratoireService.getAll();
    console.log('laboratoires: ', this.laboratoires$);
  }

  /*add(params) {
    console.log(`Emit:`, params);
  }*/

  deleteLabo(params) {
    console.log('Delete Labo Emitter -------- ', params);
    if(confirm('Êtes-vous sur de vouloir supprimer ce laboratoire ?')) {
      this.laboratoireService.delete(params).subscribe(res => {
        console.log('--------- Subs Delete Labo ------------ ', res);
        this.getLaboratoires();
      });
    }
    
  }

  openEditLaboModal(params) {
    console.log('Edit Labo Emitter -------- ', params);
    const initialState: ModalOptions = {
      initialState: {
        labo: params
      }
    };
    
    this.bsModalRef = this.modalService.show(EditLaboratoireComponent, {initialState, class: 'modal-xl' });
    // this.bsModalRef.content.closeBtnName = 'Fermer';
    this.bsModalRef.content.editLaboEmitter.subscribe(res => {
      console.log('Subs Edit Labo Emitter: ', res);
      this.laboratoireService.update(res).toPromise().then(res => {
        console.log('Edit response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getLaboratoires();
      });
    });

    /*this.bsModalRef.content.onCloseEditEmitter.subscribe(res => {
      console.log('On Hide Emitter: ', res);
      // this.bsModalRef.content.editLaboEmitter.unsubscribe();
      this.bsModalRef.hide();
    });*/

    /*this.modalService.onHide.subscribe(res => console.log('On hide Subs: -------------- ', res));*/
  }

  openAddLaboModal() {
    this.bsModalRef = this.modalService.show(AddLaboratoireComponent, { class: 'modal-xl' });
    // console.log('Emitter add labo emitter: ', this.bsModalRef.content.addLaboEmitter);
    this.bsModalRef.content.addLaboEmitter.subscribe(res => {
      console.log('Subs Add Labo Emitter: ', res);
      this.laboratoireService.add(res).toPromise().then(res => {
        console.log('Add response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getLaboratoires();
      });
    });
  }
}
