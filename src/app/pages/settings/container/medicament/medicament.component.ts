import { DciService } from './../../../../core/services/dci.service';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { Medicament } from 'src/app/core/models/medicament';
import { MedicamentService } from 'src/app/core/services/medicament.service';
import { AddMedicamentComponent } from '../../components/medicament/add-medicament/add-medicament.component';
import { EditMedicamentComponent } from '../../components/medicament/edit-medicament/edit-medicament.component';
import { Dci } from 'src/app/core/models/dci';

@Component({
  selector: 'app-medicament',
  templateUrl: './medicament.component.html',
  styleUrls: ['./medicament.component.scss']
})
export class MedicamentComponent implements OnInit {

  faPlus = faPlus;
  medicaments$: Observable<Medicament[]>;
  dcis$: Observable<Dci[]>;
  bsModalRef: BsModalRef;
  searchText: string;

  constructor(private medicamentService: MedicamentService, private dciService: DciService, private modalService: BsModalService) { }

  ngOnInit() {
    console.log('NgOnInit ----------------- Medicaments');
    this.getMedicaments();
    this.getDcis();
  }

  getMedicaments() {
    this.medicaments$ = this.medicamentService.getAll();
    console.log('medicaments: ', this.medicaments$);
  }

  deleteMedi(params) {
    console.log('Delete Medi Emitter -------- ', params);
    if (confirm('Êtes-vous sur de vouloir supprimer ce laboratoire ?')) {
      this.medicamentService.delete(params).subscribe(res => {
        console.log('--------- Subs Delete Medi ------------ ', res);
        this.getMedicaments();
      });
    }
  }

  openEditMediModal(params) {
    console.log('Edit Medi Emitter -------- ', params);

    const initialState: ModalOptions = {
      initialState: {
        medicament: params,
        dcis: []
      }
    };
    this.dcis$.subscribe(res => initialState.initialState['dcis'] = res);

    this.bsModalRef = this.modalService.show(EditMedicamentComponent, { initialState, class: 'modal-xl' });
    this.bsModalRef.content.editMediEmitter.subscribe(res => {
      console.log('Subs Edit Medi Emitter: ', res);
      this.medicamentService.update(res).toPromise().then(res => {
        console.log('Edit response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getMedicaments();
      });
    });
  }

  openAddMediModal() {
    const initialState: ModalOptions = {
      initialState: {
        dcis: []
      }
    };
    this.dcis$.subscribe(res => initialState.initialState['dcis'] = res);
    console.log('Dcis: ', initialState.initialState['dcis']);

    this.bsModalRef = this.modalService.show(AddMedicamentComponent, { initialState, class: 'modal-xl' });

    this.bsModalRef.content.addMediEmitter.subscribe(res => {
      console.log('Subs Add Medi Emitter: ', res);
      this.medicamentService.add(res).toPromise().then(res => {
        console.log('Add response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getMedicaments();
      });
    });
  }

  getDcis() {
    this.dcis$ = this.dciService.getAll();
  }

}
