import { EditDciComponent } from './../../components/dci/edit-dci/edit-dci.component';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { Dci } from 'src/app/core/models/dci';
import { DciService } from 'src/app/core/services/dci.service';
import { AddDciComponent } from '../../components/dci/add-dci/add-dci.component';

@Component({
  selector: 'app-dci',
  templateUrl: './dci.component.html',
  styleUrls: ['./dci.component.scss']
})
export class DciComponent implements OnInit {

  faPlus = faPlus;
  dcis$: Observable<Dci[]>; /* | string>;*/
  bsModalRef: BsModalRef;
  searchText: string;

  constructor(private dciService: DciService, private modalService: BsModalService) { }

  ngOnInit() {
    console.log('NgOnInit ----------------- DCI');
    this.getDcis();
  }

  getDcis() {
    this.dcis$ = this.dciService.getAll();
    console.log('Dcis: ', this.dcis$);
  }


  openAddDciModal() {
    this.bsModalRef = this.modalService.show(AddDciComponent, { class: 'modal-xl' });
    // console.log('Emitter add labo emitter: ', this.bsModalRef.content.addLaboEmitter);
    this.bsModalRef.content.addDciEmitter.subscribe(res => {
      console.log('Subs Add Labo Emitter: ', res);
      this.dciService.add(res).toPromise().then(res => {
        console.log('Add response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getDcis();
      });
    });
  }

  openEditDciModal(params) {
    console.log('Edit Dci Emitter -------- ', params);
    const initialState: ModalOptions = {
      initialState: {
        dci: params
      }
    };

    this.bsModalRef = this.modalService.show(EditDciComponent, { initialState, class: 'modal-xl' });
    // this.bsModalRef.content.closeBtnName = 'Fermer';
    this.bsModalRef.content.editDciEmitter.subscribe(res => {
      console.log('Subs Edit Dci Emitter: ', res);
      this.dciService.update(res).toPromise().then(res => {
        console.log('Edit response for server: ------- ', res);
        this.bsModalRef.hide();
        this.getDcis();
      });
    });
  }

  deleteDci(params) {
    console.log('Delete Dci Emitter -------- ', params);
    if(confirm('Êtes-vous sur de vouloir supprimer cette dci ?')) {
      this.dciService.delete(params).subscribe(res => {
        console.log('--------- Subs Delete DCI ------------ ', res);
        this.getDcis();
      });
    }
    
  }
}
