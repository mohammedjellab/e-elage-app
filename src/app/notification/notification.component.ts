import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() type: string;
  @Input() msg: string;
  constructor() { }

  ngOnInit() {
  }

  getType(){
    if(this.type=== 'success'){
      return 'success';
    }else if (this.type=== 'error'){
      return 'error';
    }
  }

}
