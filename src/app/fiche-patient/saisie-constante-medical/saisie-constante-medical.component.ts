import { ConstanteService } from '../../services/constante.service';
import { Constante } from '../../models/constante';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ConstanteMedicalService } from '../../services/constante-medical.service';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-saisie-constante-medical',
  templateUrl: './saisie-constante-medical.component.html',
  styleUrls: ['./saisie-constante-medical.component.scss']
})
export class SaisieConstanteMedicalComponent implements OnInit {

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre patient a été créer avec succès.',
      show: false
    },
    { 
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  constantes;
  myConstante: Constante = {
    date: null,
    constante: null,
    value: null,
    patient: null,
    consultation: 3
  }  
  editForm: boolean = true;
  
  loadedData: boolean = false;
  savedData: boolean = true;
  list;
  @Input() lpatient_id;
  ///
  imc =null;
  t: number = 0;
  p: number = 0;
  @Output() event = new EventEmitter;
  

  constructor(private constanteMedicalService: ConstanteMedicalService,
              private constanteService: ConstanteService ) { }

  ngOnInit(){
    this.myConstante.patient = this.lpatient_id;
    this.findAllConstantesActive();
    
    console.log('lpatient_id',this.lpatient_id);
  }

  persistConstante(f){
    console.log('rrr',f);
    this.insertConstante(f.value);
  }

  findAllConstantesActive(){
    this.constanteMedicalService.findAllConstante().subscribe(data =>{
      this.constantes = data ;
      this.loadedData = true;
    })
  }

  insertConstante(constante){
    this.constanteService.persist(constante).subscribe(data =>{
      this.event.emit({constante: data});
    })
  }
  poids(event){
    this.p = event.target.value;
    this.calculIMC(this.t,this.p);
    /*this.calculIMC(event.target.value,event.target.value);
    this.imc = event.target.value*/
    console.log('poids',event.target.value);
  }

  taille(event){
    this.t = event.target.value;
    
   /* this.imc = event.target.value*/
    console.log('taille',event.target.value);
  }

  calculIMC(poids,taille){
    taille= (taille/100)*(taille/100);
    this.imc = poids/taille;

    this.imc = this.imc.toFixed(2) ;

  }


  
  activateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  desactivateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }

  closeModal(){
    this.event.emit({constante: null});

  }


}
