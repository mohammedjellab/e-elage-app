import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieConstanteMedicalComponent } from './saisie-constante-medical.component';

describe('SaisieConstanteMedicalComponent', () => {
  let component: SaisieConstanteMedicalComponent;
  let fixture: ComponentFixture<SaisieConstanteMedicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieConstanteMedicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieConstanteMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
