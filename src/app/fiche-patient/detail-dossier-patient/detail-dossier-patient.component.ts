import { ActivatedRoute } from '@angular/router';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MutuelleService } from '../../services/mutuelle.service';
import { LDossierPatient } from 'src/app/models/lDossierPatient';
import { UserService } from 'src/app/services/user.service';
import { PaysService } from 'src/app/services/pays.service';
import { VilleService } from 'src/app/services/ville.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { MutuelleFaiteService } from 'src/app/services/mutuelle-faite.service';
import { DossierPatientService } from 'src/app/services/dossier-patient.service';
import { GDossierPatient } from 'src/app/models/gDossierPatient';

@Component({
  selector: 'app-detail-dossier-patient',
  templateUrl: './detail-dossier-patient.component.html',
  styleUrls: ['./detail-dossier-patient.component.scss']
})
export class DetailDossierPatientComponent implements OnInit {

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre patient a été créer avec succès.',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  prefix: any = {
    "speciaite": "/api/specialites/",
    "users": "/api/users/",
    "centreSoin": "/api/centre_soins/1",
    "ville":"/api/villes/",
    "pays": "/api/pays/",
    "mutuelle": "/api/mutuelles/"
  };
  correspondants;
  medecins;
  searchTerms;
  is_correspondant = "";
  test:string = "2021-06-16";
  myGDossierPatient: GDossierPatient ={
    gpatient_id: null,
    lpatient_id: null,
    sexe: '',
    firstname: '',
    lastname: '',
    cin: '',
    tel: '',
    adresse: '',
    dateNaissance: null,
    ville: '',
    pays: '',
    situationFamille: '',
    civilite: '',
    active: true,
    centreSoin: "",
    correspondant: '',
    nbEnfant: null,
    mutuelle: '',
    fixe: '',
    ramed: '',
    description:'',
    recommendation: '',
    recommendationAutre: '',
    lastname_arabe:'',
    firstname_arabe:'',
    adressePar:'',
    allergieConnue:'',
    comorbidite: '',
    fumeur: '',
    groupSanguin: '',
    matricule: '',
    photo: '',
    profession: '',
    email: ''
  }
  myLDossierPatient: LDossierPatient ={
    centreSoin: null,
    medecin: null,
    medecin2: null,
    adressePar: null,
    gdossierPatient: null,
    lpatient_id: null
  }
  age;
  typeAmbulance: string;
  selectedCorresp: string;
  isMutualiste: boolean;
  mutuelles ;
  selectedMutuelle: string;
  villes ;
  selectedVille: string;
  pays;
  selectedPays: string;
  mutuelleFaites;
  consultations;
  configPatient  = [];
  user;
  patientCurrent;
  centre = 1;
  //file 
  selectedFile;
  selectedFileAntecedent;
  //recupere parametre depuis dossier patient
  @Input() add_patient;
  @Input() gpatient_id;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private userService: UserService,
    private paysService: PaysService,
    private villeService: VilleService,
    private consultationService: ConsultationService,
    private mutuelleFaiteService: MutuelleFaiteService,
    private dossierPatientService: DossierPatientService,
    private mutuelleService: MutuelleService,
    private route: ActivatedRoute) { }

    ngOnInit() {
      this.getUser();
      if(!this.add_patient){
        console.log('add_patient 33',this.add_patient);
        this.getPatientCurrent();
        this.showPatient(this.myGDossierPatient)
      }else{
        this.initMyGDossierPatient();
      }
      //this.getCorrespondant();
      this.getMedecinByEtablissment();
      //this.getConfigPatient();
      this.getVilles();
      this.getMutuelle();

      console.log('gpatient_id',this.gpatient_id);

      
    }


    //recuperer patient selectionné
    getPatientCurrent(){
      
        this.myGDossierPatient.gpatient_id = this.gpatient_id;
       

    }

    getVilles(){
      this.villeService.findAllVilles().subscribe(data =>{
        this.villes = data;
      })
    }
    getUser(){
      this.user = JSON.parse(localStorage.getItem('user'));
      console.log('this.user',this.user);
    }
    fieldInArray(name){
      //console.log('existe nom',this.configPatient.includes['lastname_arabe'])
      return this.configPatient.includes[name];

    }
    initMyGDossierPatient(){
      this.myGDossierPatient = {
        gpatient_id: null,
        lpatient_id: null,
        sexe: '',
        firstname: '',
        lastname: '',
        cin: '',
        tel: '',
        adresse: '',
        dateNaissance: null,
        ville: '',
        pays: '',
        situationFamille: '',
        civilite: '',
        active: true,
        centreSoin: "",
        correspondant: '',
        nbEnfant: null,
        mutuelle: '',
        fixe: '',
        ramed: '',
        description:'',
        recommendation: '',
        recommendationAutre: '',
        lastname_arabe:'',
        firstname_arabe:'',
        adressePar:'',
        allergieConnue:'',
        comorbidite: '',
        fumeur: '',
        groupSanguin: '',
        matricule: '',
        photo: '',
        profession: '',
        email: ''
      }
    }


    showPatient(patient){
      console.log('current Patient',patient)
      this.age = null;
      let centreSoin = 1;
      this.dossierPatientService.show(patient.gpatient_id,centreSoin).subscribe(data =>{
        if(data){
          this.myGDossierPatient = data
          
          console.log('showPatient',data)
          this.myLDossierPatient ={
            lpatient_id: data['lpatient_id'],
            medecin: data['medecin'],
            centreSoin: data['centreSoin'],
            medecin2: data['praticien'],
            adressePar: data['adressePar'],
            gdossierPatient: data['gpatient_id']
          }
          console.log('patient',data)
          this.age = this.getAge2(data['dateNaissance']);
        }else{
          this.initMyGDossierPatient();
        }

        //this.myDossierPatient.dateNaissance = data['dateNaissance'];
      },err=>{

      })
    }

    getAge2(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
          age--;
      }
      return age+'Ans';
    } 

    //lors de changement dans iput file
    fileSelected(v){
      this.selectedFile = <File>v.target.files[0];
      console.log('fileSelected',this.selectedFile)
    }

    //traitement et evoi de fichier avec les informations 
    onUpload(){
      //photo
      let fd = new FormData();
      if(this.selectedFile){
        let extention = this.selectedFile.type;
        extention = extention.split('/').pop();
        fd.append('image',this.selectedFile,this.selectedFile.name)
        fd.append('extention',extention);
      }
      

      //antecedent
      if(this.selectedFileAntecedent){
        let extention_antec = this.selectedFileAntecedent.type;
        extention_antec = extention_antec.split('/').pop();
        fd.append('antecedent',this.selectedFileAntecedent,this.selectedFileAntecedent.name)
        fd.append('extention_antec',extention_antec);
      }
      if(this.selectedFileAntecedent || this.selectedFile){
        this.dossierPatientService.uploadFile(fd,this.myGDossierPatient.gpatient_id).subscribe(res =>{
          console.log('onUpload',res);
        });
      }
      
    }

    //lors de changement dans iput file
    fileSelectedAntecedent(v){
      this.selectedFileAntecedent = <File>v.target.files[0];
      console.log('fileSelectedAntecedent',this.selectedFileAntecedent)
      console.log('fileSelectedAntecedent',v)
    }
    //traitement et evoi de fichier avec les informations 
    /*onUploadAntecedent(){
      console.log('fileSelected',this.selectedFileAntecedent)
      let extention = this.selectedFileAntecedent.type;
      extention = extention.split('/').pop();
      let fd = new FormData();
      fd.append('antecedent',this.selectedFileAntecedent,this.selectedFileAntecedent.name)
      fd.append('extention_antec',extention);
      this.dossierPatientService.uploadFile(fd,this.myGDossierPatient.gpatient_id).subscribe(res =>{
        console.log('onUpload',res);
      });
    }*/


    // remplir les informations de patient
    persistGPatient(form: NgForm){
      console.log('form', form)
      this.myGDossierPatient = {
        gpatient_id: this.myGDossierPatient.gpatient_id,
        lpatient_id: this.myGDossierPatient.lpatient_id,
        sexe: form.value.sexe,
        firstname: form.value.firstname,
        lastname: form.value.lastname,
        cin: form.value.cin,
        tel: form.value.tel,
        adresse: form.value.adresse,
        dateNaissance: form.value.dateNaissance,
        ville: form.value.ville,
        pays: form.value.pays,
        situationFamille: form.value.situationFamille,
        civilite: form.value.civilite,
        active: true,
        centreSoin: this.user.centreSoin,
        correspondant: form.value.correspondant,
        nbEnfant: form.value.nbEnfant,
        mutuelle: form.value.mutuelle,
        fixe: form.value.fixe,
        ramed: form.value.ramed,
        description: form.value.description,
        recommendation: form.value.recommendation,
        recommendationAutre: form.value.recommendationAutre,
        lastname_arabe: form.value.lastname_arabe,
        firstname_arabe: form.value.firstname_arabe,
        adressePar: form.value.adressePar,
        allergieConnue: form.value.allergieConnue,
        comorbidite:  form.value.comorbidite,
        fumeur:  form.value.fumeur,
        groupSanguin: form.value.groupSanguin,
        matricule: form.value.matricule,
        photo: form.value.photo,
        profession: form.value.profession,
        email: form.value.email
       
      }
      if(form.value.dateNaissance){
        
      }

      

      //test si form est valide on enregistre
      if(form.form.valid){
        if(!this.myGDossierPatient.gpatient_id){
          this.dossierPatientService.persistGDossierPatient(this.myGDossierPatient).subscribe(data=>{
            console.log('dpersistGDossierPatient',data.gpatient_id)
            this.persistLDossierPatient(form,data.gpatient_id);
            this.myGDossierPatient.gpatient_id = data.gpatient_id;
            this.onUpload();
            //this.onUploadAntecedent();
          })
        }else{
          
          this.dossierPatientService.updateGDossierPatient(this.myGDossierPatient).subscribe(data=>{
            console.log('dpersistGDossierPatient',data)
            this.persistLDossierPatient(form,data.gpatient_id);
          })
        }
      }

      //envoyer gpatient_id pour la page dossier patient
      


    }
    /*
    //
    */

    calculAge(event){
      console.log('agagaga',event.target.value);
      this.age = this.getAge2(event.target.value);
    }

    closeModal(){
      this.event.emit({lpatient_id: null,gpatient_id: null});

    }
    persistLDossierPatient(form,gdossierPatient){
      this.myLDossierPatient = {
        centreSoin: this.user.centreSoin,
        medecin: form.value.medecin,
        adressePar: form.value.adressePar,
        gdossierPatient: gdossierPatient,
        lpatient_id: this.myGDossierPatient.lpatient_id
      }
      if(!this.myGDossierPatient.gpatient_id){
        this.dossierPatientService.persistLDossierPatient(this.myLDossierPatient).subscribe(data=>{
          this.event.emit({lpatient_id: data.lpatient_id,gpatient_id: this.myGDossierPatient.gpatient_id});

         // if(data){
            console.log('persistLDossierPatient',data)
            this.defaultAlerts[0].show = true;
            console.log('agenda persit 1',this.defaultAlerts)
          //}

        })
      }else{
        console.log('agenda persit 2',this.myLDossierPatient)
        console.log('this.myGDossierPatient',this.myGDossierPatient)
        
        this.dossierPatientService.updateLDossierPatient(this.myLDossierPatient).subscribe(data=>{


        })
      }

    }
    getConfigPatient(){
      this.dossierPatientService.getFieldPatient().subscribe(data=>{
        console.log('config patient',data)
        //this.configPatient = data;
      },err=>{

      })
    }

    /*
    getSituationFamille(s){
      this.myLDossierPatient.situationFamille=s.target.value;
    }
    selectCorrespondant(s){
      this.myLDossierPatient.correspondant=this.prefix.users+s.target.value;
    }
    selectMedecin(s){
      this.myLDossierPatient.medecin=this.prefix.users+s.target.value;
    }
    selectMedecin2(s){
      this.myLDossierPatient.medecin2=this.prefix.users+s.target.value;
    }

    getCivilite(s){
      this.myLDossierPatient.civilite=s.target.value;
    }

    deleteAttributeEmpty(arr){
      $.each(arr, function(key,value){
        if(value ==="" || value ===null){
          console.log('eee',arr[key])
          delete arr[key];
        }
      });
    }*/

    /*getMutuelleFaites(){
      this.mutuelleFaiteService.findByPatient(this.myGDossierPatient.gpatient_id).subscribe(data =>{
        this.mutuelleFaites = data;
        console.log('mutuelleFaites',this.mutuelleFaites);

      }, err =>{

      })
    }
    getConsultations(){

      this.consultationService.listConsltations(1).subscribe(data =>{

        this.consultations = data;
        console.log('mutuelleFaites',data);
      }, err =>{

      })
    }*/

    /*selectCorrespondant(corresp){
      console.log('corresp',corresp);
      this.selectedCorresp = corresp.firstname+" "+corresp.lastname;
      this.myDossierPatient.correspondant = this.prefix.users+corresp.id;

      this.clearCorresponant();

    }

    clearCorresponant(){
      this.correspondants = [];
    }
    searchCorrespondant(search){
      console.log('eeee',search);

      console.log('eeee',search);
      this.userService.searchCorrespondant(search).subscribe( data =>{
          this.correspondants =data
          console.log('corresp',data);
        })
    }
    */

    getCorrespondant(){
      this.userService.getCorrespondant().subscribe(data =>{
        this.correspondants = data
        console.log('getCorrespondant',data);
      }, err =>{

      })
    }
    getMedecinByEtablissment(){
      this.userService.getMedecinByEtablissment(1).subscribe(data =>{
        this.medecins = data
        console.log('data',data);
      }, err =>{

      })
    }
    getMutuelle(){
      this.mutuelleService.findAllMutuelle().subscribe( data =>{
        console.log('mutuelles',data)
        this.mutuelles =data;
      }, err =>{});
    }
    //si l'utilisateur check case à cocher pour afficher la liste des mutuelles ou cacher
    checkMutuelle(event){
      this.isMutualiste = event.target.checked ;
      console.log('event',event.target.checked);
    }
    /*
    searchMutuelle(search){
      this.mutuelleService.searchMutuelle(search).subscribe( data =>{
        console.log('fdfddf',data)
        this.mutuelles =data;
      }, err =>{});
    }

    selectMutuelle(mutuelle){
      console.log('mutuelle',mutuelle);
      this.myDossierPatient.mutuelle = this.prefix.mutuelle+mutuelle.id;
      this.selectedMutuelle = mutuelle.libelle;
      this.clearMutuelle();
    }

    clearMutuelle(){
      this.mutuelles = [];
    }

    searchVille(term){
      console.log('term',term);

      this.villeService.searchVille(term).subscribe(data =>{
        this.villes = data;
      })
    }

    selectVille(ville){
      this.myDossierPatient.ville = this.prefix.ville+ville.id;
      this.selectedVille = ville.name ;
      this.clearVille();
    }

    clearVille(){
      this.villes = [] ;
    }*/

    /*searchPays(term){
      this.paysService.searchPays(term).subscribe(data =>{
      console.log('data',data);

        this.pays = data;
      console.log('pays',this.pays)

      })
    }

    selectPays(pays){
      this.myDossierPatient.pays = this.prefix.pays+this.pays.id;
      this.selectedPays = pays.name ;
      this.clearVille();
    }

    clearPays(){
      this.pays = [] ;
    }*/

}
