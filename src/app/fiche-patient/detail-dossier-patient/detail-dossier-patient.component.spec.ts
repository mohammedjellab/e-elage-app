import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDossierPatientComponent } from './detail-dossier-patient.component';

describe('DetailDossierPatientComponent', () => {
  let component: DetailDossierPatientComponent;
  let fixture: ComponentFixture<DetailDossierPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDossierPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDossierPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
