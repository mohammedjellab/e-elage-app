import { SaisieConstanteMedicalComponent } from './saisie-constante-medical/saisie-constante-medical.component';
import { DetailDossierPatientComponent } from './detail-dossier-patient/detail-dossier-patient.component';
import { ConstanteMedicalService } from 'src/app/services/constante-medical.service';
import { ApiService } from '../services/api.service';
import { ConstanteService } from '../services/constante.service';
import { GDossierPatient } from '../models/gDossierPatient';
import { ConsultationService } from '../services/consultation.service';
import { MutuelleFaiteService } from '../services/mutuelle-faite.service';
import { Ville } from '../models/ville';
import { VilleService } from '../services/ville.service';
import { PaysService } from '../services/pays.service';
import { MutuelleService } from '../services/mutuelle.service';
import { DossierPatientService } from '../services/dossier-patient.service';
import { UserService } from '../services/user.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../models/User';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LDossierPatient } from '../models/lDossierPatient';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap';
import { MutuelleFaiteComponent } from './mutuelle-faite/mutuelle-faite.component';
import { ApexAxisChartSeries, ApexChart, ChartComponent,ApexXAxis,ApexDataLabels,ApexGrid,ApexStroke,ApexTitleSubtitle, ApexYAxis, ApexMarkers } from 'ng-apexcharts';
import { ReglementConsultationComponent } from './reglement-consultation/reglement-consultation.component';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  markers: ApexMarkers;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-fiche-patient',
  templateUrl: './fiche-patient.component.html',
  styleUrls: ['./fiche-patient.component.scss']
})

export class FichePatientComponent implements OnInit {


  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre patient a été créer avec succès.',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  prefix: any = {
    "speciaite": "/api/specialites/",
    "users": "/api/users/",
    "centreSoin": "/api/centre_soins/1",
    "ville": "/api/villes/",
    "pays": "/api/pays/",
    "mutuelle": "/api/mutuelles/"
  };
  correspondants;
  medecins;
  searchTerms;
  is_correspondant = "";
  test: string = "2021-06-16";
  myGDossierPatient: GDossierPatient = {
    sexe: '',
    firstname: '',
    lastname: '',
    cin: '',
    tel: '',
    adresse: '',
    dateNaissance: null,
    ville: '',
    pays: '',
    situationFamille: '',
    civilite: '',
    active: true,
    centreSoin: "",
    correspondant: '',
    nbEnfant: null,
    mutuelle: '',
    fixe: '',
    ramed: '',
    description: '',
    recommendation: '',
    recommendationAutre: '',
    lastname_arabe: '',
    firstname_arabe: '',
    adressePar: '',
    allergieConnue: '',
    comorbidite: '',
    fumeur: '',
    groupSanguin: '',
    matricule: '',
    photo: '',
    profession: '',
  }
  myLDossierPatient: LDossierPatient = {
    centreSoin: null,
    medecin: null,
    medecin2: null,
    adressePar: null,
    gdossierPatient: null
  }
  age;
  typeAmbulance: string;
  selectedCorresp: string;
  isMutualiste: boolean;
  mutuelles;
  selectedMutuelle: string;
  villes;
  selectedVille: string;
  pays;
  selectedPays: string;
  mutuelleFaites;
  consultations;
  configPatient = [];
  user;
  patientCurrent;
  myPatient;
  loadedData: boolean = false;
  savedData: boolean = true;
  ///////////////////statistique
  pouls;
  chart_pouls = {
    type: 'line',
    height: 90,
    sparkline: {
      enabled: true
    }
  }
  series_pouls = [
    {

      name: 'Orders',
      data: null
    }
  ]
  series_temp = [
    {

      name: 'T',
      data: null
    }
  ]
  series_ta = [
    {

      name: 'TA',
      data: null
    }
  ]
  
  series_poids = [
    {

      name: 'Orders',
      data: null
    }
  ]

  series_bus = [
    {

      name: 'Orders',
      data: null
    }
  ]
  series_bua = [
    {

      name: 'Orders',
      data: null
    }
  ]
  series_glypp = [
    {

      name: 'Orders',
      data: null
    }
  ]
  series_imc = [
    {
      name: 'Orders',
      data: null
    }
  ]
  series_poids_taille = [
    {
      name: 'Poids',
      data: null
    },
    {
      name: 'Taille',
      data: null
    }
  ]
  @ViewChild("chart", null) chart: ChartComponent;
  public chartOptions_ta: Partial<ChartOptions>;
  public chartOptions_glypp: Partial<ChartOptions>;
  public chartOptions_temp: Partial<ChartOptions>;
  public chartOptions_glyjeun: Partial<ChartOptions>;
  public chartOptions_poids_taille: Partial<ChartOptions>;
  public chartOptions_bua: Partial<ChartOptions>;
  public chartOptions_bus: Partial<ChartOptions>;
  public chartOptions_pouls: Partial<ChartOptions>;
  public chartOptions_imc: Partial<ChartOptions>;
  

  //file
  url_images: string;
  url_images_antecedent: string;

  //list consultation
  situations;
  reglements;

  //modal
  bsModalRef: BsModalRef;
  modalShow: boolean = false;

  reglement1 =[];
  reglement2 =[];
  refresh_reg: boolean = false;
  refresh__list_reg: boolean = false;


  //modal saisie constante

  @ViewChild('saisieConstanteMedicale', { static: false }) saisieConstanteMedicale: ModalDirective;

  //liste des constantes
  constantes ;
  paramSurveillances;
  fakeArray = new Array(6);
  arr_date = [];
  btn_show: boolean =true;

  //pour réinitialisé le patient
  add_patient: boolean = false;
  constructor(private userService: UserService,
    private paysService: PaysService,
    private villeService: VilleService,
    private consultationService: ConsultationService,
    private mutuelleFaiteService: MutuelleFaiteService,
    private dossierPatientService: DossierPatientService,
    private mutuelleService: MutuelleService,
    private apiService: ApiService,
    private constanteService: ConstanteService,
    private modalService: BsModalService,
    private constanteMedicalService: ConstanteMedicalService,
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getUser();
    this.getNameFolderImage();
    this.getPatientCurrent();
    
    //this.getCorrespondant();
    //this.getMedecinByEtablissment();
    //this.getMutuelleFaites();
    //this.getConfigPatient();
    //this.getVilles();
    //this.getMutuelle();


    if(!this.myGDossierPatient.gpatient_id){
      //this.myGDossierPatient =
      let patient = JSON.parse(localStorage.getItem('myGDossierPatient'));
      if(patient){
        this.myPatient = patient;
      }
      console.log('JSON.parse', this.myPatient);
      console.log('JSON.parse', this.myPatient.lastname);
    }else{
      this.showPatient(this.myGDossierPatient)
    }
    //this.getConstantesMiniGraph();
    //this.findAllConsultationByPatient();
    //this.situationFinanciereByPatient();
    //console.log('dddd',this.getAge('14/06/2021'))
    /*let date = new Date();
    console.log('dattt',date.getFullYear());*/
    this.findAllConstante();
    
  

  }


  //
  getNameFolderImage() {
    this.url_images = this.apiService.getUrlImagesPatient();
  }
  getNameFolderImageAntecedent() {
    this.url_images_antecedent = this.apiService.getUrlImagesPatientAntecedent();
  }

  //recuperer patient selectionné
  getPatientCurrent() {
    this.route.queryParams.subscribe(params => {
      this.patientCurrent = params;
      this.myGDossierPatient.gpatient_id = params.gpatient_id;
      console.log('patientCurrent', this.myGDossierPatient);
      console.log('params.id', params);
    });

  }

  getVilles() {
    this.villeService.findAllVilles().subscribe(data => {
      this.villes = data;
    })
  }
  getUser() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user', this.user);
  }
  fieldInArray(name) {
    //console.log('existe nom',this.configPatient.includes['lastname_arabe'])
    return this.configPatient.includes[name];

  }


  showPatient(patient) {
    console.log('current Patient', patient)
    this.dossierPatientService.show(patient.gpatient_id, this.user.centreSoin).subscribe(data => {
      this.myPatient = data

      if(data){
        this.myGDossierPatient = data;
        localStorage.setItem('myGDossierPatient',JSON.stringify(this.myGDossierPatient));

        console.log('this.myPatient',data)
        this.age = this.getAge2(data['dateNaissance']);
        this.getConstantesMiniGraph();

        this.findAllConsultationByPatient();
        this.situationFinanciereByPatient();
        this.listRegelementByPatient();
        //this.getMutuelleFaites();

      }
      this.loadedData = true;

    }, err => {
    })
  }


  getAge2(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }


  persistGPatient(form: NgForm) {
    console.log('form', form)
    this.myGDossierPatient = {
      gpatient_id: this.myGDossierPatient.gpatient_id,
      lpatient_id: this.myGDossierPatient.lpatient_id,
      sexe: form.value.sexe,
      firstname: form.value.firstname,
      lastname: form.value.lastname,
      cin: form.value.cin,
      tel: form.value.cin,
      adresse: form.value.adresse,
      dateNaissance: form.value.dateNaissance,
      ville: form.value.ville,
      pays: form.value.pays,
      situationFamille: form.value.situationFamille,
      civilite: form.value.civilite,
      active: true,
      centreSoin: this.user.centreSoin.id,
      correspondant: form.value.correspondant,
      nbEnfant: form.value.nbEnfant,
      mutuelle: form.value.mutuelle,
      fixe: form.value.fixe,
      ramed: form.value.ramed,
      description: form.value.description,
      recommendation: form.value.recommendation,
      recommendationAutre: form.value.recommendationAutre,
      lastname_arabe: form.value.lastname_arabe,
      firstname_arabe: form.value.firstname_arabe,
      adressePar: form.value.adressePar,
      allergieConnue: form.value.allergieConnue,
      comorbidite: form.value.comorbidite,
      fumeur: form.value.fumeur,
      groupSanguin: form.value.groupSanguin,
      matricule: form.value.matricule,
      photo: form.value.photo,
      profession: form.value.profession

    }



    //this.myLDossierPatient.nbEnfant=+this.myLDossierPatient.nbEnfant;
    //this.deleteAttributeEmpty(arr);
    if (form.form.valid) {
      if (!this.myGDossierPatient.gpatient_id) {
        this.dossierPatientService.persistGDossierPatient(this.myGDossierPatient).subscribe(data => {
          console.log('dpersistGDossierPatient', data)
          this.persistLDossierPatient(form, data.gpatient_id);
        })
      } else {
        this.dossierPatientService.updateGDossierPatient(this.myGDossierPatient).subscribe(data => {
          console.log('dpersistGDossierPatient', data)
          this.persistLDossierPatient(form, data.lpatient_id);
        })
      }
    }


  }
  /*
  //
  */

  persistLDossierPatient(form, gdossierPatient) {
    this.myLDossierPatient = {
      centreSoin: this.user.centreSoin.id,
      medecin: form.value.medecin,
      adressePar: form.value.adressePar,
      gdossierPatient: gdossierPatient,
      lpatient_id: null
    }
    console.log('this.myLDossierPatient', this.myLDossierPatient);
    if (!this.myGDossierPatient.gpatient_id) {
      this.dossierPatientService.persistLDossierPatient(this.myLDossierPatient).subscribe(data => {

        // if(data){
        console.log('persistLDossierPatient', data)
        this.defaultAlerts[0].show = true;
        console.log('agenda persit 1', this.defaultAlerts)
        //}

      })
    } else {
      console.log('this.myLDossierPatient', this.myLDossierPatient);
      this.dossierPatientService.updateLDossierPatient(this.myLDossierPatient).subscribe(data => {
        console.log('agenda persit 2', this.defaultAlerts)


      })
    }

  }
  getConfigPatient() {
    this.dossierPatientService.getFieldPatient().subscribe(data => {
      console.log('config patient', data)
      //this.configPatient = data;
    }, err => {

    })
  }

  /*
  getSituationFamille(s){
    this.myLDossierPatient.situationFamille=s.target.value;
  }
  selectCorrespondant(s){
    this.myLDossierPatient.correspondant=this.prefix.users+s.target.value;
  }
  selectMedecin(s){
    this.myLDossierPatient.medecin=this.prefix.users+s.target.value;
  }
  selectMedecin2(s){
    this.myLDossierPatient.medecin2=this.prefix.users+s.target.value;
  }

  getCivilite(s){
    this.myLDossierPatient.civilite=s.target.value;
  }

  deleteAttributeEmpty(arr){
    $.each(arr, function(key,value){
      if(value ==="" || value ===null){
        console.log('eee',arr[key])
        delete arr[key];
      }
    });
  }*/

  getMutuelleFaites() {
    this.mutuelleFaiteService.findByPatient(this.myGDossierPatient.lpatient_id).subscribe(data => {
      this.mutuelleFaites = data;
      console.log('mutuelleFaites', this.mutuelleFaites);

    }, err => {

    })
  }
  mutuelleFaite(c){
    
  }


  /*selectCorrespondant(corresp){
    console.log('corresp',corresp);
    this.selectedCorresp = corresp.firstname+" "+corresp.lastname;
    this.myDossierPatient.correspondant = this.prefix.users+corresp.id;

    this.clearCorresponant();

  }

  clearCorresponant(){
    this.correspondants = [];
  }
  searchCorrespondant(search){
    console.log('eeee',search);

    console.log('eeee',search);
    this.userService.searchCorrespondant(search).subscribe( data =>{
        this.correspondants =data
        console.log('corresp',data);
      })
  }
  */

  getCorrespondant() {
    this.userService.getCorrespondant().subscribe(data => {
      this.correspondants = data
      console.log('getCorrespondant', data);
    }, err => {

    })
  }
  getMedecinByEtablissment() {
    this.userService.getMedecinByEtablissment(1).subscribe(data => {
      this.medecins = data
      console.log('data', data);
    }, err => {

    })
  }
  getMutuelle() {
    this.mutuelleService.findAllMutuelle().subscribe(data => {
      console.log('mutuelles', data)
      this.mutuelles = data;
    }, err => { });
  }
  //si l'utilisateur check case à cocher pour afficher la liste des mutuelles ou cacher
  checkMutuelle(event) {
    this.isMutualiste = event.target.checked;
    console.log('event', event.target.checked);
  }
  /*
  searchMutuelle(search){
    this.mutuelleService.searchMutuelle(search).subscribe( data =>{
      console.log('fdfddf',data)
      this.mutuelles =data;
    }, err =>{});
  }

  selectMutuelle(mutuelle){
    console.log('mutuelle',mutuelle);
    this.myDossierPatient.mutuelle = this.prefix.mutuelle+mutuelle.id;
    this.selectedMutuelle = mutuelle.libelle;
    this.clearMutuelle();
  }

  clearMutuelle(){
    this.mutuelles = [];
  }

  searchVille(term){
    console.log('term',term);

    this.villeService.searchVille(term).subscribe(data =>{
      this.villes = data;
    })
  }

  selectVille(ville){
    this.myDossierPatient.ville = this.prefix.ville+ville.id;
    this.selectedVille = ville.name ;
    this.clearVille();
  }

  clearVille(){
    this.villes = [] ;
  }*/

  /*searchPays(term){
    this.paysService.searchPays(term).subscribe(data =>{
    console.log('data',data);

      this.pays = data;
    console.log('pays',this.pays)

    })
  }

  selectPays(pays){
    this.myDossierPatient.pays = this.prefix.pays+this.pays.id;
    this.selectedPays = pays.name ;
    this.clearVille();
  }

  clearPays(){
    this.pays = [] ;
  }*/

  activateLoader() {
    this.savedData = true;
    this.loadedData = true;
  }
  desactivateLoader() {
    this.savedData = true;
    this.loadedData = true;
  }

  //recuperer les constantes
  getConstantesMiniGraph() {
    console.log('this.myGDossierPatient.lpatient_id',this.myGDossierPatient.lpatient_id);
    this.constanteService.getConstantesMiniGraph(this.myGDossierPatient.lpatient_id).subscribe(data => {
      console.log('constantes',data);
      this.constantes = data ;
      let arr_pouls = [];
      let arr_temp = [];
      let arr_ta = [];
      let arr_glypp = [];
      let arr_glyjeun = [];
      let arr_poids = [];
      let arr_bus = [];
      let arr_bua = [];
      let arr_taille = [];
      let arr_imc = [];
      let arr_date = [];
      let of_date
      Object.keys(data).map(function (key) {
        
        if (data[key]['abreviation'] == 'Pouls') {
          arr_pouls.push(+data[key]['valeur'])
          arr_date.push( data[key]['date']);
          return Array(arr_pouls,arr_date);
        } else if (data[key]['abreviation'] == 'T') {
          arr_temp.push(+data[key]['valeur'])
          return arr_temp;
        } else if (data[key]['abreviation'] == 'Poids') {
          arr_poids.push(+data[key]['valeur'])
          return arr_poids;
        } else if (data[key]['abreviation'] == 'Glypp') {
          arr_glypp.push(+data[key]['valeur'])
          return arr_glypp;
        }else if (data[key]['abreviation'] == 'Glycj') {
          arr_glyjeun.push(+data[key]['valeur'])
          return arr_glypp;
        } else if (data[key]['abreviation'] == 'BuS') {
          arr_bus.push(+data[key]['valeur'])
          return arr_bus;
        } else if (data[key]['abreviation'] == 'BuS') {
          arr_bua.push(+data[key]['valeur'])
          return arr_bua;
        } else if (data[key]['abreviation'] == 'TA') {
          arr_ta.push(+data[key]['valeur'])
          return arr_ta;
        } else if (data[key]['abreviation'] == 'Taille') {
          arr_taille.push(+data[key]['valeur'])
          return arr_taille;
        } else if (data[key]['abreviation'] == 'IMC') {
          arr_imc.push(+data[key]['valeur'])
          return arr_imc;
        }
      });

      /*data.forEach(element => {

      });*/

      this.series_pouls[0].data = arr_pouls;
      this.series_ta[0].data = arr_ta;
      this.initChartOptionTa("TA",arr_ta);
      this.initChartOptionGlypp("Glycémie PP",arr_glypp);
      this.initChartOptionTemp("Temp",arr_temp);
      this.initChartOptionGlysmJeun("Glycémie à jeun",arr_glyjeun);
      this.initChartOptionBua("Bu a",arr_bua);
      this.initChartOptionBus("Bu s",arr_bus);
      this.initChartOptionPouls("Pouls",arr_pouls);
      this.initChartOptionImc("Imc",arr_imc);
      this.initChartOptionPoidsTaille("Poids","Taille",arr_poids,arr_taille);
      this.series_bus[0].data = arr_bus;
      this.series_bua[0].data = arr_bua;
      this.series_glypp[0].data = arr_glypp;
      this.series_temp[0].data = arr_temp;
      this.series_imc[0].data = arr_imc;
      this.series_poids_taille[0].data = arr_poids;
      this.series_poids_taille[1].data = arr_taille;
      this.arr_date = arr_date;
      console.log('this.arr_date', this.arr_date);
      console.log('arr_imc', arr_imc);
      
    });
  }
  
  initChartOptionPouls(name,array){
    this.chartOptions_pouls = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionImc(name,array){
    this.chartOptions_imc = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'bar',
        height: 60,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionTa(name,array){
    this.chartOptions_ta = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionGlypp(name,array){
    this.chartOptions_glypp = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionTemp(name,array){
    this.chartOptions_temp = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionGlysmJeun(name,array){
    this.chartOptions_glyjeun = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionBua(name,array){
    this.chartOptions_bua = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionBus(name,array){
    this.chartOptions_bus = {
      series: [
        {
          name: name,
          data: array
        }
      ],
      chart: {
        type: 'line',
        height: 90,
        sparkline: {
            enabled: true
        },
      },
      xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }
    };

    
  }
  initChartOptionPoidsTaille(name1,name2,array1,array2){
    this.chartOptions_poids_taille = {
      series: [
        {
          name: name1,
          data: array1
        },
        {
          name: name2,
          data: array2
        }
      ],
      
      chart: {
        type: 'area',
        height: 265,
        /*sparkline: {
            enabled: true
        },*/
      },
      /*xaxis:{
        crosshairs: {
          width: 0
        }
      },
      yaxis: {
        min: 0
      },
      markers:{
        size: 0
      }*/
    };

    
  }
  //afficher la list des consultations par patient
  findAllConsultationByPatient() {
    this.consultationService.findAllByPatient(this.myGDossierPatient.lpatient_id).subscribe(data => {
      this.consultations = data;
    })
  }

  situationFinanciereByPatient() {
    this.consultationService.situationFinanciere(this.myGDossierPatient.lpatient_id).subscribe(data => {
      this.situations = data;
      this.refresh_reg = true;
    })
  }

  listRegelementByPatient() {
    this.consultationService.listReglementByLpatient(this.myGDossierPatient.lpatient_id).subscribe(data => {
      this.reglements = data;
      this.refresh__list_reg = true;
    })
  }


  //ouvrir modal de regeler les consultations un seul
  openLoginModal(reglement) {
    this.reglement2 = [];
    this.reglement2.push(reglement);
    let initialState = {
      reglement: this.reglement2,
      source: 'patient'
    };
    console.log('reglement',this.reglement2);
    this.bsModalRef = this.modalService.show(ReglementConsultationComponent, { initialState,class: 'modal-xl' });
    this.modalService.config.class = "modal-xl";
    this.bsModalRef.content.event.subscribe(res => {
      let reglement = res.reglement;
      console.log('action get info', res)
      if (reglement) {
        this.refresh_reg = true;
        this.situationFinanciereByPatient();
        this.listRegelementByPatient();
        this.bsModalRef.hide();
      }else{
        this.bsModalRef.hide();
      }
    });
  }
  //ouvrir modal de regeler les consultations un seul
  openEditProfileModal(){
    let initialState = {
      add_patient: this.add_patient,
      gpatient_id : this.myGDossierPatient.gpatient_id
    };
    console.log('initialState',initialState.add_patient);
    this.bsModalRef = this.modalService.show(DetailDossierPatientComponent,  { initialState,class: 'modal-xl' });
    this.bsModalRef.content.event.subscribe(res => {
      let lpatient_id = res.lpatient_id;
      let gpatient_id = res.gpatient_id;
      this.myGDossierPatient.gpatient_id = gpatient_id
      this.myGDossierPatient.lpatient_id = lpatient_id
      this.add_patient =false;
      console.log('action get info', lpatient_id)
      console.log('myGDossierPatient', this.myGDossierPatient)
      if(gpatient_id) {
        this.refresh_reg = true;
        setTimeout(() => {
          this.showPatient(this.myGDossierPatient)
        }, 1000);
        this.bsModalRef.hide();
      }else if(!gpatient_id || !lpatient_id){
        this.bsModalRef.hide();

      }
    });
  }
  //vider la list des reglments
  viderListReglement(){
    this.reglement1 =[];
  }
  //
  modalSaisieConstanteMedicale(){
    let initialState = {
      lpatient_id: this.myGDossierPatient.lpatient_id,
    };
    console.log('initialState',initialState.lpatient_id);
    this.bsModalRef = this.modalService.show(SaisieConstanteMedicalComponent,  { initialState,class: 'modal-xl' });
    this.bsModalRef.content.event.subscribe(res => {
      let constante = res.constante;
      /*let gpatient_id = res.gpatient_id;
      this.myGDossierPatient.gpatient_id = gpatient_id
      this.myGDossierPatient.lpatient_id = lpatient_id*/
      console.log('action get info', res)
      if (constante) {
        //this.ngOnInit();
        this.getConstantesMiniGraph();
        
        this.bsModalRef.hide();
      }else{
        this.bsModalRef.hide();
      }
    });
  }
 
    

  

   //apres l'ajout d'une nouvelle ligne de constante, cacher modal et actialiser la liste
  onChangeConstante(){
      //this.hideModalConstante();
      this.getConstantesMiniGraph();
  }

  //initialiser tous les tableaux
  addPatient(e){
    console.log('action get info', e);
    this.add_patient =e.add_patient;
    if(e.add_patient){
      this.myGDossierPatient.lpatient_id = null;
      this.myPatient = {}
      this.consultations = [];
      this.situations = [];
      this.mutuelleFaites = [] ;
      this.constantes = [];
      this.arr_date = [] ;
      this.chartOptions_pouls.series[0].data = null;
      this.chartOptions_glyjeun.series[0].data = null;
      this.chartOptions_ta.series[0].data = null;
      this.chartOptions_bus.series[0].data = null;
      this.chartOptions_bua.series[0].data = null;
      this.chartOptions_glypp.series[0].data = null;
      this.chartOptions_temp.series[0].data = null;
      this.chartOptions_imc.series[0].data = null;
      this.chartOptions_poids_taille.series[0].data = null;
      this.chartOptions_poids_taille.series[1].data = null;

    }
  }
  //pour afficher/cacher ça depend de nav qu il a cliqué l'utilisateur
  Reglement(){
    this.btn_show = !this.btn_show;
  }

  refreshListRegelement(){

  }

  //pour envoyer un parametre au fille page app-detail-dossier-patient
  newPatientChanged(){
    this.add_patient =true;
  }

  //ouvrir modal de regeler les consultations plusieurs
  openLoginModalGlobal(){
    let initialState = {
      reglement: this.reglement1,
    };
    console.log('this.reglement1',this.reglement1);
    this.bsModalRef = this.modalService.show(ReglementConsultationComponent, { initialState,class: 'modal-xl' });

    this.bsModalRef.content.event.subscribe(res => {
      let reglement = res.reglement;
      console.log('action get info', res)
      if(reglement) {
        this.refresh_reg = true;
        this.situationFinanciereByPatient();
        this.bsModalRef.hide();
      }else{
        this.bsModalRef.hide();

      }
      this.viderListReglement();
    });
  }


  hideModalReglement(){
    this.bsModalRef.hide();
  }

  openModalMutuelle(mutuelle){
    let initialState = {
      mutuelle: mutuelle,
    };
    this.bsModalRef = this.modalService.show(MutuelleFaiteComponent,{initialState});
    this.bsModalRef.content.event.subscribe(res => {
      let mutuelle = res.mutuelle;
      console.log('action', res)
      if(mutuelle) {
        this.bsModalRef.hide();
        this.mutuelleFaites = [mutuelle, ...this.mutuelleFaites]
      }else{
        this.bsModalRef.hide();
      }
    });
  }

  hideModal(){
    this.modalService.hide(3);
  }


  hideModalConstante(){
    this.saisieConstanteMedicale.hide();
  }


  selectReglement(c){
    console.log('reglement',c);
    this.reglement1.push(c);
  }

 

  onChangeRegelemnt(p){


    console.log('pdpdpdpdp',p)
    if(p.reglement){
      this.hideModalConstante();
      //this.getConstantesMiniGraph();
    }
  }
  //recuperer les constantes
  findAllConstante(){
    this.constanteMedicalService.findAllConstante().subscribe(data =>{
      this.paramSurveillances = data;
      this.loadedData = true;
      console.log('constantes', data);
    }, err =>{

    })
  }


}
