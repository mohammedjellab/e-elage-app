import { DefaultService } from '../../services/default.service';
import { Component, Input, OnInit,EventEmitter  } from '@angular/core';
import { RdvPaiement } from '../../models/rdvPaiement';
import { RdvPaiementService } from '../../services/rdv-paiement.service';

@Component({
  selector: 'app-reglement-consultation',
  templateUrl: './reglement-consultation.component.html',
  styleUrls: ['./reglement-consultation.component.scss']
})
export class ReglementConsultationComponent implements OnInit {

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre patient a été créer avec succès.',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'vous devez saisir un montant égal ou inférieur de montant de consultation',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
    myPaiement: RdvPaiement = {
    typePaiement: '',
    dateReglement: null,
    montant: null,
    observation: '',
    centreSoin: null,
    consultations: null,
    patient: null
  }
  patient: number = 3;
  idRdv: number;
  mt : number = 0;
  //pour recuperer les info depuis dossier patient
  @Input() reglement;
  consultations: Array<number> = [];
  montant_validee: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(private defaultService: DefaultService,
              private rdvPaiementService: RdvPaiementService) { }

  ngOnInit() {
    console.log("reglement",this.reglement)

    this.initMyPaiement();
  }
  initMyPaiement(){
    let date = this.getDateFormatMonthDayYearSlash(new Date());

    if(this.reglement){
      let mt= 0;
      for(let reg of this.reglement){

         
        //if(typeof this.mt=='number'){
          //console.log('reste', +<number>reg.reste);
          //console.log('montant_regle', +<number>reg.montant_regle);
          mt = +<number>reg.montant - +reg.montant_regle+mt;
          //console.log('reg',reg);
          //console.log('mt',mt);
          this.consultations.push(reg.consultation_id);
        //}
        
      };

      console.log('consultations',mt);
      this.myPaiement ={
        typePaiement: 'Chéque',
        dateReglement: date,
        montant: mt,
        observation: '',
        centreSoin: this.reglement.centreSoin,
        consultations: this.consultations,
        patient: this.reglement.lpatient_id
      }
    }

  }
  remplirRdvPaiement(f){
    this.myPaiement = {
      consultations: this.consultations,
      typePaiement: f.value.typePaiement,
      dateReglement: f.value.dateReglement,
      montant: f.value.montant,
      observation: f.value.observation,
      centreSoin: this.reglement.centreSoin,
      patient: this.reglement.lpatient_id
    }
    if(f.form.valid){
      this.persistPaiement();
    }
  }

  getDateFormatMonthDayYearSlash(today){
    return this.defaultService.getDateFormatMonthDayYearSlash(today);
  }

  persistPaiement(){
    console.log('myPaiement',this.myPaiement);
    this.rdvPaiementService.persist(this.myPaiement).subscribe(data =>{
      this.event.emit({reglement: data});

    })
  }
  closeModal(){
    this.event.emit({reglement: null});

  }

 /* getInfoReglementByConsultation(){
    this.
  }*/

  changeMontant(e){
    console.log('change mt'+typeof this.myPaiement.montant,typeof +e.target.value);
    if(+e.target.value > this.myPaiement.montant){
      this.montant_validee = false ;
      this.defaultAlerts[2].show = true;
    }else{
      this.montant_validee = true ;
      this.defaultAlerts[2].show = false;

    }
    //if()
  }

}
