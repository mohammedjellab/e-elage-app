import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {  ReglementConsultationComponent } from './reglement-consultation.component';

describe('ReglementConsultationComponent', () => {
  let component: ReglementConsultationComponent;
  let fixture: ComponentFixture<ReglementConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglementConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReglementConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
