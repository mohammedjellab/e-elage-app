import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {  FichePatientComponent } from './fiche-patient.component';

describe('DossierPatientComponent', () => {
  let component: FichePatientComponent;
  let fixture: ComponentFixture<FichePatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichePatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichePatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
