import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MutuelleFaiteComponent } from './mutuelle-faite.component';

describe('MutuelleFaiteComponent', () => {
  let component: MutuelleFaiteComponent;
  let fixture: ComponentFixture<MutuelleFaiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutuelleFaiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MutuelleFaiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
