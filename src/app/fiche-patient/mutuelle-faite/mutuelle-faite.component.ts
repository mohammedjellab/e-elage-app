import { MutuelleFaiteService } from 'src/app/services/mutuelle-faite.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { MutuelleFaite } from 'src/app/models/mutuelleFaite';


@Component({
  selector: 'app-mutuelle-faite',
  templateUrl: './mutuelle-faite.component.html',
  styleUrls: ['./mutuelle-faite.component.scss']
})
export class MutuelleFaiteComponent implements OnInit {

  myMutuelle: MutuelleFaite ={
    observation: '',
    dateMut: null,
    valider: null,
    remplie:  null,
    lpatient_id:  null,
    centreSoin:  null,
    mutuelle:  null,

  }
  @Input() mutuelle;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private mutuelleFaiteService: MutuelleFaiteService) { }

  ngOnInit() {
    console.log('mmmmm',this.mutuelle)

  }

  remplirMutuelle(f){


    this.myMutuelle = {
      observation: f.value.observation,
      dateMut: f.value.dateMut,
      valider: f.value.valider,
      remplie: f.value.remplie,
      lpatient_id:  11,
      centreSoin:  1,
      mutuelle:  2,
    }
    if(f.value.dateMut){
      this.saveMutuelle();
    }

  }

  saveMutuelle(){
    this.mutuelleFaiteService.persist(this.myMutuelle).subscribe(data =>{
      this.event.emit({mutuelle: data});
    });
  }
  closeModal(){
    this.event.emit({mutuelle: null});
  }

  updateMutuelle(){
    this.mutuelleFaiteService.update(this.mutuelle).subscribe(data =>{
      this.event.emit({mutuelle: data});
    });
  }

}
