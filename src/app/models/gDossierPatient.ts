export class GDossierPatient {
    gpatient_id?: number;
    lpatient_id?: number;
    sexe?: string;
    firstname?: string;
    lastname?: string;
    cin?: string;
    tel?: string;
    adresse?: string;
    dateNaissance?: Date;
    ville?: string;
    pays?: string;
    situationFamille?: string;
    civilite?: string;
    active?: boolean;
    centreSoin?: string;
    correspondant?: string;
    nbEnfant?: number;
    mutuelle?: string;
    fixe?: string;
    ramed?: string;
    description?: string;
    recommendation?: string;
    recommendationAutre?: string;
    lastname_arabe?: string;
    firstname_arabe?: string;
    adressePar?: string;
    allergieConnue?: string
    comorbidite?:string;
    fumeur?:string;
    groupSanguin?: string;
    matricule?: string;
    photo?: string;
    profession?: string;
    email?: string;
}

