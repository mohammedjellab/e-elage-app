export class CentreSoin {
    id?: number;
    etablissement: string;
    code: string;
    active: boolean;
    activite: string;
    tel: string;
    adresse: string;
    etatConvention: string;
    ice: string;
    inpe: string;
    logo?: string;

}