export class RdvPaiement {
    id?: number;
    typePaiement: string;
    active?: boolean;
    centreSoin: number;
    dateReglement: string;
    montant: number;
    observation: string;
    consultations: Array<number>;
    patient: number;

}