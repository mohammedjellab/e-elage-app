export class UserType {
    id?: number;
    libelle: string;
    active?: boolean;
    centreSoin?: string;
}