export class Consultation {
    id?: number;
    dateConsultation?: Date;
    montant: number;
    new: boolean;
    lpatient: number;
    centreSoin: number;
    typeConsultation: number;

}