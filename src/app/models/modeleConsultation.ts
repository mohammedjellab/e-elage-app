export class ModeleConsultation {
    id?: number;
    body: string;
    centreSoin: number;
    couleur: string;
    police: string;
    taille: number;
    isGras: boolean;
    format: string;

}