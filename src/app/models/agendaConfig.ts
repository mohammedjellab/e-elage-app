export class AgendaConfig {
    id?: number;
    firstDay: string;
    zoom: number;
    modeAffichage: string;
    heureOuverture: string;
    heureDebutOuvrable: string;
    heureFermeture: string;
    heureFinOuvrable: string;
    granularite: string;


}