export class DossierMedical {
    id?: number;
    comorbidite: number;
    dateConsultation: Date;
    description: string;
    commentaire: string;
    diagnostic: string;
    centreSoin: number;


}