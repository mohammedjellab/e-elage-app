export class Annuaire {
    id?: number;
    nom: string;
    prenom: string;
    tel: string;
    gsm: string;
    email: string;
    typeRep: string;
    fax: string;
    centreSoin?: number;

}