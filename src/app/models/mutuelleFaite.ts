export class MutuelleFaite {
    id?: number;
    observation: string;
    dateMut: Date;
    valider: boolean;
    remplie: boolean;
    lpatient_id: number;
    centreSoin: number;
    mutuelle: number;

}