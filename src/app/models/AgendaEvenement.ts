export class AgendaEvenement {
    id?: number;
    salle: string;
    user: string;
    auteur: string;
    dateDebut: Date;
    dateFin: Date;
    lpatient_id: number;
    info_patient;
    description: string;
    heureArrivee: string;
    heureSortie: string;
    typeConsultation: number;
    praticien: number;
    sansRdv: boolean;
    centreSoin: number;
    legende: number;
    statut: string;
    gpatient_id?: number;
    annule?: boolean;

}