export class User {
    id?: number;
    typeUser: string;
    specialite: string;
    lastname: string;
    firstname: string;
    tel: string;
    adresse: string;
    password: string;
    username: string;
    email: string;
    centreSoin?: string;
    cin: string;
}