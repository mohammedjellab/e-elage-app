export class infoPatientDm {

    lpatient_id: number;
    gpatient_id: number;
    centreSoin: number;
    nom_complet: string;
    mutuelle: string;
    typeConsulation: string;
    nbEnfant: string;
    num_dossier: number;
    diagnostic: string

}