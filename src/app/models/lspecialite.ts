export class LSpecialite {
    id?: number;
    active?: boolean;
    centreSoin: string;
    gspecialiteId?: number;
}

export class Specialite {
    id?: number;
    active?: boolean;
    libelle: string;
    centreSoin?: string;
}