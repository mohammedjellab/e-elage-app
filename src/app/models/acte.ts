export class Acte {
    id?: number;
    libelle: string;
    prix: number;
    active: boolean;
    centreSoin: string;
    familleActe: string;
    couleur: string;
    duree: number;

}