import { ApiService } from './../services/api.service';
import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { DossierMedicalService } from '../services/dossier-medical.service';

@Component({
  selector: 'app-detail-document-medical',
  templateUrl: './detail-document-medical.component.html',
  styleUrls: ['./detail-document-medical.component.scss']
})
export class DetailDocumentMedicalComponent implements OnInit {

  @Input() isFavorite;

  @Output() change = new EventEmitter();
  typeDocuments = [
    {
    name: 'imagerie'
    },
    {
      name: 'bilan'
    },
    {
      name: 'Documents'
    },
    {
      name: 'ECG'
    },
    {
      name: 'ECHO'
    },
    {
      name: 'Courier'
    }
  ]
  typeDocument: string = 'imagerie';
  selectedFile: File;
  files;
  url_images: string ;
  patient:number = 3;
  constructor(
    private dossierMedicalService: DossierMedicalService,
    private apiService: ApiService,

  ) { }

  ngOnInit() {
    console.log('isFavorite',this.isFavorite);
    this.getNameFolderImage();
    this.getFiles();
  }

  getFiles(){
    this.dossierMedicalService.getFiles(this.patient).subscribe(data =>{
      this.files = data
    },err=>{})
  } 

  //recuperer le type document 
  getTypeOfDocument(type){
    this.typeDocument = type;
    console.log('this.typeDocument',this.typeDocument);
    this.change.emit({typeDocument: this.typeDocument});

  }
  /*changeTypeDocument(type){
    this.typeDocument = type.typeDocument;
    console.log('this.typeDocument',type);
  }*/

    //pour recuperer le lien des images
    getNameFolderImage(){
      this.url_images =this.apiService.getUrlImages();
    }
  
    //lors de changement dans iput file
    fileSelected(v){
      this.selectedFile = <File>v.target.files[0];
    }
    //traitement et evoi de fichier avec les informations 
    onUpload(){
      let extention = this.selectedFile.type;
          extention = extention.split('/').pop();
      let fd = new FormData();
      fd.append('image',this.selectedFile,this.selectedFile.name)
      fd.append('typeDocument',this.typeDocument);
      fd.append('idDossierMedical','1');
      fd.append('extention',extention);
      this.dossierMedicalService.uploadFile(fd).subscribe(res =>{
        this.files=[res,...this.files];
        console.log('res',res);
      });
    }

    /**/
  

}
