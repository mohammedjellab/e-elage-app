import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDocumentMedicalComponent } from './detail-document-medical.component';

describe('DetailDocumentMedicalComponent', () => {
  let component: DetailDocumentMedicalComponent;
  let fixture: ComponentFixture<DetailDocumentMedicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDocumentMedicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDocumentMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
