import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PagesAcceuilLoginMobileComponent } from './pages-acceuil-login-mobile.component';


describe('LoginMobileComponent', () => {
  let component: PagesAcceuilLoginMobileComponent;
  let fixture: ComponentFixture<PagesAcceuilLoginMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesAcceuilLoginMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesAcceuilLoginMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
