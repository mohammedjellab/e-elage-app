import { PagesLoginMobileComponent } from './pages-login-mobile.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('PagesLoginMobileComponent', () => {
  let component: PagesLoginMobileComponent;
  let fixture: ComponentFixture<PagesLoginMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesLoginMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesLoginMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
