import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesRegisterMobileComponent } from './pages-register-mobile.component';

describe('LoginMobileCreateAccountContentComponent', () => {
  let component: PagesRegisterMobileComponent;
  let fixture: ComponentFixture<PagesRegisterMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesRegisterMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesRegisterMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
