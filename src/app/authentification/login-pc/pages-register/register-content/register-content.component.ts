import { User } from '../../../../models/User';
import { UserTypeService } from '../../../../services/user-type.service';
import { DefaultService } from '../../../../services/default.service';
import { UserService } from '../../../../services/user.service';
import { SpecialiteService } from '../../../../services/specialite.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../services/auth';

@Component({
  selector: 'app-register-content',
  templateUrl: './register-content.component.html',
  styleUrls: ['./register-content.component.scss']
})
export class RegisterContentComponent implements OnInit {

  myUser: User ={
    typeUser: '',
    specialite: '',
    lastname: '',
    firstname: '',
    tel: '',
    adresse: '',
    password: '',
    username: '',
    email: '',
    centreSoin: '',
    cin: ''
  }

  prefix: any = {
    "speciaite": "/api/specialites/",
    "users": "/api/users/",
    "centreSoin": "/api/centre_soins/1",
    "ville":"/api/villes/",
    "pays": "/api/pays/",
    "mutuelle": "/api/mutuelles/",
    "typeUser": "/api/type_users/"
  };
  typeUsers;
  specialites;
  is_medecin: boolean;
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre compte a été créer avec succès on va vous contactez .',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  selectedFile;
  constructor(
    private specialiteService: SpecialiteService,
              private userService: UserService,
              private authService: AuthService,
              private defaultService: DefaultService,
              private userTypeService: UserTypeService,
  ) { }

  ngOnInit() {
    this.findAllTypeuserActive();
    this.findALLSpecialite();
    console.log('dddd',this.prefix.speciaite)
  }

  getTypeUser(type){
    console.log('value',type.target.value);
    if(type.target.value=="/api/type_users/1"){
      this.is_medecin = true;
    }else{
      this.is_medecin = false;
    }

    //this.myUser.typeUser = type.target.value;
  }
  findAllTypeuserActive(){
    this.userTypeService.findAll().subscribe(typeUsers =>{
      this.typeUsers=typeUsers;
      console.log('users',this.typeUsers);
    })
  }
  findALLSpecialite(){
    this.specialiteService.findALLSpecialite().subscribe(specialites =>{
      this.specialites=specialites;
      console.log('users',specialites);
    })
  }

  toggleSpecialite(myUser){
    //console.log('user',myUser)
  }

  //lors de changement dans iput file
  fileSelected(v){
    this.selectedFile = <File>v.target.files[0];
    console.log('fileSelected',this.selectedFile)
  }
  
  //traitement et evoi de fichier avec les informations 
  onUpload(user){
    //photo
    let fd = new FormData();
    if(this.selectedFile){
      let extention = this.selectedFile.type;
      extention = extention.split('/').pop();
      fd.append('image',this.selectedFile,this.selectedFile.name)
      fd.append('extention',extention);
      this.userService.uploadFile(fd,user).subscribe(res =>{
        console.log('onUpload',res);
      });
    }
  }

  remplirMyUser(f: NgForm){
    this.myUser = {
      typeUser: f.value.typeUser,
      specialite: f.value.specialite,
      lastname: f.value.lastname,
      firstname: f.value.firstname,
      tel: f.value.tel,
      adresse: f.value.adresse,
      password: f.value.password,
      username: f.value.username,
      email: f.value.email,
      centreSoin: "/api/centre_soins/1",
      cin: f.value.cin,
    }
    this.defaultService.deleteAttributeEmpty(this.myUser);
    console.log('form',this.myUser)
    this.persist();
  }

  persist(){
    console.log('ddd',this.myUser)

    this.authService.register(this.myUser).subscribe(user =>{
      this.defaultAlerts[0].show = true;
      console.log('user',user)
      this.onUpload(user['id']);
    },err=>{

    }
    );
  }
}
