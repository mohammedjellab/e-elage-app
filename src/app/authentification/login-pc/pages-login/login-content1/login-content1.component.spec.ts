import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginContent1Component } from './login-content1.component';


describe('LoginMobileLoginContentComponent', () => {
  let component: LoginContent1Component;
  let fixture: ComponentFixture<LoginContent1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginContent1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginContent1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
