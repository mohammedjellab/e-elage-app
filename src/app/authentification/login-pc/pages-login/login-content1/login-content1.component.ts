import { NgForm } from '@angular/forms';
import { DefaultService } from '../../../../services/default.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { UserTypeService } from '../../../../services/user-type.service';
import { UserService } from '../../../../services/user.service';
import { User } from '../../../../models/User';
import { SpecialiteService } from './../../../../services/specialite.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../services/auth';

@Component({
  selector: 'app-login-content1',
  templateUrl: './login-content1.component.html',
  styleUrls: ['./login-content1.component.scss']
})
export class LoginContent1Component implements OnInit {

  user={
    password: "1234",
	  username: "benjelloun"
  }
  error_auth: string;
  dismissible = true;

  constructor(private specialiteService: SpecialiteService,
    private userService: UserService,
    private authService: AuthService,
    private defaultService: DefaultService,
    private userTypeService: UserTypeService,
    private router: Router,
    //private l: Storage
    private route: ActivatedRoute
    ) { }

    ngOnInit(){

    }
  remplirUser(f: NgForm){
    this.user ={
      username: f.value.username,
      password: f.value.password
    }
    console.log('login',this.user);
    console.log('login',f);

    this.login();
  }
  login(){
    /*let us = {"id":7,"username":"benjelloun","lastname":"Mohamed","firstname":"Benjelloun","tel":"222","roles":["ROLE_USER"],"centreSoin":1}
     this.router.navigate(['calendar'], { queryParams: us})
      localStorage.setItem('user',JSON.stringify(us))*/
    this.authService.login(this.user).subscribe(user =>{
      console.log('login',user);
      let currentUrl = localStorage.getItem('currentUrl');
      
      this.router.navigate(['calendar'], { queryParams: user})
      localStorage.setItem('user',JSON.stringify(user))
      },
      err =>{
        this.error_auth='Votre login ou mot de passe et incorrecte'
        console.log('err',err);
      }
    );
    //let user =this.myUser;
    //this.router.navigate(['specialite'], { queryParams: user})
  }


}
