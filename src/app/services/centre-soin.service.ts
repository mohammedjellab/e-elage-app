import { CentreSoin } from './../models/centreSoin';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CentreSoinService {

  urlPlatform = this.api.getUrlPlatform()+"/centre_soins";
  url = this.api.getUrl()+"/centre_soins";
  constructor(private api: ApiService, private http: HttpClient) { }

  persist(centreSoin){
    return this.http.post<CentreSoin>(`${this.url}/insert`,centreSoin);
  }

  update(centreSoin){
    return this.http.put<CentreSoin>(`${this.url}/update/${centreSoin.id}`,centreSoin);
  }

  findAll(){
    return this.http.get(`${this.url}/list`);
  }

  delete(centreSoin){
    return this.http.get<CentreSoin>(`${this.url}/delete/${centreSoin.id}`);
  }

}
