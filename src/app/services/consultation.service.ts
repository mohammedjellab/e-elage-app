import { HttpClient } from '@angular/common/http';
import { Consultation } from './../models/consultation';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ConsultationService {

  urlPlatform = this.api.getUrlPlatform()+"/consultation";
  url = this.api.getUrl()+"/consultation";
  constructor(private api: ApiService,private http: HttpClient) { 

  }
  persist(consultation){
    return this.http.post<Consultation>(`${this.url}/insert`,consultation);
  }

  findAllByPatient(patient){
    return this.http.get(`${this.url}/list/${patient}`);
  }

  situationFinanciere(patient){
    return this.http.get(`${this.url}/situation/${patient}`);
  }
  
  listReglementByLpatient(lpatient){
    return this.http.get(`${this.url}/reglement/${lpatient}`);
  }

  
  update(motif){
    return this.http.put(`${this.urlPlatform}/${motif.id}`,motif);
  }

  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }
  listConsltations(patient){
    return this.http.get(`${this.urlPlatform}/list/${patient}`);
  }

  findRegelement(search){
    return this.http.post(`${this.url}/reglement/list`,search);
  }
  mesInterventions(search){
    return this.http.post(`${this.url}/reglement/interventions`,search);
  }
  
}
