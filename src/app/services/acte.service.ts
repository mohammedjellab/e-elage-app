import { Acte } from './../models/acte';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActeService {

  urlPlatform = this.api.getUrlPlatform()+"/actes";
  url = this.api.getUrl()+"/actes";
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(userType){
    return this.http.post<Acte>(this.urlPlatform,userType);
  }
  findAllActive(){
    return this.http.get(this.urlPlatform);
  }

  joinWithFamille(){
    return this.http.get(`${this.url}/join-famille`);
  }
  
}
