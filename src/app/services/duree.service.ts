import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DureeService {

  urlPlatform = this.api.getUrlPlatform()+"/durees";
  url = this.api.getUrl()+"/durees";
  constructor(private api: ApiService,private http: HttpClient) { }

  findAll(){
    return this.http.get(this.urlPlatform);
  }
  
}
