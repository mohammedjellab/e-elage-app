import { Salle } from './../models/salle';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SalleService {

  urlPlatform = this.api.getUrlPlatform()+"/salles";
  url = this.api.getUrl()+"/salles";
  constructor(private api: ApiService, private http: HttpClient) {

  }

  /*persist(salle){
    return this.http.post<Salle>(this.urlPlatform,salle);
  }*/

  findAll(){
    return this.http.get(this.urlPlatform);
  }

  findAllByCentre(centre){
    return this.http.get(`${this.url}/${centre}/list`);
  }
  delete(id){
    return this.http.get(`${this.url}/delete/${id}`);
  }
  update(salle){
    return this.http.put(`${this.url}/update/${salle.id}`,salle);
  }
  persist(salle){
    return this.http.post<Salle>(`${this.url}/insert`,salle);
  }
  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }

  showPerso(id){
    return this.http.get(`${this.url}/${id}`);
  }

  getSallesByCentre(centre){
    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    }
    
    const requestOptions = {                                                                                                                                                                                 
      headers: new Headers(headerDict), 
    };
    ////////////
    const optionRequete = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin':'*'
      })
    };
    return this.http.get(`${this.url}/centre/${centre}`);
  }

  getUsers(user){
    return this.http.get(`https://jsonplaceholder.typicode.com/users/${user}`);
  }

  getPosts(){
    return this.http.get(`https://jsonplaceholder.typicode.com/posts`);
  }
}
