import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Unite } from './../models/unite';


@Injectable({
  providedIn: 'root'
})
export class ConstanteService {

  urlPlatform = this.api.getUrlPlatform()+"/surveillance/patient";
  url = this.api.getUrl()+"/surveillance/patient";
  
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(constante){
    return this.http.post<Unite>(`${this.url}/insert`,constante);
  }
  
  findAll(){
    return this.http.get(this.url);
  }


  
  update(unite){
    return this.http.put<Unite>(`${this.url}/update/${unite.id}`,unite);
  }

  delete(unite){
    return this.http.get<Unite>(`${this.url}/delete/${unite}`);
  }

  getConstantesMiniGraph(lpatient){
    return this.http.get(`${this.url}/mini/graph/${lpatient}`);
  }
}
