import { Mutuelle } from './../models/mutuelle';
import { Acte } from './../models/acte';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MutuelleService {

  urlPlatform = this.api.getUrlPlatform()+"/mutuelles";
  url = this.api.getUrl()+"/mutuelles";
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(mutuelle){
    return this.http.post<Mutuelle>(`${this.url}/insert`,mutuelle);
  }
  findAll(){
    return this.http.get(this.urlPlatform);
  }
  findAllMutuelle(){
    return this.http.get(`${this.url}/list`);
  }
  searchMutuelle(term){
    return this.http.get(`${this.url}/search/${term}`)
  }

  update(mutuelle){
    return this.http.put(`${this.url}/update/${mutuelle.id}`,mutuelle);
  }

  delete(id){
    return this.http.get(`${this.url}/delete/${id}`)
  }
}
