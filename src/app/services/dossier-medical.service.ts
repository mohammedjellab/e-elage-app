import { GDossierPatient } from '../models/gDossierPatient';
import { LDossierPatient } from '../models/lDossierPatient';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { DossierMedical } from '../models/dossierMedical';

@Injectable({
  providedIn: 'root'
})
export class DossierMedicalService {

  urlPlatform = this.api.getUrlPlatform()+"/dmedical";
  url = this.api.getUrl()+"/dmedical";
  lpatient = this.api.getUrl()+"/lpatient";
  gpatient = this.api.getUrl()+"/gpatient";
  constructor(private api: ApiService, private http: HttpClient) { 

  }

 
  uploadFile(file){
    return this.http.post(`${this.url}/upload`,file);
  }
  
  getFiles($idDossier){
    return this.http.get(`${this.url}/get/files/${$idDossier}`);
  }

  updateDescriptionInDm(idDossier,text){
    return this.http.post(`${this.url}/consultation/description/update/${idDossier}`,{'description':text});
  }
  createDm(dm){
    return this.http.post<DossierMedical>(`${this.url}/insert`,dm);
  }

  getInformationForDM(idDossier){
    return this.http.get(`${this.url}/show/${idDossier}`);
  }

  saveComorbidite(gpatient,comorbidite){
    return this.http.get(`${this.url}/insert/comorbidite/${gpatient}/${comorbidite}`);
  }

  saveDiagnostic(lpatient,diagnostic){
    return this.http.get(`${this.url}/insert/diagnostic/${lpatient}/${diagnostic}`);
  }

  

 
}
