import { MutuelleFaite } from './../models/mutuelleFaite';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MutuelleFaiteService {

  urlPlatform = this.api.getUrlPlatform()+"/mutuelle_faites";
  url = this.api.getUrl()+"/mutuelle_faites";
  constructor(private api: ApiService,private http: HttpClient) { 

  }

  persist(mut){
    return this.http.post<MutuelleFaite>(`${this.url}/insert`,mut);
  }

  findAll(){
    return this.http.get(this.urlPlatform);
  }

  update(motif){
    return this.http.put(`${this.urlPlatform}/${motif.id}`,motif);
  }

  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }
  findByPatient(patient){
    return this.http.get(`${this.url}/list/${patient}`);
  }
}
