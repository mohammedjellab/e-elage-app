import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  /*urlPlatform ="http://localhost:8000/api";
  url ="http://localhost:8000";*/
  /*urlPlatform ="http://192.168.23.135/elageback_dev/public/api";
  url ="http://192.168.23.135/elageback_dev/public";*/
   /*urlPlatform ="http://192.168.23.135/elageback2/public/api";
  url ="http://192.168.23.135/elageback2/public";*/
  urlPlatform ="http://127.0.0.1:8000/api";
  url ="http://127.0.0.1:8000";
  folder_images= "/images/products";
  folder_images_patient= "/images/patient";
  folder_images_antecedent= "/images/antecedent";


  constructor(){ }

  getUrl(){
    return this.url;
  }
  getUrlPlatform(){
    return this.urlPlatform;
  }

  getUrlImages(){
    return this.url+this.folder_images;
  }

  getUrlImagesPatient(){
    return this.url+this.folder_images_patient;
  }
  getUrlImagesPatientAntecedent(){
    return this.url+this.folder_images_antecedent;
  }

}
