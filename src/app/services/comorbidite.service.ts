import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Comorbidite } from '../models/comorbidite';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ComorbiditeService {

  urlPlatform = this.api.getUrlPlatform()+"/comorbidites";
  url = this.api.getUrl()+"/comorbidites";
  constructor(private api: ApiService, private http: HttpClient) {

  }

  persist(comorbidite){
    return this.http.post<Comorbidite>(`${this.url}/insert`,comorbidite);
  }

  findAll(){
    return this.http.get(`${this.url}/list`);
  }

  update(comorbidite){
    return this.http.put<Comorbidite>(`${this.url}/update/${comorbidite.id}`,comorbidite);
  }
  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }
  delete(id){
    return this.http.get(`${this.url}/delete/${id}`)
  }
}
