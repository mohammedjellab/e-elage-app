import { AgendaEvenement } from './../models/AgendaEvenement';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  urlPlatform = this.api.getUrlPlatform()+"/agenda_evenements";
  url = this.api.getUrl()+"/agenda_evenements";
  constructor(private api: ApiService,private http: HttpClient) { 

  }
  persist(agenda){
    return this.http.post<AgendaEvenement>(`${this.url}/insert`,agenda);
  }

  findAll(){
    return this.http.get(this.urlPlatform);
  }

  update(agenda,id){
    return this.http.put<AgendaEvenement>(`${this.url}/update/${id}`,agenda);
  }
  updateStatus(status,id,legende){
    return this.http.get<AgendaEvenement>(`${this.url}/update/${id}/${status}/${legende}`);
  }

  annulerRdv(id){
    return this.http.get(`${this.url}/annuler/rdv/${id}`);
  }

  show(id){
    return this.http.get(`${this.url}/show/${id}`);
  }
  listConsltations(patient){
    return this.http.get(`${this.urlPlatform}/list/${patient}`);
  }

  nbreRdvByTypeConsultation(centre){
    return this.http.get(`${this.url}/list/${centre}`);
  }

  searchConsultation(search){
    return this.http.post(`${this.url}/listrdv`,search);

  }

  rdvByPatient(rdv){
    return this.http.get(`${this.url}/rdv_patient/${rdv.patient}/${rdv.id}`);
  }
  ////calendar
  
  rdvCalendar(search){
    return this.http.post(`${this.url}/calendarrdv`,search);

  }

  getNbreActeByDayAndTypeConsultation(day,acte){
    return this.http.get(`${this.url}/nbre/${day}/${acte}`);
  }
  getNbreRdvInIterval(start,end){
    return this.http.get(`${this.url}/interval/${start}/${end}`);
  }
  
}
