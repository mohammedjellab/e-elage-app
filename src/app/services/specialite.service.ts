import { GSpecialite } from '../models/gspecialite';
import { LSpecialite,Specialite } from '../models/lspecialite';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpecialiteService {

  urlPlatform = this.api.getUrlPlatform()+"/specialites";
  urlPlatformGSpecialite = this.api.getUrlPlatform()+"/gspecialites";
  url = this.api.getUrl()+"/specialites";
  gspecialite = this.api.getUrl()+"/gspecialite";
  lspecialite = this.api.getUrl()+"/lspecialite";
  constructor(private api: ApiService,private http: HttpClient) { }
//`
  persistGSpecialite(Gspecialite){
    return this.http.post<GSpecialite>(`${this.gspecialite}/insert`,Gspecialite);
  }
  
  findGSpecialite(){
    return this.http.get(`${this.gspecialite}/list`);
  }
  findLSpecialiteALL(){
    return this.http.get(`${this.lspecialite}/list`);
  }
  findByCentre(centreSoin){
    return this.http.get(`${this.lspecialite}/list/${centreSoin}`);
  }
  
  findALLSpecialite(){
    return this.http.get(`${this.url}/list`);
  }
  /////////////////////////////
  findAll(){
    return this.http.get(`${this.url}/list`);
  }
  persist(specialite){
    return this.http.post<Specialite>(`${this.url}/insert`,specialite);
  }
  update(specialite){
    return this.http.put<Specialite>(`${this.url}/update/${specialite.id}`,specialite);
  }
  delete(id){
    return this.http.get<Specialite>(`${this.url}/delete/${id}`);
  }
  searchSpecialite(term){
    return this.http.get(`${this.url}/search/${term}`);
  }


}
