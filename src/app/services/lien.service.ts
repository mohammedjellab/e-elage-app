import { Router } from '@angular/router';
import { FamilleActe } from './../models/familleActe';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LienService {

  urlPlatform = this.api.getUrlPlatform()+"/famille_actes";
  url = this.api.getUrl()+"/famille_actes";
  constructor(private api: ApiService,private http: HttpClient, private router: Router) {

  }

  goToPage(patient,page){
    console.log('patient',patient)

    //this.router.navigate(['rdv'], { queryParams: patient});

      this.router.navigate([page], { queryParams: patient});

  }



  


  removePaddingSetting(){
    let d = document.getElementsByClassName('app-content--inner p-4')[0];
    d.classList.remove('p-4');
    d.classList.add("app-inner-content-layout--main");
    d.classList.add("p-0");
  }

  addPaddingSetting(){
    let d = document.getElementsByClassName('app-content--inner p-0')[0];
    d.classList.remove('p-0');
    d.classList.add("p-4");
  }


}
