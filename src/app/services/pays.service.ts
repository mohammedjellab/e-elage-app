import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  urlPlatform = this.api.getUrlPlatform()+"/pays";
  url = this.api.getUrl()+"/pays";
  constructor(private api: ApiService, private http: HttpClient) { }

  searchPays(term){
    return this.http.get(`${this.url}/search/${term}`);
  }
}
