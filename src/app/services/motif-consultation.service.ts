import { MotifConsultation } from './../models/motifConsultation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MotifConsultationService {
  
  
  urlPlatform = this.api.getUrlPlatform()+"/motif_consultations";
  url = this.api.getUrl()+"/motif_consultations";
  constructor(private api: ApiService,private http: HttpClient) { 

  }

  persistOld(motif){
    return this.http.post<MotifConsultation>(this.urlPlatform,motif);
  }

  findAllOld(){
    return this.http.get(this.urlPlatform);
  }

  updateOld(motif){
    return this.http.put(`${this.urlPlatform}/${motif.id}`,motif);
  }

  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }

  //////
  
  update(mutuelle){
    return this.http.put<MotifConsultation>(`${this.url}/update/${mutuelle.id}`,mutuelle);
  }
  findAll(){
    return this.http.get(`${this.url}/list`);

  }
  delete(id){
    return this.http.get<MotifConsultation>(`${this.url}/delete/${id}`)
  }
  findAllByCentre(centre){
    return this.http.get(`${this.url}/list/${centre}`);
  }
  persist(mutuelle){
    return this.http.post<MotifConsultation>(`${this.url}/insert`,mutuelle);
  }
}
