import { GDossierPatient } from '../models/gDossierPatient';
import { LDossierPatient } from '../models/lDossierPatient';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { infoPatientDm } from '../models/dossierMedicalPatient';

@Injectable({
  providedIn: 'root'
})
export class DossierPatientService {

  urlPlatform = this.api.getUrlPlatform()+"/patients";
  url = this.api.getUrl()+"/patients";
  lpatient = this.api.getUrl()+"/lpatient";
  gpatient = this.api.getUrl()+"/gpatient";
  constructor(private api: ApiService, private http: HttpClient) { 

  }

  uploadFile(file,gpatient_id){
    return this.http.post(`${this.gpatient}/upload/${gpatient_id}`,file);
  }

  persistGDossierPatient(patient){
    return this.http.post<GDossierPatient>(`${this.gpatient}/insert`,patient);
  }

  updateGDossierPatient(patient){
    return this.http.post<GDossierPatient>(`${this.gpatient}/update/${patient.gpatient_id}`,patient);
  }

  findAll(){
    return this.http.get(`${this.url}/list`);
  }
  show(id,centreSoin){
    return this.http.get(`${this.url}/${id}/${centreSoin}`);
  }

  
  persistLDossierPatient(patient){
    return this.http.post<LDossierPatient>(`${this.lpatient}/insert`,patient);
  }

  updateLDossierPatient(patient){
    return this.http.post<LDossierPatient>(`${this.lpatient}/update/${patient.lpatient_id}`,patient);
  }

  getFieldPatient(){
    return this.http.get(`${this.lpatient}/config`);
  }
  searchPatient(term){
    return this.http.get(`${this.gpatient}/search/${term}`);
  }
  searchPatients(arr){
    return this.http.post(`${this.gpatient}/search`,arr);
  }

  //dossier médical afficher lpatient
  showLpatient(id,centreSoin){
    return this.http.get<infoPatientDm>(`${this.lpatient}/${id}/${centreSoin}`);
  }

 
}
