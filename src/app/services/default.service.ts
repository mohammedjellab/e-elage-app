import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DefaultService {

  minute = 10;
  urlPlatform = this.api.getUrlPlatform()+"/default";
  url = this.api.getUrl()+"/default";
  constructor(private api: ApiService, private http: HttpClient) { }


  deleteAttributeEmpty(arr){
    $.each(arr, function(key,value){
      if(value ==="" || value ===null){
        delete arr[key];
      }
    });
  }

  getNowDate(){
    let today = new Date();
    let dd = String(today. getDate()). padStart(2, '0');
    let mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
    let yyyy = today. getFullYear();
    return   mm+ '/' + dd + '/' + yyyy;
  }
  getDateFormat(today){
    let dd = String(today. getDate()). padStart(2, '0');
    let mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
    let yyyy = today. getFullYear();
    return   yyyy + '-' + mm+ '-' + dd ;
  }
  getDateFormatMonthDayYear(today){
    let dd = String(today. getDate()). padStart(2, '0');
    let mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
    let yyyy = today. getFullYear();
    return    mm+ '-' +  dd+ '-' + yyyy ;
  }
  getDateFormatMonthDayYearSlash(today){
    let dd = String(today. getDate()). padStart(2, '0');
    let mm = String(today. getMonth() + 1). padStart(2, '0'); //January is 0!
    let yyyy = today. getFullYear();
    return    mm+ '/' +  dd+ '/' + yyyy ;
    //return today;
  }
//Y-m-d
  getNowHour(){
    let today = new Date();
    console.log('hour',today);
    let heure = String(today. getHours()). padStart(2, '0');
    let minute = String(today. getMinutes() + 1). padStart(2, '0'); 
    return  heure + ':' + minute ;
  }

  getNowHourAndConfig(){
    let today = new Date();
    console.log('hour',today);
    let heure = String(today. getHours()). padStart(2, '0');
    let minute = String(today. getMinutes() + 1). padStart(2, '0'); 
    return  heure + ':' + minute ;
  }

  getSysdate(){
    this.http.get(`${this.url}/sysdate`);
  }

}
