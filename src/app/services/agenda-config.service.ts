import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { AgendaConfig } from '../models/agendaConfig';

@Injectable({
  providedIn: 'root'
})
export class AgendaConfigService {

  urlPlatform = this.api.getUrlPlatform()+"/actes";
  url = this.api.getUrl()+"/agenda_config";
  constructor(private api: ApiService,private http: HttpClient) { }
  //`

  update(agendaConfig){
    return this.http.post<AgendaConfig>(`${this.url}/update/${agendaConfig.id}`,agendaConfig);
  }
  persist(agendaConfig){
    return this.http.post<AgendaConfig>(`${this.url}/insert`,agendaConfig);
  }

  findByCentreSoin(centreSoin){
    return this.http.get<AgendaConfig>(`${this.url}/${centreSoin}`);
  }


  
}
