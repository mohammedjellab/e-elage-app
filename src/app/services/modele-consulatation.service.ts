import { ModeleConsultation } from './../models/modeleConsultation';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModeleConsulatationService {

  urlPlatform = this.api.getUrlPlatform()+"/modele_consultation";
  url = this.api.getUrl()+"/modele_consultation";
  constructor(private api: ApiService,private http: HttpClient) {

  }

  update(modele){
    return this.http.post<ModeleConsultation>(`${this.url}/update`,modele);
  }

  findByCentreSoin(centreSoin){
    return this.http.get(`${this.url}/show/${centreSoin}`);
  }

  searchFamille(term){
    return this.http.get(`${this.url}/search/${term}`);
  }



}
