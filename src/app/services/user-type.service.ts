import { UserType } from './../models/userType';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  urlPlatform = this.api.getUrlPlatform()+"/type_users";
  url = this.api.getUrl()+"/type_users";
  
  constructor(private api: ApiService,private http: HttpClient) { }
//`
  persist(userType){
    return this.http.post<UserType>(this.urlPlatform,userType);
  }
  findAllActive(){
    return this.http.get(this.urlPlatform);
  }

  findAll(){
    let headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json, text/javascript ',
      'Access-Control-Allow-Origin': '*',
      //'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Headers': '*'
    };
    return this.http.get(`${this.url}/list`,{headers});
  }
  delete(id){
    return this.http.get(`${this.url}/delete/${id}`);
  }

  update(userType){
    return this.http.put(`${this.url}/update/${userType.id}`,userType);
  }

}
