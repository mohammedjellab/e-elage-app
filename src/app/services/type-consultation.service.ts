import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { TypeConsultationLocal } from '../models/typeConsultationLocal';
import { TypeConsultationGlobal } from '../models/typeConsultationGlobal';

@Injectable({
  providedIn: 'root'
})
export class TypeConsultationService {

  urlPlatform = this.api.getUrlPlatform()+"/type_consultations";
  url = this.api.getUrl()+"/type_consultations";
  constructor(private api: ApiService, private http: HttpClient) {

  }

  persistLocal(typeConsultation){
    return this.http.post<TypeConsultationLocal>(`${this.url}/local/insert`,typeConsultation);
  }
  persistGlobal(typeConsultation){
    return this.http.post<TypeConsultationGlobal>(`${this.url}/global/insert`,typeConsultation);
  }

  updateLocal(typeConsultation){
    return this.http.put<TypeConsultationLocal>(`${this.url}/local/update/${typeConsultation.id_local}`,typeConsultation);
  }
  updateGlobal(typeConsultation){
    return this.http.put<TypeConsultationGlobal>(`${this.url}/global/update/${typeConsultation.id_global}`,typeConsultation);
  }
  deleteLocal(id_local){
    return this.http.get(`${this.url}/local/delete/${id_local}`);
  }

  findAll(){
    return this.http.get(`${this.url}/list`);
  }
  findAllByCentre(centreSoin){
    return this.http.get(`${this.url}/${centreSoin}/list`);
  }

  update(type){
    return this.http.put(`${this.urlPlatform}/${type.id}`,type);
  }
  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }

  findTypeConsultationByLibelle(libelle){
    return this.http.get<TypeConsultationGlobal>(`${this.url}/global/${libelle}`);
  }

  


}