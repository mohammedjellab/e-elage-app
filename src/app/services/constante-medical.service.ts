import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Unite } from './../models/unite';


@Injectable({
  providedIn: 'root'
})
export class ConstanteMedicalService {

  urlPlatform = this.api.getUrlPlatform()+"/paramserveillance";
  url = this.api.getUrl()+"/paramserveillance";
  
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(unite){
    return this.http.post<Unite>(`${this.url}/insert`,unite);
  }
  findAllConstante(){
    return this.http.get(this.url);
  }


  
  update(unite){
    return this.http.put<Unite>(`${this.url}/update/${unite.id}`,unite);
  }

  delete(unite){
    return this.http.get<Unite>(`${this.url}/delete/${unite}`);
  }
}
