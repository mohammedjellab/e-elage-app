import { Annuaire } from './../models/annuaire';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnnuaireService {

  urlPlatform = this.api.getUrlPlatform()+"/annuaires";
  url = this.api.getUrl()+"/annuaires";
  constructor(private api: ApiService,private http: HttpClient) { 

  }

  findAllByCentre(centre){
    return this.http.get(`${this.url}/list/${centre}`);
  }
  persist(annuaire){
    return this.http.post<Annuaire>(`${this.url}/insert`,annuaire);
  }
  update(annuaire){
    return this.http.put<Annuaire>(`${this.url}/update/${annuaire.id}`,annuaire);
  }

  delete(id){
    return this.http.get<Annuaire>(`${this.url}/delete/${id}`)
  }

}
