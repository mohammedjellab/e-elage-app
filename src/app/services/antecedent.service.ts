import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Antecedent } from '../models/antecedent';

@Injectable({
  providedIn: 'root'
})
export class AntecedentService {


  urlPlatform = this.api.getUrlPlatform()+"/antecedents";
  url = this.api.getUrl()+"/antecedents";
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(ant){
    return this.http.post<Antecedent>(`${this.url}/insert`,ant);
  }
  findAll(){
    return this.http.get(this.urlPlatform);
  }
  findAllAntecedent(){
    return this.http.get(`${this.url}/list`);
  }
  searchMutuelle(term){
    return this.http.get(`${this.url}/search/${term}`)
  }

  update(ant){
    return this.http.put<Antecedent>(`${this.url}/update/${ant.id}`,ant);
  }

  saveAntecedantPatient(ant,patient){
    return this.http.get(`${this.url}/patient/${ant}/${patient}`);
  }
  getAntecedantByPatient(patient){
    return this.http.get(`${this.url}/patient/${patient}`);
  }
  delete(id){
    return this.http.get<Antecedent>(`${this.url}/delete/${id}`)
  }

}
