import { FamilleActe } from './../models/familleActe';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FamilleActeService {

  urlPlatform = this.api.getUrlPlatform()+"/famille_actes";
  url = this.api.getUrl()+"/famille_actes";
  constructor(private api: ApiService,private http: HttpClient) {

  }

  persistOld(familleActe){
    return this.http.post<FamilleActe>(this.urlPlatform,familleActe);
  }

  findAllActive(){
    return this.http.get(this.urlPlatform);
  }
  findAllFamilleActe(){
    return this.http.get<FamilleActe[]>(`${this.url}/list`);
  }


  searchFamille(term){
    return this.http.get(`${this.url}/search/${term}`);
  }

  searchHeroes(term: string): Observable<FamilleActe[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<FamilleActe[]>(`${this.url}/${term}`);
  }

  update(mutuelle){
    return this.http.put<FamilleActe>(`${this.url}/update/${mutuelle.id}`,mutuelle);
  }

  delete(id){
    return this.http.get<FamilleActe>(`${this.url}/delete/${id}`);
  }

  persist(mutuelle){
    return this.http.post<FamilleActe>(`${this.url}/insert`,mutuelle);
  }

}
