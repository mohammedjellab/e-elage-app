import { ExamenComplementaire } from './../models/examenComplementaire';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ExamenComplementaireService {

  urlPlatform = this.api.getUrlPlatform()+"/examen_complementaires";
  url = this.api.getUrl()+"/examen_complementaires";
  constructor(private api: ApiService, private http: HttpClient) {

  }

  persist(examenComplementaire){
    return this.http.post<ExamenComplementaire>(this.urlPlatform,examenComplementaire);
  }

  findAll(){
    return this.http.get(this.urlPlatform);
  }

  update(examenComplementaire){
    return this.http.put(`${this.urlPlatform}/${examenComplementaire.id}`,examenComplementaire);
  }
  show(id){
    return this.http.get(`${this.urlPlatform}/${id}`);
  }
}
