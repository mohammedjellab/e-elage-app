import { User } from './../models/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  urlPlatform = this.api.getUrlPlatform()+"/users";
  url = this.api.getUrl()+"/users";
  url_login = this.api.getUrl()+"/login";


  constructor(private api: ApiService,private http: HttpClient) { }
  uploadFile(file,user){
    return this.http.post(`${this.url}/upload/${user}`,file);
  }
  //`
  persist(userType){
    return this.http.post<User>(this.urlPlatform,userType);
  }
  findAllActive(){
    return this.http.get(this.urlPlatform);
  }

  findALL(){
    return this.http.get(`${this.url}/list`);
  }

  login(user){
    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    }

    const requestOptions = {
      headers: new Headers(headerDict),
    };
    ////////////
    const optionRequete = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin':'*',
        'Accept':'application/json, text/plain',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS'
      })
    };



    return this.http.get(this.url_login,optionRequete);

  }
  getHedears(){
    let headers = new HttpHeaders().set("Access-Control-Allow-Origin", "*");
    headers.append('Access-Control-Allow-Methods','GET, POST, DELETE, PUT, OPTIONS');
    headers.append('Access-Control-Allow-Headers',"Origin, Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With, Access-Control-Request-Method, Access-Control-Request-Headers");
    headers.append('Access-Control-Allow-Credentials','true');
    headers.append("cache-control","no-cache");
    headers.append("Accept","application/json, text/plain");
    headers.append("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    return headers;
  }

  searchCorrespondant(term: string){
    return this.http.get(`${this.url}/correspandant/search/${term}`);
  }
  getCorrespondant(){
    return this.http.get(`${this.url}/correspandant/list`)
  }
  getCorrespondantByEtablissment(centre){
    return this.http.get(`${this.url}/correspandant/list/${centre}`)
  }
  getMedecin(){
    return this.http.get(`${this.url}/medecin/list`)
  }

  getMedecinByEtablissment(centre){
    return this.http.get(`${this.url}/medecin/list/${centre}`)
  }

}
