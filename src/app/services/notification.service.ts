import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  
  constructor() { }

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'la specialité ajouté avec succès.'
    },
    /*{
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.'
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.'
    },*/
    {
      type: 'warning',
      msg: 'Cette specialité déjà existe.'
    }
  ];
  alerts = this.defaultAlerts;

  reset(): void {
    this.alerts = this.defaultAlerts;
  }

  onClosed(dismissedAlert: any): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
  alertSuccess(){
    return this.defaultAlerts[0];
  }

}
