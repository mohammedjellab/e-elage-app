import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { RdvPaiement } from '../models/rdvPaiement';

@Injectable({
  providedIn: 'root'
})
export class RdvPaiementService {

  urlPlatform = this.api.getUrlPlatform()+"/rdvpaiement";
  url = this.api.getUrl()+"/rdvpaiement";
  constructor(private api: ApiService,private http: HttpClient) { }
  //`
  persist(paiement){
    return this.http.post<RdvPaiement>(`${this.url}/insert`,paiement);
  }
  
  joinWithFamille(){
    return this.http.get(`${this.url}/join-famille`);
  }

  getInfoRelementByConsultation(consultation_id){
    return this.http.get(`${this.url}/information/${consultation_id}`)
  }
  getInfoRelementByRdv(rdv){
    return this.http.get(`${this.url}/agenda/${rdv}`)
  }

  
}
