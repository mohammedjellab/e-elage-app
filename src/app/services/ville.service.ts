import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  urlPlatform = this.api.getUrlPlatform()+"/villes";
  url = this.api.getUrl()+"/villes";
  constructor(private api: ApiService, private http: HttpClient) { }

  searchVille(term){
    return this.http.get(`${this.url}/search/${term}`);
  }
  findAllVilles(){
    return this.http.get(`${this.url}/list`);
  }
}
