import { User } from './../models/User';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  url = this.api.getUrl()+"/api";
  url2 = this.api.getUrl()+"";
  
  constructor(private api: ApiService,private http: HttpClient) { }
    //`


  login(user){
    return this.http.post(`${this.url2}/login`,user);
    

  }
  register(user){
    return this.http.post(`${this.url}/register`,user);

  }
  logout(user){
    return this.http.post(`${this.url}/logout`,user);
  }
}
