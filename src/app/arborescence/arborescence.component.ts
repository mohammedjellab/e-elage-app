import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-arborescence',
  templateUrl: './arborescence.component.html',
  styleUrls: ['./arborescence.component.scss']
})
export class ArborescenceComponent implements OnInit {
  ngOnInit(){

  }
  nodes = [
    {
      name: 'authentification',
      children: [
          { name: 'login pc', children: [
                  {name: 'pages-login', children: [
                    {name: 'login-content-1'}]
                  },
                  {name: 'pages-register', children: [
                    {name: 'register-content'},
                    {name: 'login-content-2'}]
                  }]
          },
          { name: 'login mobile', children: [
            { name: 'page-acceuil-login-mobile', children: [
              {name: 'pages-login-mobile', children: [
                {name: 'login-content-1'}]
              },
              {name: 'pages-register-mobile', children: [
                {name: 'register-content'},
                {name: 'login-content-2'}]
              }]
            },
          ]
          },
    ]
    },
    {
      name: 'Mes patients',
      children: [
          { name: 'dossier-patient', children: [
                  {name: 'New York'},
                  {name: 'California'},
                  {name: 'Florida'}
              ] },
          { name: 'dossier-medicale', children: [
            {name: 'New York'},
            {name: 'California'},
            {name: 'Florida'}
        ] }]
    },
    {
      name: 'Fiche patient',
      children: [
          { name: 'dossier-patient', children: [
                  {name: 'New York'},
                  {name: 'California'},
                  {name: 'Florida'}
              ] },
          ]
    },
    {
      name: 'Dossier médicale',
      children: [
          { name: 'dossier-patient', children: [
                  {name: 'New York'},
                  {name: 'California'},
                  {name: 'Florida'}
              ] },
          { name: 'dossier-medicale', children: [
            {name: 'New York'},
            {name: 'California'},
            {name: 'Florida'}
        ] }]
    },
    {
        name: 'Agenda',
        children: [
            { name: 'Argentina', children: [] },
            { name: 'Brazil' }
        ]
    },
    {
        name: 'Salle d attente',
        children: [
            { name: 'England' },
            { name: 'Germany' },
            { name: 'France' },
            { name: 'Italy' },
            { name: 'Spain' }
        ]
    },
    {
      name: 'Mes consultations',
      children: [
          { name: 'England' },
          { name: 'Germany' },
          { name: 'France' },
          { name: 'Italy' },
          { name: 'Spain' }
      ]
  },
  {
    name: 'Reglement',
    children: [
        { name: 'England' },
        { name: 'Germany' },
        { name: 'France' },
        { name: 'Italy' },
        { name: 'Spain' }
    ]
  },
  {
    name: 'Statistique',
    children: [
        { name: 'England' },
        { name: 'Germany' },
        { name: 'France' },
        { name: 'Italy' },
        { name: 'Spain' }
    ]
  },
  {
    name: 'Parametrage',
    children: [
        { name: 'England' },
        { name: 'Germany' },
        { name: 'France' },
        { name: 'Italy' },
        { name: 'Spain' }
    ]
  }
];
options = {};
}
