import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LieuConsultationComponent } from './lieu-consultation.component';

describe('LieuConsultationComponent', () => {
  let component: LieuConsultationComponent;
  let fixture: ComponentFixture<LieuConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LieuConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LieuConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
