// pour désactiver/activer weeksOf
weeksOf

// Le temps entre d'une consultaion 
slotDuration

// L'heure de commencer pour le cabinet
scrollTime

//pour désactiver la ligne all-day
allDaySlot

//configurer le jour de commencement la semaine
firstDay

//L'heure de commencer pour le cabinet
minTime

//L'heure de fin pour le cabinet
maxTime

//pour les jours de semaine
locale

// pour le titre de date
views: {
          dayGridMonth: { // name of view
            titleFormat: { year: 'numeric', month: '2-digit', day: '2-digit' }
            // other view-specific options here
          }
        }
[views]="optionsCalendar.views"
///