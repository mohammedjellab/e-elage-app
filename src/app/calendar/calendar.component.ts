import { map } from 'rxjs/operators';
import { TypeConsultationService } from './../services/type-consultation.service';
import { AgendaConfig } from './../models/agendaConfig';
import { DOCUMENT } from '@angular/common';
import { ThemeOptions } from './../theme-options';
import { DefaultService } from './../services/default.service';
import { AgendaEvenement } from './../models/AgendaEvenement';
import { element } from 'protractor';
import { title } from 'process';
import { SalleService } from './../services/salle.service';
import { UserService } from './../services/user.service';
import { AgendaService } from './../services/agenda.service';
import { AfterViewInit, ApplicationRef, Component, ComponentFactoryResolver, Injector, Input, OnChanges, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation, Renderer2, Inject, SimpleChanges } from '@angular/core';
import { EventInput,Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { BsModalService, BsModalRef, ModalOptions } from "ngx-bootstrap/modal";
import * as $ from 'jquery';
import {
  CalendarEvent,
  DAYS_OF_WEEK,
  CalendarView
} from 'angular-calendar';
import { setHours, setMinutes } from 'date-fns';
import { BsLocaleService, ModalDirective } from 'ngx-bootstrap';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { frLocale } from 'ngx-bootstrap/locale';
import { DatepickerDateCustomClasses } from 'ngx-bootstrap/datepicker';
import { RdvComponent } from './rdv/rdv.component';
import { AgendaConfigService } from '../services/agenda-config.service';
import Tooltip from 'tooltip.js';
import { faCoffee, faPalette } from '@fortawesome/free-solid-svg-icons';
import { LieuConsultationComponent } from './lieu-consultation/lieu-consultation.component';
import * as moment from 'moment';
import { resolve } from 'url';
@Component({
  template: `
    <div class="fc-content" [ngbPopover]="template" container="body" triggers="manual">
      <ng-content></ng-content>
    </div>
  `,
})

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styles: [`.fc-time-grid .fc-slats td {
    height: 2em ;
    }`],
  encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit, OnDestroy,AfterViewInit {
  //////
  height = 6;
  /*public get style(): string {
    return `
    .fc-time-grid .fc-slats td {
      height: ${this.height}
    }
    `;
  }*/
  //////
  toggleMobileSidebar: boolean = true;
  tooltip: Tooltip;
  faCoffee = faCoffee;
  faPalette = faPalette;


  post = {
    isFavorite: false
  }

  @ViewChild('modal', { static: false }) modal: ModalDirective;

  //calendar option
  @ViewChild('calendar', { static: false }) calendar: FullCalendarComponent;
  calendarApi: Calendar;


  optionsCalendarOLD =   {
        weeksOf: true,
        header:{
          left: 'prev,next today myCustomButton',//'prev,next today',
          center: 'title',
          right: 'timeGridDay,timeGridWeek,dayGridMonth,listWeek'
        },
        buttonText: {
          today:  "Aujourd'hui",
          month:    'Mois',
          week:     'Semaine',
          day:      'Jour'
        },
        allDaySlot: false,
        firstDay: null,
        minTime: null,
        maxTime: null,
        //eventContent: this.renderEventContent,
        businessHours: {
          startTime: null,
          endTime: null,
        },
        slotDuration: null,
        //scrollTime: '14:00:00',
        defaultView: 'timeGridWeek',
        locale: 'fr',
        height: 'auto',
        start: null,
        columnHeaderHtml: null
      }
  optionsCalendar =   {
    editable: true,
    weeksOf: true,
    dayRender: function (dayRenderInfo) {        
      let dateMoment = moment(dayRenderInfo.date);
      console.log('dateMoment',dateMoment)
      if (dateMoment.day() === 6 || dateMoment.day() === 0) {
        dayRenderInfo.el.style.backgroundColor = '#d6e7e1';
      }
      else {
        dayRenderInfo.el.style.backgroundColor = 'red';
      }
      return dayRenderInfo;
    },
    header:{
      left: 'prev,next today myCustomButton',//'prev,next today',
      center: 'title',
      right: 'timeGridDay,timeGridWeek,dayGridMonth,listWeek'
    },
    buttonText: {
      today:  "Aujourd'hui",
      month:    'Mois',
      week:     'Semaine',
      day:      'Jour'
    },
    allDaySlot: false,
    firstDay: 1,
    minTime: '14:00:00',
    maxTime: '18:00:00',
    //eventContent: this.renderEventContent,
    businessHours: {
      startTime: '14:00:00',
      endTime: '18:00:00',
    },
    slotDuration: '00:20:00',
    //scrollTime: '14:00:00',
    defaultView: 'timeGridWeek',
    locale: 'fr',
    height: 'auto',
    start: null,
    columnHeaderHtml: null
  }
  
  myAgenda: AgendaEvenement = {
    salle: '',
    user: '',
    auteur: '',
    dateDebut: null,
    dateFin: null,
    lpatient_id: null,
    info_patient: null,
    description: '',
    heureArrivee: '',
    typeConsultation: null,
    praticien: null,
    sansRdv: false,
    heureSortie: null,
    centreSoin: null,
    legende: null,
    statut: ''
  }
  medecins;
  salles;
  bsInlineValue = new Date();
  bsInlineRangeValue: Date[];
  maxDate = new Date();
  calendarPlugins;
  showCalendar: boolean = false;
  calendarEvents: EventInput[] = [];
  calendarEventsOrigine: EventInput[] = [];
  search = {
    salle:'',
    vue: 'semaine',
    date: '',
    date2: '',
    praticien: '',
    status: null,
    centre: null,
    start: null,
    end: null
  }
  itemList = ["Book","Pen"];

  bsModalRef: BsModalRef;
  modalShow: boolean = false;
  locale = 'fr';
  user;
  dateCustomClasses: DatepickerDateCustomClasses[] =[];
  @Input() someHtml: string;
  agendaConfig: AgendaConfig = {
    firstDay: null,
    granularite: null,
    heureDebutOuvrable: null,
    heureFermeture: null,
    heureFinOuvrable: null,
    heureOuverture: null,
    modeAffichage: null,
    zoom: null,

  }
  //actes
  typeConsultations;
  occurrences = [];
  nbresRdvByJour = [];
  nbresRdvByNameDay = [];
  days = [] ;
  day= [
    {fr:'Lundi',en:'Monday',nbre:null},
    {fr:'Mardi',en:'Thursday',nbre:null},
    {fr:'Mercredi',en:'Wednesday',nbre:null},
    {fr:'Jeudi',en:'Thusday',nbre:null},
    {fr:'Vendredi',en:'Friday',nbre:null},
    {fr:'Samedi',en:'Saturday',nbre:null},
    {fr:'Dimanche',en:'Sunday',nbre:null}
  ];
  months = [
    {fr:'janvier',text: 'janv.',id:1},
    {fr:'février',text: 'févr.',id:2},
    {fr:'mars',text: 'mars',id:3},
    {fr:'avril',text: 'avr.',id:4},
    {fr:'mai',text: 'mai',id:5},
    {fr:'juin',text: 'juin',id:6},
    {fr:'juillet',text: 'juil.',id:7},
    {fr:'août',text: 'août',id:8},
    {fr:'septembre',text: 'sept.',id:9},
    {fr:'octobre',text: 'oct.',id:10},
    {fr:'novembre',text: 'nov.',id:11},
    {fr:'décembre',text: 'déc.',id:12}
  ]

  someValue = 5;
  @Input() categoryId: string;
  fc_center;



  checkedAll: boolean = true;
  actesSelected = [];
  //double click
  isSingleClick: number = 0;     
  gotoDate: boolean = false ;
  loadedData: boolean = false;
  eventBlocked : boolean = true ;
  constructor(private agendaService: AgendaService,
              private modalService: BsModalService,
              private userService: UserService,
              private salleService: SalleService,
              private defaultService: DefaultService,
              private localeService: BsLocaleService,
              private agendaConfigService: AgendaConfigService,
              private appRef: ApplicationRef,
              private resolver: ComponentFactoryResolver,
              private injector: Injector,
              private typeConsultationService: TypeConsultationService,
              private globals: ThemeOptions,
              private renderer: Renderer2, @Inject(DOCUMENT) private document: any
    ) { }

  ngAfterViewInit(){
    //this.showElementHtml();

    console.log('dateJour', new Date())

  }
  prev(){
      this.calendar.getApi().prev();
      let date = this.calendar.getApi().getDate();
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      setTimeout(()=>{
        this.getFcCenter();
      })
      if(this.optionsCalendar.defaultView=="dayGridMonth"){
        this.getPeriodeCalendarMonth();
        this.getRdvMonth(firstDay,lastDay);
      }else{
        this.getPeriodeCalendar(null);
        this.getRdv();
      }
      
  }
  next(){
      this.calendar.getApi().next();
      let date = this.calendar.getApi().getDate();
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      setTimeout(()=>{
        this.getFcCenter();
      })
      if(this.optionsCalendar.defaultView=="dayGridMonth"){
        this.getPeriodeCalendarMonth();
        this.getRdvMonth(firstDay,lastDay);
      }else{
        this.getPeriodeCalendar(null);
        this.getRdv();
      }
  }

  today(){
    this.calendar.getApi().today();
    let date = this.calendar.getApi().getDate();
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    setTimeout(()=>{
      this.getFcCenter();
    })
    if(this.optionsCalendar.defaultView=="dayGridMonth"){
      this.getPeriodeCalendarMonth();
      this.getRdvMonth(firstDay,lastDay);
    }else{
      this.getPeriodeCalendar(null);
      this.getRdv();
    }
  }

  jour(){
    this.calendar.getApi().changeView('timeGridDay')
    this.optionsCalendar.defaultView="timeGridDay"
    this.getPeriodeCalendar(null);
    this.getRdv();
    setTimeout(()=>{
      this.getFcCenter();
    })
  }

  //onValueChange
  selectedDate(newDate) {
    console.log('newDate',newDate);
      if(this.showCalendar){
        
        this.calendar.getApi().gotoDate(newDate)

        /*if(this.optionsCalendar.defaultView=="dayGridMonth"){
          this.mois();
  
        }*/
        if(this.optionsCalendar.defaultView=="timeGridWeek" && !this.gotoDate){
          this.gotoDate= true ; 
          this.semaine();
          console.log('newDate',newDate);
          
          
          
        }
        setTimeout(()=>{
          //this.getPeriodeCalendar(null);
          this.getFcCenter();
          
        })
        
    }

    
    // this.calendar.getApi().select(new Date(),new Date(this.bsInlineValue))
    this.bsInlineValue =newDate;
  }

  semaine(){
    this.calendar.getApi().changeView('timeGridWeek')
    this.optionsCalendar.defaultView="timeGridWeek";
    this.getPeriodeCalendar(null);
    this.getRdv();
    setTimeout(()=>{
      this.getFcCenter();
    })
  }
  mois(){
    let date = this.calendar.getApi().getDate();
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    this.calendar.getApi().changeView('dayGridMonth')
    this.optionsCalendar.defaultView="dayGridMonth"
    //this.getPeriodeCalendar(null);
    this.getPeriodeCalendarMonth();
    this.getRdvMonth(firstDay,lastDay);
    setTimeout(()=>{
      this.getFcCenter();
    })
    this.getNbresMonth();
  }

 //remplir date debut et fin et appeler la methode getListRdv
  getRdvMonth(start,end){
    this.search.start = start;
    this.search.end = end
    this.getlistRdv();
    
  }
  //remplir date debut et fin et appeler la methode getListRdv
  getRdv(){
    this.search.start = this.dateCustomClasses[0].date;
    this.search.end = this.dateCustomClasses.slice(-1)[0].date;
    this.getlistRdv();
    
  }

  //
  toggleActe(val){

    console.log('checked',val.target.checked)
    if(val.target.checked){
      this.calendarEvents = this.calendarEventsOrigine;
    }else{
      this.calendarEvents = [];
    }
    this.typeConsultations.forEach(element => {
      element.isSelected = val.target.checked;
    });
  }

  
  filterRdvByActe(acte){

    console.log('consult'+this.actesSelected,acte.isSelected)
    if(!acte.isSelected){
      this.actesSelected.push(acte.id)
      console.log('consult'+this.actesSelected,acte)
    }else{
      this.removeValueInArray(this.actesSelected,acte.id);
    }


    //
    //this.calendarEvents = this.calendarEventsOrigine;
   console.log('this.calendarEvents',this.calendarEvents)

      this.calendarEvents = this.calendarEventsOrigine.filter(events => this.actesSelected.includes(events.typeConsultation) );
      console.log('this.calendarEvents',this.calendarEvents) /**/
  }
  removeValueInArray(arr,value){
    let index =  arr.findIndex((element)=> element==value)
    arr.splice(index);
  }
  removeValueInArrayByindex(arr,index){
    arr.splice(index);
  }


  ngOnDestroy(){
    this.hideSidebar();
  }
  /*ngOnChanges(changes: SimpleChanges) {
    //console.log(changes,this.bsInlineValue)

    /*const styles = this.document.getElementById('dynamic-theme-css');
    if (styles) {
      styles.innerHTML = this.style;
    }*/
  //}



    // You can also use categoryId.previousValue and
    // categoryId.firstChange for comparing old and new values



  parseDate(e) {
    this.bsInlineValue = new Date(e);
    console.log('bsInlineValue',this.bsInlineValue)
  }
  /*initialiseCss(){
    const styles = this.document.createElement('STYLE') as HTMLStyleElement;
    styles.id = 'dynamic-theme-css';
    styles.innerHTML = this.style;
    this.renderer.appendChild(this.document.head, styles);
  }*/
  ngOnInit() {

    this.showSidebar();
    //this.addEvent();
    this.getUser();
    this.search.centre = this.user.centreSoin;
    this.getConfigAgenda();
    this.getlistRdv();
    this.initCalandar();
    
    this.initMethod();
    console.log('events',this.calendarEvents)

    this.localeBsDatepicker();


    //this.initialiseCss();

    console.log('dateSelected',this.bsInlineRangeValue)





  }

 


  
  dateSelected(event){

    console.log('dateSelected',this.bsInlineRangeValue)

  }

  showSidebar(){
    this.globals.sidebarFixed =true;
    this.globals.headerFixed = true;
    this.globals.toggleSidebar =  true;
  }
  hideSidebar(){
    this.globals.sidebarFixed =false;
    this.globals.headerFixed = false;
    this.globals.toggleSidebar =  false;
  }


  //recuperer la configuration de l'agenda
  getConfigAgenda(){
    this.agendaConfigService.findByCentreSoin(this.user.centreSoin).subscribe(data =>{
      this.agendaConfig = data;

      let weeksOf = true;
      let defaultView;
      if(data.modeAffichage=='weeksOff'){
        weeksOf =false;

      }
      if(data.modeAffichage=='weeksOff' || data.modeAffichage=='weeksOn'){
        defaultView =   'timeGridWeek';
      }else{
        defaultView = data.modeAffichage;
      }


      this.optionsCalendar =   {
        editable: true,
        weeksOf: weeksOf,
        dayRender: function (dayRenderInfo) {        
          let dateMoment = moment(dayRenderInfo.date);
          console.log('dateMoment',dateMoment)
          if (dateMoment.day() === 6 || dateMoment.day() === 0) {
            dayRenderInfo.el.style.backgroundColor = '#d6e7e1';
          }
          else {
            dayRenderInfo.el.style.backgroundColor = 'red';
          }
          return dayRenderInfo;
        },
        header:{
          left: 'prev,next today myCustomButton',//'prev,next today',
          center: 'title',
          right: 'timeGridDay,timeGridWeek,dayGridMonth,listWeek'
        },

        buttonText: {
          today:  "Aujourd'hui",
          month:    'Mois',
          week:     'Semaine',
          day:      'Jour'
        },
        //scrollTime: '10:00:00',
        allDaySlot: false,
        firstDay: +data.firstDay,
        minTime: data.heureDebutOuvrable,
        maxTime: data.heureFinOuvrable,
        //eventContent: this.renderEventContent,
        businessHours: {

          startTime: data.heureOuverture,
          endTime: data.heureFermeture,
        },

        slotDuration: data.granularite,
        //scrollTime: '14:00:00',
        defaultView: defaultView,
        locale: 'fr',
        height: 'auto',
        start: this.bsInlineValue,
        columnHeaderHtml: function(date) {

          if (date.getDay() === 1) {

            return 'Lundi';
          }if (date.getDay() === 2) {
            return 'Mardi';
          }if (date.getDay() === 3) {
            return 'Mercredi';
          }if (date.getDay() === 4) {
            return 'Jeudi';
          }if (date.getDay() === 5) {
            return 'Vendredi';
          }if (date.getDay() === 6) {
            return 'Samedi';
          } else {
            return 'Dimanche';
          }
        }
      }
    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }


  goto(){
    this.calendar.getApi().gotoDate(new Date('2021-09-19'))
  }
  //afficher popover pour chaque event
  showPopoverOld(eventInfo){
    console.log('eventInfo',eventInfo)
    let start,end: Date;
    start = eventInfo.event.start;
    end = eventInfo.event.end;
    let minute_start = start.getMinutes();
    let heure_start = start.getHours();
    let minute_end = end.getMinutes();
    let heure_end = end.getHours();
    let time_start: string;
    let time_end: string;
    let time: string;
    //start
    if( heure_start<10){
      time_start = '0'+heure_start+':'+minute_start;
    }else if(minute_start==0 ){
      time_start = heure_start+':0'+minute_start;
    }else if(minute_start!=0 && heure_start>9){
      time_start = heure_start+':'+minute_start;

    }

    //end
    if(heure_end<10){
      time_end = '0'+heure_end+':'+minute_end;
    }
    else if(minute_end==0){
      time_end = heure_end+':0'+minute_end;
    }else if(minute_end!=0 && heure_end>9){
      time_end = heure_end+':'+minute_end;
    }
    time = time_start+' '+time_end
    //eventInfo.el.childNodes[0].setAttribute("data-toggle", "popover");
    //eventInfo.el.childNodes[0].setAttribute("title",time+' '+eventInfo.event.title);
    //eventInfo.el.childNodes[0].setAttribute("placement", "top");
    eventInfo.el.childNodes[0].setAttribute("class", "fc-content");
    //eventInfo.el.childNodes[0].setAttribute("container", "body");
    //eventInfo.el.childNodes[0].setAttribute("data-content", "And here's some amazing content. It's very engaging. Right?");
  }
  showPopover(eventInfo){
    eventInfo.el.childNodes[0].setAttribute("class", "fc-content");
  }
  hidePopoverOver(eventInfo){
    console.log(' eventInfo.el.childNodes[0]', eventInfo.el.childNodes[0].setAttribute("class", "bg-white text-black p-2 shadow-sm-dark br-tl br-tr br-bl br-br z-index-1"));
    ///eventInfo.el.childNodes[0].removeClass("p-4 bg-white");
  }

  getPeriodeCalendarMonth(){
    //this.days = [];
    let list;
    let text: string = this.fc_center;
    text = text.split(' ').shift();
    console.log('list',text)
    let num:number = list.shift().id;
    let d : Date = new Date(2008, num , 0);
    let numberJour =d.toDateString().split(' ')[2];
      this.dateCustomClasses = [];
      let day: string
      for(let i=1;i<=+numberJour;i++){
        var days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        if(i<10){
          day =`0${i}`
        }else{
          day = ''+i;
        }
        let date = new Date(`2021-${num}-${day}`);
        this.dateCustomClasses.push({ date: date, classes: ['bg-gris-8'] });
        var dayName = days[d.getDay()];
        this.days.push({'day': date,'name': dayName,'nbre':null})
      }
      this.insertDayInHtml();
  }
  getPeriodeCalendar(type){
    console.log('xxxxxxxx')
    this.days = [];
    let p = document.querySelectorAll("th[data-date]");
    this.dateCustomClasses = [];
    //console.log('this.dateCustomClasses',this.dateCustomClasses);
    for(let i=0;i<p.length; i++){
      var days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
      let date = p[i].getAttribute("data-date");
      var d = new Date(date);
      this.bsInlineValue = d;
      this.dateCustomClasses.push({ date: d, classes: ['bg-gris-8'] });
      var dayName = days[d.getDay()];
      this.days.push({'day': date,'name': dayName,'nbre':null})
    }
    this.insertDayInHtml();
    //console.log('this.dateCustomClasses',this.dateCustomClasses);
  }












  //initialiser le tableau des jour, et supprimer les div qui contient les jour pour eviter le redoublement
  /*initArrayDay(){
    this.days = [];
    let myDivDay = document.getElementById('myDivDay');
    if(myDivDay){
      myDivDay.remove();
    }
  }*/

  getNbresMonth(){
    console.log('getNbresMonth avant',this.day);
    this.days = this.day;
    this.days.forEach(d => {
      this.nbresRdvByNameDay.forEach(nbre =>{
        if(d.en==nbre[0]){
          d.nbre = nbre[1]
        }
      });
    });
    console.log('getNbresMonth apres',this.days);

  }

  insertDayInHtml(){
    console.log('this.days',this.days)
    for(let i = 0; i<this.days.length; i++){
      let n = 0;
      this.nbresRdvByJour.forEach(nbres =>{
        if(nbres[0]==this.days[i]['day']){
          //console.log('isEntre'+nbres[0],this.days[i]['day'])
          n=nbres[1];
          this.days[i]['nbre']= n;
        }
      })
    }
  }
  insertDayInHtmlOLD(){
    this.getPeriodeCalendar(null);
    //création de div pour inserer dedans le tableau des jours
    /*let warning = document.createElement("div");
    warning.setAttribute("id", "myDivDay");
    warning.setAttribute("class", "ml-5");
    warning.textContent = "New notifications";

    //recuperer le div fc-toolbar
    let div = document.getElementsByClassName("fc-toolbar fc-header-toolbar");
    //inserer mydiv apres cette div
    div[0].after(warning);
    //recuperer myDiv et remplir le tableau par les jours et le nbres des rdvs
    let myDivDay = document.getElementById('myDivDay');

    let table ;
          table+='<div class="table-responsive" >';
          table+='<table class="table table-hover text-nowrap mb-0 table-borderless">';
          table+='<thead>';
          table+=' <tr>';
          // table+='<th>#</th>';*/
          for(let i = 0; i<this.days.length; i++){
            let n = 0;
            this.nbresRdvByJour.forEach(nbres =>{

              if(nbres[0]==this.days[i]['day']){
                console.log('isEntre'+nbres[0],this.days[i]['day'])
                n=nbres[1];
                this.days[i]['nbre']= n;

              }

            })

            //table+='<th class="text-center bg-white">  <span class="badge badge-pill m-1 badge-neutral-info text-info bottom-0 float-none"><b>'+ n + '</b></span></th>';
          }
          console.log('isEntreee',this.days)

         /*

          table+='</tr>';
          table+='</thead>';
          table+='</table>';
          table+='</div>';
          myDivDay.innerHTML  = table;
          //pour enlever undefined
          console.log('isEntre','n')

          myDivDay.firstChild.textContent = '';*/
         // this.prev();
  }

  localeBsDatepicker() {
    defineLocale(this.locale,frLocale);
    this.localeService.use(this.locale);
  }

  onFavoriteChange(v){
    console.log('dd',v)
  }

  getTypeConsultation(){
    this.typeConsultationService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      var result = Object.keys(data).map((key) => [data[key]]);
      result.forEach((type) =>{
        //console.log("type[0]['nbre']", type);
        this.occurrences.forEach(function (occ,index){
          if(index==type[0]['id']){
            type[0]['nbre']=occ;
          }else{
            //type[0]['nbre']=0;
          }
        })

      })
      this.typeConsultations = data;
      //console.log('typeConsultations',this.typeConsultations)
    });
  }

  showElementHtml(){
    this.calendarEvents = [];
    //this.getlistRdv();

      const elements = document.getElementsByClassName('fc-axis fc-time fc-widget-content');
      for(let i=0; i<elements.length; i++) {
        //elements[i].style.height = "10em";
      }
    console.log('showElementHtml','showElementHtml');
    //encapsulation: ViewEncapsulation.None
    //element.style.color = "blue";
    //element.style.height = "5em";
    //height: 2.5em;
    //console.log('showElementHtml',element);
  }

  initCalandar(){
    this.calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  }

  //pour créer un rdv et 
  handleDateClick(calDate) {
    this.isSingleClick += 1;
    if(this.isSingleClick>1){
      this.openLoginModal(calDate,null,calDate.date,'persist');
    }
    setTimeout(()=>{
      this.isSingleClick = 0 ;
    },500)
  }

  //

  // activer desactiver weekends
  toggleWeeks(value){
    this.optionsCalendar.weeksOf = value;
    console.log('toggleWeeks',value)
  }
  hideModal() {
    this.modal.hide();
  }

  lieuConsultation(){
    let initialState = {
    }
    this.bsModalRef = this.modalService.show(LieuConsultationComponent,{initialState, class: 'modal-xl'});
  }

  addStatut(calDate){
    ////////////////////
    let statut =['info','warning'];
    let element = calDate.el.childNodes[0].childNodes[0];
    let textContent ;
    
    let badge  ='' ;
    for(let i=0; i<statut.length;i++){

      badge  += `<span class="badge badge-circle badge-${statut[i]} color-patient-danger">&nbsp;</span>${badge}` ;
    }
    console.log('calDate -',badge);

    if(element ){
    console.log('calDate+',badge);

      textContent = calDate.el.childNodes[0].textContent.split(";");
      element.setAttribute("class", "heure-hide");
      calDate.el.childNodes[0].innerHTML=`<div class="d-flex justify-content-end mb-1">
      ${badge}</div>
      <div class="nom_patient"> ${textContent[0]};
      <div class="acte_rdv">${textContent[1]};</div>
      </div>`;
    }
    //////////////////////////
  }
  //ouvrir modal rdv et envoyée id de rdv
  openLoginModal(calDate,idRdv,calDateClicked,action) {
     //this.addStatut(calDate);
     //this.renderEventContent(calDate);
    this.modalShow =true;
    let initialState = {
      idRdv: idRdv,
      calDateClicked: calDateClicked,
      agendaConfig: this.agendaConfig
    };
    this.bsModalRef = this.modalService.show(RdvComponent,{initialState, class: 'modal-xl'});

    this.bsModalRef.content.event.subscribe(res => {
        
      console.log('action ss'+action,res)
      let rdv = res.rdv;

      if(rdv){
        
        
         
        
        let title;
        console.log('action ee',res.action)
        console.log('action ee',rdv)
        //test pour patient sans ou avec rdv
        if(rdv['patient']){
          console.log('lpatient_id'+action,res)
           title = rdv['nom_complet'].concat(';',rdv['acte'],';',rdv['statut']);
        }else if(!rdv['patient'] && rdv['nom_complet_ss_rdv']){
          console.log('ss lpatient_id'+action,res)

           title = rdv['nom_complet_ss_rdv'].concat(';',rdv['acte'],';',rdv['statut']);
        }

        ///test pour savoir quelle action modification ou ajout
        if(res.action=='persist'){
          console.log('avent',this.calendarEvents)

          let statut =this.etatLegende(rdv['sansRdv'],rdv['statut'],rdv['annule']);
          this.addEvent(title.concat(';',rdv['acte'],';',rdv['statut']),
                        setHours(setMinutes(new Date(rdv['dateDebut']), this.getHeureArriveeMin(rdv['heureArrivee'])), this.getHeureArrivee(rdv['heureArrivee'])),
                        //setHours(setMinutes(new Date(rdv['year_start'], rdv['month_start']-1, rdv['day_start']), rdv['minute_start']), rdv['hours_start']),
                        //setHours(setMinutes(new Date(rdv['year_end'], rdv['month_end']-1, rdv['day_end']), rdv['minute_end']), rdv['hours_end']),
                        setHours(setMinutes(new Date(rdv['dateFin']), this.getHeureSortieMin(rdv['heureSortie'])), this.getHeureSortie(rdv['heureSortie'])),
                        rdv['id'],
                        rdv['legende'],
                        rdv['sansRdv'],
                        rdv['color'],
                        rdv['salle'],
                        rdv['praticien'],
                        statut,
                        rdv['typeConsultation'],
                        rdv['cin'],
                        rdv['tel'],
                        rdv['annule']
                        );
           this.bsModalRef.hide();
          console.log('apres',this.calendarEvents)
        }else if(res.action=='update'){
          let statut =this.etatLegende(rdv['sansRdv'],rdv['statut'],rdv['annule']);
          console.log('statut',statut);
          this.searchEvent(title.concat(';',rdv['acte'],';',statut),
          rdv['dateDebut'],
          rdv['dateFin'],
          rdv['id'],
          rdv['legende'],
          rdv['sansRdv'],
          rdv['color'],
          rdv['salle'],
          rdv['praticien'],
          rdv['heureSortie'],
          rdv['heureArrivee'],
          );
          console.log('this.searchEvent(idRdv)',rdv);
           //this.renderEventContent(calDateClicked);
          
          this.bsModalRef.hide();
        }else if(res.action=='updateStatus'){
          let statut =this.etatLegende(rdv['sansRdv'],rdv['statut'],rdv['annule']);
          console.log('statut',statut);
         // this.updateStatus(idRdv,status);
          
          
          console.log('this.searchEvent(idRdv)',rdv);
           //this.renderEventContent(calDateClicked);
          
          this.bsModalRef.hide();
        }else if(res.action=='annuler'){
          console.log('annuler0',rdv)
          this.annulerRdv(rdv);
          this.bsModalRef.hide();
        }

      }
      else{
        this.bsModalRef.hide();
      }
   });
   
 
   /*(title,
    setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
    setHours(setMinutes(new Date(rdv[0]['year_end'], rdv[0]['month_end']-1, rdv[0]['day_end']), rdv[0]['minute_end']), rdv[0]['hours_end']),*/

  }





  onEventRender(info: any) {
    console.log('onEventRender', info.el);
  }
  calendarEventResize(calDate) {
    this.eventDrop(calDate);
  }

  

  showModal() {
    this.modal.show();
  }

  //cliquer sur un rdv pour voir détail ou modifier
  //et faire un double click pour afficher rdv
  eventClicked(event){
    this.findEvent(195);
    console.log('calDate',event);
    
    this.isSingleClick += 1;
    if(this.isSingleClick>1){
      this.openLoginModal(event,event.event.id,null,'update');
    }
    setTimeout(()=>{
      this.isSingleClick = 0 ;
    },300)
    
  }

   //Event Render Function
   renderEventContent(eventInfo) {
    let list = eventInfo.event.textColor;
    list = list.split(",")
    let heure: string = '';
     heure = eventInfo.el.childNodes[0].childNodes[0].childNodes[0].textContent ;
    let textContent ;
      let badge = '';
      for(let i=0; i<list.length;i++){
        badge  = `<span class="badge badge-circle badge-${list[i]} color-patient-danger">&nbsp;</span>${badge}` ;
      }

        let element = eventInfo.el.childNodes[0].childNodes[0]
        if(element ){
          textContent = eventInfo.el.childNodes[0].textContent.split(";");
          element.setAttribute("class", "heure-hide");
          eventInfo.el.childNodes[0].innerHTML=`<div class="d-flex justify-content-end mb-1">
          ${badge}</div>
          <div class="nom_patient"> ${textContent[0]};
          <div class="acte_rdv">${textContent[1]};</div>
          </div>`;
        }
        else if(element.childNodes[1]){
          textContent = eventInfo.el.childNodes[0].childNodes[1].textContent.split(";");
          element.childNodes[0].setAttribute("class", "heure-hide");
          eventInfo.el.childNodes[0].childNodes[1].innerHTML=`<div class="d-flex justify-content-end">
          <span class="badge badge-circle badge-${badge} color-patient-danger">&nbsp;</span>
          <div class="nom_patient"> ${textContent[0]};
          <div class="acte_rdv">${textContent[1]};</div>
          </div>`;
        }
      //pour le premier chargement
      this.getFcCenter();


  }
  // update rdv
  updateRdv(id){
    console.log('id rdv',id)
    this.agendaService.update(this.myAgenda,id).subscribe(data =>{


    })
  }

  //bloquer déplacement des rendez-vous
  blockEvent(event){
    let status = event.event.textColor;
      if(status){
        status = status.split(',');
      }
      status.forEach(element => {
        console.log('element',element);
        if(element == "success"){
          this.eventBlocked = false;
          alert('Vous ne pouvez pas déplacer ce rendez-vous est déjà vu');
        }
    });
  }
  //pour deplacer les rdv et changer leur debut et fin
  eventDrop(calDate){
    
    this.blockEvent(calDate)
    if(this.eventBlocked){
      let hour_start = (<Date>calDate.event.start).getHours();
      let minitue_start = (<Date>calDate.event.start).getMinutes();
      let hour_end = (<Date>calDate.event.end).getHours();
      let prefix_hour_start:string = null;
      let prefix_hour_end:string = null;
      if(hour_end<10){
        prefix_hour_end = '0';
      }
      if(hour_start<10){
        prefix_hour_start = '0';
      }
      let minitue_end = (<Date>calDate.event.end).getMinutes();
      this.myAgenda.heureArrivee = prefix_hour_start+hour_start+':'+minitue_start;
      this.myAgenda.heureSortie = prefix_hour_end+hour_end+':'+minitue_end;
      this.myAgenda.dateDebut  = new Date(this.getDateFormatMonthDayYearSlash(calDate.event.start))
      this.myAgenda.dateFin  = new Date(this.getDateFormatMonthDayYearSlash(calDate.event.end))
      console.log('this.myAgenda.heureArrivee', prefix_hour_start+hour_start+':'+minitue_start)
      this.updateRdv(calDate.event.id);
    }
    
  }



  getlistRdv(){

    this.calendarEvents = [];
    this.agendaService.rdvCalendar(this.search).subscribe(
      data =>{
        let occurrences = [];
        let nbresRdvByJour = [];
        let nbresRdvByNameDay = [];
        let result = Object.keys(data).map((key) => [data[key]]);
        console.log('data',data)
        let ev: any =[] ;

        result.forEach(rdv => {
          nbresRdvByJour[rdv[0]["dateJour"]] = (nbresRdvByJour[rdv[0]["dateJour"]] || 0) + 1;
          nbresRdvByNameDay[rdv[0]["dayName"]] = (nbresRdvByNameDay[rdv[0]["dayName"]] || 0) + 1;
          occurrences[rdv[0]["typeConsultation"]] = (occurrences[rdv[0]["typeConsultation"]] || 0) + 1;

          let title: any;
          if(rdv[0]['lpatient_id']){
            title = rdv[0]['nom_complet'].concat(';',rdv[0]['acte'],';',rdv[0]['statut']);
          }else if(!rdv[0]['lpatient_id'] && rdv[0]['nom_complet_ss_rdv']){
            title = rdv[0]['nom_complet_ss_rdv'].concat(';',rdv[0]['acte'],';',rdv[0]['statut']);
          }
          let statut =this.etatLegende(rdv[0]['sansRdv'],rdv[0]['statut'],rdv[0]['annule']);
          ///


          ///
          this.addEvent(title,
                        setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
                        setHours(setMinutes(new Date(rdv[0]['year_end'], rdv[0]['month_end']-1, rdv[0]['day_end']), rdv[0]['minute_end']), rdv[0]['hours_end']),
                        rdv[0]['id'],
                        rdv[0]['legende'],
                        rdv[0]['sansRdv'],
                        rdv[0]['color'],
                        rdv[0]['salle'],
                        rdv[0]['praticien'],
                        statut,
                        rdv[0]['typeConsultation'],
                        rdv[0]['cin'],
                        rdv[0]['tel'],
                        rdv[0]['annule']
                        )
          /*ev = {
            title: 'No event end date ZZZZ',
            start: setHours(setMinutes(new Date(), 0), 4),
            color: colors.blue
          }
          setHours(setMinutes(new Date(rdv[0]['year'], rdv[0]['month'], rdv[0]['day']), rdv[0]['minute']), rdv[0]['hours'])
          */

        })
        this.occurrences = occurrences;
        this.nbresRdvByJour = Object.entries(nbresRdvByJour);
        this.nbresRdvByNameDay = Object.entries(nbresRdvByNameDay);
        console.log('nbresRdvByNameDay',this.nbresRdvByNameDay)
        this.showCalendar = true;
        this.calendarEventsOrigine = this.calendarEvents;
        this.loadedData = true;

        this.getTypeConsultation();
        setTimeout(()=>{

          this.getPeriodeCalendar(null);
          this.getFcCenter();

        },1000);

      },err=>{}

    )
  }
  getListRdvPromise(){
    return new Promise((resolve,reject)=>{
      this.calendarEvents = [];
      this.agendaService.rdvCalendar(this.search).subscribe(
        data =>{
          resolve(data)
        },err=>{
          reject(err)
        }
      )
    })
  }
  getlistRdvNew(){
    this.getListRdvPromise().then((data)=>{
      let occurrences = [];
      let nbresRdvByJour = [];
      let nbresRdvByNameDay = [];
      let result = Object.keys(data).map((key) => [data[key]]);
      result.forEach(rdv => {
        nbresRdvByJour[rdv[0]["dateJour"]] = (nbresRdvByJour[rdv[0]["dateJour"]] || 0) + 1;
        nbresRdvByNameDay[rdv[0]["dayName"]] = (nbresRdvByNameDay[rdv[0]["dayName"]] || 0) + 1;
        occurrences[rdv[0]["typeConsultation"]] = (occurrences[rdv[0]["typeConsultation"]] || 0) + 1;
        let title: any;
        if(rdv[0]['lpatient_id']){
          title = rdv[0]['nom_complet'].concat(';',rdv[0]['acte'],';',rdv[0]['statut']);
        }else if(!rdv[0]['lpatient_id'] && rdv[0]['nom_complet_ss_rdv']){
          title = rdv[0]['nom_complet_ss_rdv'].concat(';',rdv[0]['acte'],';',rdv[0]['statut']);
        }
        let statut =this.etatLegende(rdv[0]['sansRdv'],rdv[0]['statut'],rdv[0]['annule']);
        this.addEvent(title,
                      setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
                      setHours(setMinutes(new Date(rdv[0]['year_end'], rdv[0]['month_end']-1, rdv[0]['day_end']), rdv[0]['minute_end']), rdv[0]['hours_end']),
                      rdv[0]['id'],
                      rdv[0]['legende'],
                      rdv[0]['sansRdv'],
                      rdv[0]['color'],
                      rdv[0]['salle'],
                      rdv[0]['praticien'],
                      statut,
                      rdv[0]['typeConsultation'],
                      rdv[0]['cin'],
                      rdv[0]['tel'],
                      rdv[0]['annule']
                      )
        

      })
      this.occurrences = occurrences;
      this.nbresRdvByJour = Object.entries(nbresRdvByJour);
      this.nbresRdvByNameDay = Object.entries(nbresRdvByNameDay);
      console.log('nbresRdvByNameDay',this.nbresRdvByNameDay)
      this.showCalendar = true;
      this.calendarEventsOrigine = this.calendarEvents;
      this.loadedData = true;

      this.getTypeConsultation();
      //setTimeout(()=>{

        this.getPeriodeCalendar(null);
        this.getFcCenter();

      //},1000);
    })
    
  }


 
  //permet de supprimer la bare qui contient prev next jour semaine mois '
  getFcCenter(){
    let d = document.getElementsByClassName('fc-center')[1];
    if(d){
      this.fc_center = d.childNodes[0].textContent;
      document.getElementsByClassName('fc-toolbar fc-header-toolbar')[1].remove();
      console.log('this.fc_center',this.fc_center);
    }
  }

  //pour retourner l'etat de légende
  etatLegende(ss_avec_rdv,statut,annule){
    let arr =[];
    if(ss_avec_rdv==true){
      arr.push("dark");
    }else{
      arr.push("info");
    }
    if(statut=="vu"){
      arr.push("success");
    }else if (statut=="En salle"){
      arr.push("warning");
    }else if (statut=="encaisse"){
      arr.push("light");
    }
    if(annule==0){
      arr.push("danger");
    }
    return arr;
  }



  //methode pour ajouter un evenement au calendrier
  addEvent(title,start,end,idRdv,legende,sansRdv,color,salle,praticien,textColor,typeConsultation,cin,tel,annule){
    this.calendarEvents.push({
       title: title, start: start, end: end, id: idRdv,
       legende: legende,sansRdv: sansRdv,
       color: color,salle: salle,praticien: praticien,
       textColor:textColor,
       typeConsultation: typeConsultation,
       cin,
       tel,
       annule
    });

  }


  getHeureSortieMin(heureSortie){
    return heureSortie.split(':')[1];
  }
  getHeureSortie(heureSortie){
    return heureSortie.split(':')[0];
  }
  getHeureArriveeMin(heureArrivee){
    return heureArrivee.split(':')[1];
  }
  getHeureArrivee(heureArrivee){
    return heureArrivee.split(':')[0];
  }

  searchEvent(title,start,end,idRdv,legende,sansRdv,color,salle,praticien,heureSortie,heureArrivee){
    this.calendarEvents.forEach( (rdv,index) =>{
      //setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
      if(rdv.id == idRdv){
        console.log('searchEvent',title)
        rdv.title = title;
        rdv.start = setHours(setMinutes(new Date(start), this.getHeureArriveeMin(heureArrivee)), this.getHeureArrivee(heureArrivee));
        rdv.end   = setHours(setMinutes(new Date(end), this.getHeureSortieMin(heureSortie)), this.getHeureSortie(heureSortie));
        rdv.legende = legende
        rdv.sansRdv = sansRdv
        rdv.color = color
        rdv.salle = salle
        rdv.praticien = praticien
        console.log('rdddd',rdv)
      }
    })
  }

  //trouver un element dans les rdvs
  findEvent(id){
    return  this.calendarEvents.find(element => element.id == id);
  }


  annulerRdv(idRdv){
    this.calendarEvents = this.calendarEvents.filter( (rdv,index) => rdv.id != idRdv)
  }

  updateStatus(id,title){
    this.calendarEvents.forEach( (rdv,index) =>{
      //setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
      if(rdv.id == id){
        console.log('searchEvent',title)
        rdv.title = title;
        
      }
    })
  }

  //filtrer le tableau des evants par legende ou avec rdv/sans rdv
  getRdvByStatut(val){
    console.log('this.calendarEvents',val)

    this.calendarEvents = this.calendarEventsOrigine;
    console.log('this.calendarEvents',this.calendarEvents)

    if(val == "sans_rdv"){
      this.calendarEvents = this.calendarEvents.filter(events => events.sansRdv === true);
    }else if(val == "avec_rdv"){
      this.calendarEvents = this.calendarEvents.filter(events => events.sansRdv === false);
    }
    else if(val == "annule"){
      this.calendarEvents = this.calendarEvents.filter(events => events.annule === false);
    }
    else if(val !="globale"){
      this.calendarEvents = this.calendarEvents.filter(events => events.legende == val);

    }
    
    console.log('this.calendarEvents',this.calendarEvents)
  }




  // formater la date mm/dd/yyyy
  getDateFormatMonthDayYearSlash(d){
    return this.defaultService.getDateFormatMonthDayYearSlash(d);

  }
  //afficher la liste des medecins par etablissement
  getMedecinByEtablissment(){
    this.userService.getMedecinByEtablissment(1).subscribe(data =>{
      this.medecins = data
    }, err =>{

    })
  }

  // recuperer les salles par centre
  getSallesByCentre(){
    this.salleService.getSallesByCentre(this.search.centre).subscribe(data =>{
      this.salles = data

    })
  }

  //méthode a initialiser lors de chargement de page
  initMethod(){
    this.getSallesByCentre();
    this.getMedecinByEtablissment();
  }

  //filtrer par salle
  selectSalle(event){
    let salle = event.target.value;
    this.calendarEvents = this.calendarEventsOrigine;
    if(salle){
      this.calendarEvents = this.calendarEvents.filter(events => events.salle == salle);
    }else{
      this.calendarEvents = this.calendarEventsOrigine;
    }
  }

  //rechercher un un rdv par nom, prenom, tel ou cin
  searchPatient(event){
    let value =event.target.value

    if(value){
      let temp = []
      this.calendarEvents = this.calendarEvents.filter(events  => {
        
        console.log('events title',events.id)
        if(events.title){
          if(events.title.includes(value) || (events.cin && events.cin.includes(value)) || (events.tel && events.tel.includes(value)) ){
            console.log('events title',events)
            temp.push(events);
          }
        }
        /*else if(events.cin ){
          console.log('events cin',events.cin.includes(value))
          if(events.cin.includes(value)){
            console.log('enter cin',events)
            temp.push(events);
          }
        }*/
      });
      this.calendarEvents = temp;

    }else{
      this.calendarEvents = this.calendarEventsOrigine;
    }
    console.log('calendarEvents after',this.calendarEvents)
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  //rechercher un un rdv par medecin
  selectPraticien(event){
    let praticien = event.target.value;
    this.calendarEvents = this.calendarEventsOrigine;
    console.log('praticien',praticien)
    console.log('calendarEvents before',this.calendarEvents)
    if(praticien){
      this.calendarEvents = this.calendarEvents.filter(events => events.praticien == praticien);
    }else{
      this.calendarEvents = this.calendarEventsOrigine;
    }
    console.log('calendarEvents after',this.calendarEvents)


  }
}
