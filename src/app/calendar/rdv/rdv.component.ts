import { faLink } from '@fortawesome/free-solid-svg-icons';
import { LienService } from './../../services/lien.service';
import { FamilleActeService } from './../../services/famille-acte.service';
import { Observable, Subject } from 'rxjs';
import { faFolderOpen, faFileArchive } from '@fortawesome/free-regular-svg-icons';
import { AgendaConfig } from 'src/app/models/agendaConfig';
import { ListPatientComponent } from './../../list-patient/list-patient.component';
import { SalleService } from './../../services/salle.service';
import { AgendaService } from './../../services/agenda.service';
import { DossierPatientService } from 'src/app/services/dossier-patient.service';
import { TypeConsultationService } from './../../services/type-consultation.service';
import { DefaultService } from './../../services/default.service';
import { AgendaEvenement } from './../../models/AgendaEvenement';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';
import { MOMENT } from 'angular-calendar';
import * as moment from 'moment';
import { ReglementConsultationComponent } from 'src/app/fiche-patient/reglement-consultation/reglement-consultation.component';
import { RdvPaiementService } from 'src/app/services/rdv-paiement.service';
@Component({
  selector: 'app-rdv',
  templateUrl: './rdv.component.html',
  styleUrls: ['./rdv.component.scss']
})
export class RdvComponent implements OnInit {

  @Input() idRdv;
  @Input() calDateClicked: Date;
  @Input() class;
  @Input() agendaConfig: AgendaConfig;

// declaration icon
  faFolderOpen = faFolderOpen;
  faLink = faLink;
  dismissible = true;
  
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre rdv a été créer avec succès.',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Ce rdv a été supprimer',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  myAgenda: AgendaEvenement = {
    salle: '',
    user: '',
    auteur: '',
    dateDebut: new Date(),
    dateFin: null,
    lpatient_id: null,
    info_patient: null,
    description: '',
    heureArrivee: '',
    typeConsultation: null,
    praticien: null,
    sansRdv: false,
    heureSortie: null,
    centreSoin: null,
    legende: null,
    statut: '',
    annule: null
  }
  infoComplementaireAgenda = {
    civilite:'',
    lastname:'',
    firstname:'',
    patient: null,
    cin: ''
  }


  /*rdvForm = new FormGroup({
    lastname: new FormControl(null,[Validators.required ]),
    //password: new FormControl(null,[Validators.required ])

  })*/
  salles ;
  user;
  typeConsultations;
  centre = 1;
  patientCurrent;
  ////importé depuis list patient
  patients;
  patient;
  medecins;
  searchs = {
    lastname: '',
    firstname: '',
    date: '',
    praticien: '',
    telephone: null,
    num_dossier: null,
    centre: null
  };
  selectPatient
  @Input() isFavorite;
  public event: EventEmitter<any> = new EventEmitter();
  bsModalRef: BsModalRef;

  ///fin importation
  // rechercher un patient
  pats$! : Observable<Object>;
  private searchTerms = new Subject<string>();
  selectedFamille;
  notInterval: boolean = true;
  duree_acte: number;
  reglement2;
  constructor(private defaultService: DefaultService,
              private typeConsultationService: TypeConsultationService,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private dossierPatientService: DossierPatientService,
              private agendaService: AgendaService,
              private familleActeService: FamilleActeService,
              private lienService: LienService,
              private modalService: BsModalService,
              private rdvPaiementService: RdvPaiementService,
              private salleService: SalleService) {

  }

  myFavorite(){
    this.event.emit({rdv: 'this.isFavorite'});
  }

  ngOnInit(){
    this.getUser();
    //console.log('patient dd',this.patient)
    this.getTypeConsultation();
    this.getSallesByCentre();
    //si id de rdv existe
    if(this.idRdv){
      this.getRdvById(this.idRdv);
    }else{
      this.initAgenda();
    }
    console.log('agendaConfig', this.agendaConfig.heureDebutOuvrable.substring(0,5));
    console.log('hours', this.getNowHourSortie());
    //modal
    this.getMedecinByEtablissment();
    //this.findAll();
    this.initSearch();
    this.searchPatients();

    this.getNbreActeByDayAndTypeConsultation(new Date(),1);
  }

  compareHours(heureDebutOuvrable,heureDebutRdv,heureFinOuvrable,heureFinRdv){
    //entrée
    let dateDebutOuvrable= new Date(`01/01/2016 ${heureDebutOuvrable}`);
    let dateDebutRdv = new Date(`01/01/2016 ${heureDebutRdv}`);
    //sortie
    let dateFinOuvrable= new Date(`01/01/2016 ${heureFinOuvrable}`);
    let dateFinRdv = new Date(`01/01/2016 ${heureFinRdv}`);
    console.log(dateFinOuvrable,dateFinRdv)
    if(dateDebutRdv<dateDebutOuvrable){
      this.notInterval  = false ;
      alert("Vous devez saisir l'heure d'entrée supérieur à "+heureDebutOuvrable.substr(0,5))
    }else 
    if(dateFinRdv>dateFinOuvrable ){
      this.notInterval  = false ;
      alert("Vous devez saisir l'heure de sortie inférieur à "+heureFinOuvrable.substr(0,5))
    }
    else 
    if(dateFinRdv<dateDebutOuvrable ){
      this.notInterval  = false ;
      alert("Vous devez saisir l'heure de sortie supérieur à "+heureDebutOuvrable.substr(0,5))
    }
    else 
    if(dateDebutRdv>dateFinOuvrable ){
      this.notInterval  = false ;
      alert("Vous devez saisir l'heure d'entrée inférieur à "+heureFinOuvrable.substr(0,5))
    }
    else 
    if(dateDebutRdv>dateFinRdv ){
      this.notInterval  = false ;
      alert("Vous devez saisir l'heure d'entrée inférieur à l'heure de sortie  ")
    }
    console.log('notInterval', this.notInterval);
  }

  goToPage(patient,page){
    this.event.emit({rdv: null,action: 'close'});
    this.lienService.goToPage(patient,page);
  }

  searchPat(term: string): void {
    console.log('test',term)
    if(term){
      this.searchTerms.next(term);
    }
  }

  // pour autocomplete
  searchPatients(){
    console.log('searchFamille');
    this.pats$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(
        (term: string) =>this.dossierPatientService.searchPatient(term)
      )
    );
  }

  //remplir les deux champs lastname et firstname lors de recherche 
  selectFamille(pat){
    this.infoComplementaireAgenda ={
      firstname: pat.firstname,
      lastname: pat.lastname,
      civilite: pat.civilite,
      patient: pat.lpatient_id,
      cin: pat.cin
    }
    this.clearFamilles();
  }

  clearFamilles(){
    this.pats$ = null;
  }
  //selectionner type de consultation pour recuperer la duree
  selectTypeConsult(event){
    let id = event.target.value;
    let type =[];
    console.log('iddd',id)
    type =  this.typeConsultations.filter(type => type.id == id)
    type = type.shift();
    console.log('this.user',type);
    this.duree_acte = type['duree_name'] ;
    this.myAgenda.heureSortie =  this.getHourSortie(this.calDateClicked);
  }

  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    
  }

  //
  getMedecinByEtablissment(){
    this.userService.getMedecinByEtablissment(1).subscribe(data =>{
      this.medecins = data
      console.log('data',data);
    }, err =>{

    })
  }

  initSearch(){
    this.searchs = {
      lastname: '',
      firstname: '',
      date: this.getNowDate(),
      praticien: '',
      telephone: null,
      num_dossier: null,
      centre: this.user.centreSoin
    }
  }
  selectPatients(patient){
    this.selectPatient = patient.lpatient_id;
    console.log('selectPatients',this.selectPatient)

    this.infoComplementaireAgenda ={
      firstname: patient.firstname,
      lastname: patient.lastname,
      civilite: patient.civilite,
      patient: patient.lpatient_id,
      cin: patient.cin


    }

    //lgModal.hide()
    //this.router.navigate(['rdv'], { queryParams: patient});
    //this.router.navigate(['fiche-patient'], { queryParams: patient});
  }


  remplirSearch(f: NgForm){

    this.searchs = {
      lastname: f.value.lastname,
      firstname: f.value.firstname,
      date: f.value.date,
      praticien: f.value.praticien,
      telephone: f.value.telephone,
      num_dossier: f.value.num_dossier,
      centre: this.user.centreSoin
    }
    console.log('remplirSearch',f)
    console.log('f.form.valid',f.form.valid)
    console.log('remplirSearch',this.searchs)
    this.searchPatient();
  }
  searchPatient(){
    this.dossierPatientService.searchPatients(this.searchs).subscribe(data =>{
      this.patients = data
      console.log('data',data);
    }, err =>{

    })
  }

  getNowDate(){
    let d = this.defaultService.getNowDate();
    console.log('getNowDate',d)
    return d;
  }

  getNowHourEntree(){
      let today = new Date();
      console.log('hour',today);
      let heure = String(today. getHours()). padStart(2, '0');
      let minute = (String(today. getMinutes() + 1). padStart(2, '0'));
      return  heure + ':' + minute ;
  }

  getHourEntree(today){
    
    let heure = String(today. getHours()). padStart(2, '0');
    let minute = (String(today. getMinutes() ). padStart(2, '0')); 
    return  heure + ':' + minute ;
  }
  getHourSortieOLD(today){
    let minConfig = this.agendaConfig.granularite;
    minConfig = minConfig.substring(3, 5);
    let min = +minConfig;
    console.log('minConfig',minConfig)
    console.log('hour',today);
    let heure = +String(today. getHours()). padStart(2, '0');
    let minute = +(String(today. getMinutes() ). padStart(2, '0'));

    let zero = '';
    let zero_h = '';
    if(minute+min == 60){
      heure +=1;
      return  heure + ':00'  ;
    }else{
      minute +=min;
      if(minute==0){
        zero = '0';
      }
      if(heure<10){
        zero_h = '0';
      }
      return  zero_h + heure + ':' + minute + zero ;
    }
  }


  getHourSortie(today){
    let minConfig = this.agendaConfig.granularite;
    minConfig = minConfig.substring(3, 5);
    let min ;
    if(this.duree_acte){
      min = this.duree_acte ;
    }else{
      min = +minConfig ;
    }
    console.log('minConfig',today)
    console.log('hour',today);
    let heure = +String(today. getHours()). padStart(2, '0');
    let minute = +(String(today. getMinutes() ). padStart(2, '0'));

    let zero = '';
    let zero_h = '';
    
    
    if(minute+min == 60 ){
      heure +=1; 
      console.log('cond 1');
      return  heure + ':00'  ;
    }else if (minute+min>60){
      console.log('cond 2 avant',minute+min);
      minute =  ( minute + min ) - 60; 
      console.log('cond 2 after',minute);
      heure +=1; 
         
    }
    else{
      console.log('cond 3');
      minute +=min;
      
    }

    if(minute==0 || minute<10){
      zero = '0';
    }
    if(heure<10){
      zero_h = '0';
    }
    return  zero_h + heure + ':' + zero + minute  ;
  }

  addTimeActeToHoursExit(tempsActe,today){
    let heure = +String(today. getHours()). padStart(2, '0');
    let minute = +(String(today. getMinutes() ). padStart(2, '0'));
    let zero = '';
    let zero_h = '';
    if(minute+tempsActe == 60){
      heure +=1; 
      return  heure + ':00'  ;
    }else{
      minute +=tempsActe;
      if(minute==0){
        zero = '0';
      }
      if(heure<10){
        zero_h = '0';
      }
      return  zero_h + heure + ':' + minute + zero ;
  }
  }

  getNowHourSortie(){
    let minConfig = 10;
    let today = new Date();
    console.log('hour',today);
    let heure;
    heure = String(today. getHours()). padStart(2, '0');
    let h:number;
    h= (heure as number) ;
    console.log('typeof', typeof h);
    let minute = +(String(today. getMinutes() + 1). padStart(2, '0'));
    if(minute+minConfig ==60){
      heure +=1;


      minute = 0 ;
    }else{
      minute +=minConfig;
    }
    return  heure + ':' + minute ;
  }

  initAgenda(){
    //let date_start =  this.getDateFormatMonthDayYearSlash(this.calDateClicked);
    let date_start = this.calDateClicked;
    let heure_start = this.getHourEntree(this.calDateClicked);
    let heure_end = this.getHourSortie(this.calDateClicked);
    console.log('getDateFormatMonthDayYearSlash', heure_start);
    this.myAgenda = {
      salle: '',
      user: '',
      auteur: '',
      dateDebut: date_start,
      dateFin: date_start,
      lpatient_id: null,
      info_patient: null,
      description: '',
      heureArrivee: heure_start,
      typeConsultation: null,
      praticien: null,
      sansRdv: false,
      heureSortie: heure_end,
      centreSoin: this.user.centreSoin,
      legende: 1,
      statut: 'en cours',
      gpatient_id: null,
      annule: null
    }
  }

  remplirRdv(form: NgForm){
    ////
    this.notInterval =true;
    let date: Date;
    date = form.value.dateDebut;
    let d2= new Date(date);
    console.log('form',form);
    this.infoComplementaireAgenda ={
      firstname: form.value.firstname,
      lastname: form.value.lastname,
      civilite: form.value.civilite,
      patient: form.value.lpatient_id,
      cin: form.value.cin

    }
    let d = this.getDateFormat(d2);
    ///
    console.log('daaaa',form.value.dateDebut)
    this.myAgenda = {
      salle: form.value.salle,
      user: form.value.user,
      auteur: form.value.auteur,
      dateDebut: form.value.dateDebut,
      dateFin: form.value.dateDebut,
      lpatient_id: this.infoComplementaireAgenda.patient,
      info_patient: this.infoComplementaireAgenda,
      description: form.value.description,
      heureArrivee: form.value.heureArrivee,
      typeConsultation: form.value.typeConsultation,
      praticien: form.value.praticien,
      sansRdv: form.value.sansRdv,
      heureSortie: form.value.heureSortie,
      centreSoin: this.user.centreSoin,
      legende: form.value.legende,
      statut: form.value.statut
    }
  
    

    //compareHours()
    console.log('this.heureArrivee',form.value.heureArrivee+':00');
    this.compareHours(this.agendaConfig.heureDebutOuvrable,form.value.heureArrivee+':00',this.agendaConfig.heureFinOuvrable,form.value.heureSortie+':00');
    
    
    /*if(form.form.valid){*/
    if(this.notInterval){
      
      if(this.idRdv){
        this.updateRdv(this.idRdv);
        console.log('persistRdv',this.idRdv);

      }else{
        this.persistRdv();
        console.log('updateRdv',this.idRdv);

      }

    }
  }

  //récuperer le nbre d'acte par jour et par type acte
  getNbreActeByDayAndTypeConsultation(day,acte){
    this.agendaService.getNbreActeByDayAndTypeConsultation(day,acte).subscribe(data =>{
      console.log('nbre',data)
    })
  }

  getTypeConsultation(){
    this.typeConsultationService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.typeConsultations = data;
    
    });
  }



  getSallesByCentre(){
    this.salleService.getSallesByCentre(this.centre).subscribe(data =>{
      this.salles = data
      console.log('this.salles',this.salles);
    })
  }

  //recuperer patient selectionné
  getPatientCurrent(){
    this.route.queryParams.subscribe(params =>{
      this.patientCurrent = params;
     // this.myGDossierPatient.id = params.id;
      console.log('patientCurrent',params);
    });

  }
  getPatient(p){
    console.log('pppat',p)
  }
  persistRdv(){
    this.agendaService.persist(this.myAgenda).subscribe(data =>{
      if(data){
        this.defaultAlerts[0].show = true;
        console.log('agenda persit',this.defaultAlerts)
        //transmettre le nouveau rdv vers calendrier pour l'afficher
        console.log('this.myAgenda avant',this.myAgenda);
        this.event.emit({rdv: data,action: 'persist'});
        this.getRdvById(data.id);
        console.log('this.myAgenda apres',this.myAgenda);



      }

    })
  }
  annulerRdv(idRdv){
    this.agendaService.annulerRdv(idRdv).subscribe(data =>{
      this.defaultAlerts[2].show = true;
      this.defaultAlerts[2].msg="Votre rdv a été annulé avec succès.";
      this.event.emit({rdv: idRdv,action: 'annuler'});
    })
  }
  updateRdv(id){
    this.agendaService.update(this.myAgenda,id).subscribe(data =>{
      console.log('this.myAgenda apres',data);
      if(data){
        //this.getRdvById(data.id)
        this.event.emit({rdv: data,action: 'update'});
        this.defaultAlerts[0].show = true;
        this.defaultAlerts[0].msg="Votre rdv a été modifié avec succès.";
      }

    })
  }

  updateRdvStatus(status,id,legende){
    this.agendaService.updateStatus(status,id,legende).subscribe(data =>{
      console.log('this.myAgenda apres',data);
      if(data){
        //this.getRdvById(data.id)
        this.event.emit({rdv: data,action: 'update'});
        this.defaultAlerts[0].show = true;
        this.defaultAlerts[0].msg="Votre rdv a été modifié avec succès.";
      }

    })
  }

  getDateFormat(d){
    return this.defaultService.getDateFormat(d);
  }

  getDateFormatMonthDayYearSlash(d){
    return this.defaultService.getDateFormatMonthDayYearSlash(d);
  }

  // selectionner un rdv pour afficher les informations de rdv ceci est lors de clique sur un rdv depuis agenda

  getRdvById(id){
    this.agendaService.show(id).subscribe( data =>{
      data = data[0]
      console.log('typeConsultation',data);
      
      //console.log('typeConsultation',data[0]);
      this.myAgenda = {
        salle: data['salle'],
        user: '',
        auteur: '',
        dateDebut: new Date(data['start']) ,
        dateFin: data['end'],
        //patient: this.infoComplementaireAgenda.patient,
        lpatient_id: data['lpatient_id'],
        info_patient: this.infoComplementaireAgenda,
        description: data['description'],
        heureArrivee: data['heureArrivee'],
        typeConsultation: data['typeConsultation'],
        praticien: data['praticien'],//form.value.praticien,
        sansRdv: data['sansRdv'],
        heureSortie: data['heureSortie'],
        centreSoin: this.user.centreSoin,
        legende: data['legende'],
        statut: data['statut'],
        gpatient_id: data['gpatient_id'],
        annule: data['annule'],
      }
      console.log('tesst',new Date(data['start']))
      console.log('tesst2',data['start'])
      let lastname:string ;
      let firstname:string ;
      let civilite:string ;
      let cin:string ;
      if(!data['lpatient_id']){
        firstname = data['patient_ss_rdv_firstname'];
        lastname = data['patient_ss_rdv_lastname'];
        civilite = data['patient_ss_rdv_civilite'];
        cin = data['patient_ss_rdv_cin'];
      }else{
        firstname = data['firstname'];
        lastname = data['lastname'];
        civilite = data['civilite'];
      }
      this.infoComplementaireAgenda = {
        lastname: lastname,
        firstname: firstname,
        civilite: civilite,
        patient: data['lpatient_id'],
        cin: cin,
      }
      //this.event.emit({rdv: data});

    })
  }


  //changer l'etat d'un rdv par idrdv
  changeEtatRdv(value,id,event){
    let checked = event.target.checked
    if(checked){
      this.myAgenda.legende = id;
      this.myAgenda.statut = value;
    }else{
      this.myAgenda.statut = 'en cours';
      this.myAgenda.legende = 1;

    }

    /*console.log('this.myAgenda apres',value);
    this.agendaService.update(this.myAgenda,this.idRdv).subscribe(data =>{

    })*/
    this.updateRdv(this.idRdv);

  }



  // selectionner un patient
  selectionnerPatient(){
    let initialState = {
      patient: this.patient,
    };

    this.bsModalRef = this.modalService.show(ListPatientComponent, { initialState,class: 'modal-xl' });

    this.bsModalRef.content.event.subscribe(res => {
      let patient = res.patient;
      console.log('action get info', res)
      if(patient) {
        this.infoComplementaireAgenda = patient;
        this.infoComplementaireAgenda.patient = patient.lpatient_id;
      console.log('action get info', this.infoComplementaireAgenda.patient)

        //this.refresh_reg = true;
        //this.situationFinanciereByPatient();
        this.bsModalRef.hide();
      }else{
        this.bsModalRef.hide();

      }
    });

  }

  //fermer modal
  closeModal(){
    this.event.emit({rdv: null});
  }

  getReglementByRdv(){
    this.rdvPaiementService.getInfoRelementByRdv(this.idRdv).subscribe(data =>{
      console.log('datadata',data);
      this.reglement2 = data ;
      this.reglerRdv()
    })
  }
  //ouvrir modal de regeler les consultations un seul
  reglerRdv() {
   
    //this.reglement2.push(reglement);
    let initialState = {
      reglement: this.reglement2,
      source: 'agenda'
    };
    console.log('reglement',this.reglement2);
    this.bsModalRef = this.modalService.show(ReglementConsultationComponent, { initialState,class: 'modal-xl' });
    this.modalService.config.class = "modal-xl";
    this.bsModalRef.content.event.subscribe(res => {
      let reglement = res.reglement;
      console.log('action get info', res)
      if (reglement) {
        this.updateRdvStatus('encaisse',this.idRdv,5);
        //this.refresh_reg = true;
        //this.situationFinanciereByPatient();
        //this.listRegelementByPatient();
        this.bsModalRef.hide();
      }else{
        this.bsModalRef.hide();
      }
    });
  }

}
