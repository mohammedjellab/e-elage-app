import { NgForm } from '@angular/forms';
import { DefaultService } from './../services/default.service';
import { TypeConsultationService } from 'src/app/services/type-consultation.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { MutuelleService } from './../services/mutuelle.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-reglements',
  templateUrl: './reglements.component.html',
  styleUrls: ['./reglements.component.scss']
})
export class ReglementsComponent implements OnInit {

  
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  
  reglements;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  typeConsultations;
  total_espece = 0; 
  total_cheque = 0; 
  search = {
    date:  new Date(),
    typeConsultation: null
    //date2: this.getDateFormat(new Date()),

  }
  constructor(private mutuelleService: MutuelleService,
    private typeConsultationService: TypeConsultationService,
    private defaultService: DefaultService,
    private consultationService: ConsultationService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findRegelement();
    this.getTypeConsultation();
  }

  
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  getDateFormat(d){
    return this.defaultService.getDateFormat(d);
  }
  getTypeConsultation(){
    this.typeConsultationService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.typeConsultations = data;
    
    });
  }

  searchRgelement(f: NgForm){

    this.search = {
      typeConsultation: f.value.typeConsultation,
      date: f.value.date,
    }
    this.findRegelement();

  }
  
  
  findRegelement(){
    this.total_cheque = 0;
    this.total_espece= 0;
    this.loadedData = false;
    this.consultationService.findRegelement(this.search).subscribe(data =>{
      this.reglements = data;
      this.loadedData = true;

      this.reglements.forEach(element => {
        if(element.type_reglement=="Chéque"){
          this.total_cheque += +element.montant_regle
        }else if(element.type_reglement=="Espece"){
          this.total_espece += +element.montant_regle

        }
        
      });
      

    })
  }
 


}
