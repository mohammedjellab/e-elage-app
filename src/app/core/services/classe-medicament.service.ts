import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { ClasseMedicament } from '../models/classe-medicament';

@Injectable({
  providedIn: 'root'
})
export class ClasseMedicamentService {

  
  constructor(private http: HttpClient, private apiService: ApiService) { }

  getAll(): Observable<ClasseMedicament[]> {
    return this.http.get<ClasseMedicament[]>(`${this.apiService.urlPlatform}/classemedicaments/active`);
  }

  getLabo(id: number) : Observable<ClasseMedicament> {
    return this.http.get<ClasseMedicament>(`${this.apiService.urlPlatform}/classemedicaments/${id}`);
  }

  add(classeMedica: ClasseMedicament): Observable<ClasseMedicament> {
    return this.http.post<ClasseMedicament>(`${this.apiService.urlPlatform}/classemedicaments/add`, classeMedica);
  }

  update(classeMedica: ClasseMedicament): Observable<ClasseMedicament> {
    return this.http.put<ClasseMedicament>(`${this.apiService.urlPlatform}/classemedicaments/edit/${classeMedica.id}`, classeMedica);
  }

  delete(classeMedica: ClasseMedicament): Observable<ClasseMedicament> {
    return this.http.put<ClasseMedicament>(`${this.apiService.urlPlatform}/classemedicaments/delete/${classeMedica.id}`, {});
  }
}
