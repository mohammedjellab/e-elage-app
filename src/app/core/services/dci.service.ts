import { Dci } from 'src/app/core/models/dci';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class DciService {

  constructor(private http: HttpClient, private apiService: ApiService) { }

  getAll(): Observable<Dci[]> {
    return this.http.get<Dci[]>(`${this.apiService.urlPlatform}/dcis/active`);
  }

  getDci(id: number) : Observable<Dci> {
    return this.http.get<Dci>(`${this.apiService.urlPlatform}/dcis/${id}`);
  }

  add(dci: Dci): Observable<Dci> {
    return this.http.post<Dci>(`${this.apiService.urlPlatform}/dcis/add`, dci);
  }

  update(dci: Dci): Observable<Dci> {
    return this.http.put<Dci>(`${this.apiService.urlPlatform}/dcis/edit/${dci.id}`, dci);
  }

  delete(dci: Dci): Observable<Dci> {
    return this.http.put<Dci>(`${this.apiService.urlPlatform}/dcis/delete/${dci.id}`, {});
  }

}
