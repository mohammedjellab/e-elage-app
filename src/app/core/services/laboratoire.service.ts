import { Laboratoire } from './../models/laboratoire';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class LaboratoireService {

  constructor(private http: HttpClient, private apiService: ApiService) { }

  getAll(): Observable<Laboratoire[]/* | string*/> {
    return this.http.get<Laboratoire[]/* | string*/>(`${this.apiService.urlPlatform}/laboratoires/active`);
  }

  getLabo(id: number) : Observable<Laboratoire/*| string*/> {
    return this.http.get<Laboratoire/* | string*/>(`${this.apiService.urlPlatform}/laboratoires/${id}`);
  }

  add(labo: Laboratoire): Observable<Laboratoire> {
    return this.http.post<Laboratoire>(`${this.apiService.urlPlatform}/laboratoires/add`, labo);
  }

  update(labo: Laboratoire): Observable<Laboratoire/* | string*/> {
    return this.http.put<Laboratoire /*| string*/>(`${this.apiService.urlPlatform}/laboratoires/edit/${labo.id}`, labo);
  }

  delete(labo: Laboratoire): Observable<Laboratoire/* | string*/> {
    return this.http.put<Laboratoire/*  | string*/>(`${this.apiService.urlPlatform}/laboratoires/delete/${labo.id}`, {});
  }


}
