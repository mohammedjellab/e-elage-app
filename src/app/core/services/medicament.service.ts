import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { Medicament } from '../models/medicament';

@Injectable({
  providedIn: 'root'
})
export class MedicamentService {

  constructor(private http: HttpClient, private apiService: ApiService) { }

  getAll(): Observable<Medicament[]> {
    return this.http.get<Medicament[]>(`${this.apiService.urlPlatform}/medicaments/active`);
  }

  getLabo(id: number) : Observable<Medicament> {
    return this.http.get<Medicament>(`${this.apiService.urlPlatform}/medicaments/${id}`);
  }

  add(medica: Medicament): Observable<Medicament> {
    return this.http.post<Medicament>(`${this.apiService.urlPlatform}/medicaments/add`, medica);
  }

  update(medica: Medicament): Observable<Medicament> {
    return this.http.put<Medicament>(`${this.apiService.urlPlatform}/medicaments/edit/${medica.id}`, medica);
  }

  delete(medica: Medicament): Observable<Medicament> {
    return this.http.put<Medicament>(`${this.apiService.urlPlatform}/medicaments/delete/${medica.id}`, {});
  }
}
