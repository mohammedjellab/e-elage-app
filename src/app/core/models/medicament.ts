export interface Medicament {
    id?: number;
    nomCommercial?: string;
    laboratoire?: any;
    classeMedicale?: any;
    famille?: any;
    designation?: string;
    posologie?: string;
    forme?: string;
    prixVente?: string;
    prixAchat?: string;
    prixBRemboursementPPM?: number, 
    prixHospitalier?: number, 
    baseRembousementPPV?: number, 
    prixBaseRemboursement?: number, 
    duree?: number; 
    quantite?: number; 
    consigne?: string, 
    rembourse?: boolean,
    dosage?: string,
    presentation?: string, 
    statut?: Statut, 
    codeBarre?: string;
    surdosage?: string;
    codeAtc?: string;
    natureProduit?: string;
    substancePsychoactive?: string; 
    utilisation?: string;
    contreIndications?: string; 
    effetsIndesirables?: string; 
    conditionsPrescription?: string;
    indications?: string, 
    type?: string,
    risquePotentiel?: string;
    dcis?: Array<string>;
}

enum Statut {
    'commercialisé', 
    'non commercialisé'
}