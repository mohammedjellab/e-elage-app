export interface Laboratoire {
    id?: number;
    name?: string;
    tel?: string;
    adresse?: string;
    types?: Array<any>;
}
