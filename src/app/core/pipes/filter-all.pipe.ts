import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAll'
})
export class FilterAllPipe implements PipeTransform {

  transform(value: any = [], ...args: any[]): any {
    /*console.log('--------- Transform Filter Pipe -------------');
    console.log('--------- Transform Filter Pipe Args -- SearchText ------------- ', args);
    console.log('--------- Transform Filter Pipe Value -- Data ------------- ', value);*/
    if(!args) {
      return value;
    }
    return value ? value.filter((data) => this.matchValue(data,args)) : ''; 
  }

  matchValue(data, value) {
    /*console.log('--------- Match Filter Pipe -------------');
    console.log('--------- Match Filter Pipe Data ------------- ', data);
    console.log('--------- Match Filter Pipe Value ------------- ', value);*/
    return Object.keys(data).map((key) => {
       return new RegExp(value, 'gi').test(data[key]);
    }).some(result => result);
  }

}
