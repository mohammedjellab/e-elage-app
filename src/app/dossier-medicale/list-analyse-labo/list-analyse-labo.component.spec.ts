import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAnalyseLaboComponent } from './list-analyse-labo.component';

describe('ListAnalyseLaboComponent', () => {
  let component: ListAnalyseLaboComponent;
  let fixture: ComponentFixture<ListAnalyseLaboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAnalyseLaboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAnalyseLaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
