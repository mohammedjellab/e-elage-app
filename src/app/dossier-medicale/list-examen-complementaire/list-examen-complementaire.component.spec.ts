import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExamenComplementaireComponent } from './list-examen-complementaire.component';

describe('ListExamenComplementaireComponent', () => {
  let component: ListExamenComplementaireComponent;
  let fixture: ComponentFixture<ListExamenComplementaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListExamenComplementaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExamenComplementaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
