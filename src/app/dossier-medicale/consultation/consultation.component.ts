import { TypeConsultationService } from './../../services/type-consultation.service';
import { ConsultationService } from '../../services/consultation.service';
import { Component, Input, OnInit,EventEmitter } from '@angular/core';
import { Consultation } from '../../models/consultation';


@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss']
})
export class ConsultationComponent implements OnInit {

  @Input() patient;
  public event: EventEmitter<any> = new EventEmitter();

  myConsultation: Consultation = {
    montant: null,
    new: false,
    lpatient: null,
    centreSoin: 1,
    typeConsultation: null
  }
  typeConsultations;
  user;
  constructor(private consultationService: ConsultationService,
              private typeConsultationService: TypeConsultationService) { }

  ngOnInit(){
    this.getUser();
    console.log('patientpatient',this.patient);
    this.myConsultation.lpatient = this.patient;
    this.getTypeConsultation();
  }

  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  getTypeConsultation(){
    this.typeConsultationService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.typeConsultations = data;
      console.log('typeConsultations',data)
    });
  }


  saveConsultation(){
    this.consultationService.persist(this.myConsultation).subscribe(data =>{

    });
  }

  checkNewConsultation(){
    this.myConsultation.new = !this.myConsultation.new;
  }
  remplirConsultation(f){

    if(this.myConsultation.new){
      this.myConsultation.montant = f.value.montant;
      this.myConsultation.typeConsultation = f.value.typeConsultation;
      this.myConsultation.lpatient = this.patient;
      this.saveConsultation();
      this.event.emit({rdv: 'submit'});
      console.log('submit');
    }else{
      this.event.emit({rdv: 'reset'});
      console.log('reset');
    }

  }



}
