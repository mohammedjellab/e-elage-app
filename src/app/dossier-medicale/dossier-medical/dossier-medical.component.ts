import { BsModalRef } from 'ngx-bootstrap/modal';
import { ComorbiditeService } from './../../services/comorbidite.service';
import { DefaultService } from './../../services/default.service';
import { ApiService } from './../../services/api.service';
import { AgendaService } from './../../services/agenda.service';
import { GDossierPatient } from './../../models/gDossierPatient';
import { MutuelleService } from './../../mutuelle.service';
import { MutuelleFaiteService } from './../../services/mutuelle-faite.service';
import { ConsultationService } from './../../services/consultation.service';
import { VilleService } from './../../services/ville.service';
import { PaysService } from './../../services/pays.service';
import { UserService } from './../../services/user.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DossierPatientService } from './../../services/dossier-patient.service';
import { Component, OnInit } from '@angular/core';
import { LDossierPatient } from '../../models/lDossierPatient';
import { AntecedentService } from '../../services/antecedent.service';
import { DossierMedicalService } from '../../services/dossier-medical.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ModeleConsulatationService } from '../../services/modele-consulatation.service';
import { BsModalService } from 'ngx-bootstrap';
import { ConsultationComponent } from '../consultation/consultation.component';
import { infoPatientDm } from 'src/app/models/dossierMedicalPatient';

@Component({
  selector: 'app-dossier-medical',
  templateUrl: './dossier-medical.component.html',
  styleUrls: ['./dossier-medical.component.scss']
})
export class DossierMedicalComponent implements OnInit {

  typeDocuments = [
    {
    name: 'imagerie'
    },
    {
      name: 'bilan'
    },
    {
      name: 'Documents'
    },
    {
      name: 'ECG'
    },
    {
      name: 'ECHO'
    },
    {
      name: 'Courier'
    }
  ]
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre patient a été créer avec succès.',
      show: false
    },
    {
      type: 'info',
      msg: 'This alert needs your attention, but it\'s not super important.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Better check yourself, you\'re not looking too good.',
      show: false
    },
    {
      type: 'warning',
      msg: 'This is a warning type alert that is dismissible.',
      show: false
    }
  ];
  patient = {
    lpatient: null,
    nom_complet: '',
    mutuelle: '',
    num_dossier: null,
    nbEnfant: null,
    typeConsulation: null,
    gpatient:2,
    diagnostic: null

  };
  myPatient: infoPatientDm ={
    lpatient_id: null,
    nom_complet: '',
    mutuelle: '',
    num_dossier: null,
    nbEnfant: null,
    typeConsulation: null,
    gpatient_id:null,
    diagnostic: null,
    centreSoin: null
  } ;
  user;
  antecedents;
  antecedentsPatient;

  diagnostic;
  htmlContent = '';

  //config wysiwyg
  name = 'ng2-ckeditor';
  ckeConfig ;//CKEDITOR.config;
  mycontent: string;
  log: string = '';
  event;
  centreSoin: number = 1;
  modeleConsultation;
  body: string;
  date_consulatation;
  isChangedDate: boolean = false;
  idDossier:number = 1;
  dossierMedical;
  myDossierMedicale= {
    comorbidite: null,
    dateConsultation: null,
    description: null,
    commentaire: null,
    diagnostic: null,
    centreSoin: null
  }
  comorbidites;
  data = 'test';
  //modal
  bsModalRef: BsModalRef;
  modalShow: boolean = false;
  constructor(private userService: UserService,
              private paysService: PaysService,
              private antecedentService: AntecedentService,
              private consultationService: ConsultationService,
              private mutuelleFaiteService: MutuelleFaiteService,
              private dossierPatientService: DossierPatientService,
              private dossierMedicalService: DossierMedicalService,
              private agendaService: AgendaService,
              private comorbiditeService: ComorbiditeService,
              private apiService: ApiService,
              private defaultService: DefaultService,
              private modeleConsulatationService: ModeleConsulatationService,
              private mutuelleService: MutuelleService,
              private modalService: BsModalService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getUser();
    this.getPatientCurrent();
    this.getAntecedantByPatient();
    //this.getMutuelle();

    //console.log('dddd',this.getAge('14/06/2021'))
    /*let date = new Date();
    console.log('dattt',date.getFullYear());*/
    this.getModeleConsultationByCentreSoin();
    this.date_consulatation = this.getDateFormatMonthDayYearSlash(new Date());
    this.getInformationForDM();
    this.findAllComorbidite();
    console.log('consultation modal',this.patient)

  }

  doSomething(event){
    console.log('params.id',event);

  }
  newConsultation(){
    this.modalCreationConsultation(this.patient.lpatient);
  }

  //recuperer les fichiers par dossier medical


  /*changeTypeDocument(type){
    this.typeDocument = type.typeDocument;
    console.log('this.typeDocument',type);
  }*/

  //recuperer patient selectionné
  getPatientCurrent(){
    this.route.queryParams.subscribe(params =>{
      console.log('params.id',params);
      this.dossierPatientService.showLpatient(params["lpatient_id"],this.user.centreSoin).subscribe(data=>{
        this.myPatient =data
        console.log('params.id',data.nbEnfant);
      })

      this.patient.lpatient = params.lpatient_id;
      console.log('patientCurrent',params["lpatient_id"]);
      console.log('params.id',params);
    });

  }


  //l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }



  /*getRdvByPatient(patient){
    console.log('current Patient',patient)
    this.agendaService.rdvByPatient(patient).subscribe(data =>{
      this.patient = data
      this.nom_complet= data
      console.log('patient',data)
      console.log(' this.myLDossierPatient', this.patient)
      //this.myDossierPatient.dateNaissance = data['dateNaissance'];
    },err=>{

    })
  }*/

  //recuperer le modele de consultation
  getModeleConsulatation(){
    this.modeleConsulatationService.findByCentreSoin(this.centreSoin).subscribe(data =>{
      this.modeleConsultation = data
    })
  }

  //
  addConsultation(){

    this.addConsultationInCkeditor();
    this.updateConsultionInDm(this.idDossier);
  }
  //modifier la description dans bdd
  updateConsultionInDm(idDossier){
    console.log('this.mycontent',this.mycontent)
    this.dossierMedicalService.updateDescriptionInDm(idDossier,this.body).subscribe(data =>{

    })
  }
  remplirData(formDM){
    this.addConsultation();
    console.log('fff',formDM);
    this.mycontent = formDM.value.mycontent;
  }
  //ajouter l'entete de consultation dans ck editor
  addConsultationInCkeditor(){
    let re = /date_consultation/gi;
    let date = this.date_consulatation;
    if(this.isChangedDate){
       date = this.getDateFormatMonthDayYearSlash(this.date_consulatation);

    }
    console.log('date',date);
    this.modeleConsultation.body = this.body.replace(re, date);
    console.log('this.date_consulatation',this.body);

    this.event.editor.insertHtml(this.modeleConsultation.body);
  }

  changeDate(){
    this.isChangedDate = true
  }




  getModeleConsultationByCentreSoin(){
    this.modeleConsulatationService.findByCentreSoin(this.centreSoin).subscribe(data=>{
      this.modeleConsultation = data ;
      this.body = this.modeleConsultation.body ;
    })
  }



  onChange($event: any): void {
    console.log("onChange");
    //this.log += new Date() + "<br />";
  }

  onPaste($event: any): void {
    console.log("onPaste");
    //this.log += new Date() + "<br />";
  }


  onFocus(event){
    this.event = event;
    var selection = event.editor.getSelection();
    var ranges = selection.getRanges();
    var range = ranges[0];

    var newRange = event.editor.createRange();

    var moveToEnd = true;
    newRange.moveToElementEditablePosition(range.endContainer, moveToEnd);

    var newRanges = [newRange];
    selection.selectRanges(newRanges);

    //
  }


  getDateFormatMonthDayYearSlash(d){
    return this.defaultService.getDateFormatMonthDayYearSlash(d);
  }

  //recuperer la description
  getInformationForDM(){

    this.dossierMedicalService.getInformationForDM(this.idDossier).subscribe(data =>{
      this.dossierMedical = data
      //this.body = this.dossierMedical.body
      console.log('this.dossierMedical',this.dossierMedical)
    })
  }

  //selectionner un antecedant
  checkAntecedant(antecedant){
    this.antecedentService.saveAntecedantPatient(antecedant,this.patient.lpatient).subscribe(data =>{

    })
    console.log('eee')
  }

  //recuperer la liste des antécedants par patient
  getAntecedantByPatient(){
    this.antecedentService.getAntecedantByPatient(this.patient.lpatient).subscribe(data =>{
      this.antecedents = data
    })
    console.log('eee')
  }

  //enregistrer/modifier comorobidite
  saveComorbidite(e){
    this.dossierMedicalService.saveComorbidite(this.patient.gpatient,e.target.value).subscribe(data=>{

    })
  }
  change(event) {
    console.log('event',event)

  }

  saveDiagnostic(event){
    console.log('eee',this.diagnostic)
    console.log('eee',event)
    /*this.dossierMedicalService.saveDiagnostic(this.patient.lpatient,e.target.value).subscribe(data=>{

    })*/
  }

  findAllComorbidite(){
    this.comorbiditeService.findAll().subscribe(data =>{
      this.comorbidites =data['hydra:member']
    })
  }

  //creation de consultation
  modalCreationConsultation(patient){
    this.modalShow =true;
    let initialState = {
      patient: patient,
    };
    this.bsModalRef = this.modalService.show(ConsultationComponent,{initialState});

    this.bsModalRef.content.event.subscribe(res => {
      let rdv = res.rdv;
      console.log('action',res)

      if(res){
        this.hideModal();

      }

   });
   /*(title,
    setHours(setMinutes(new Date(rdv[0]['year_start'], rdv[0]['month_start']-1, rdv[0]['day_start']), rdv[0]['minute_start']), rdv[0]['hours_start']),
    setHours(setMinutes(new Date(rdv[0]['year_end'], rdv[0]['month_end']-1, rdv[0]['day_end']), rdv[0]['minute_end']), rdv[0]['hours_end']),*/

  }
  hideModal() {
    this.modalService.hide(1);
  }

}
