import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreadcrumbDossierMedicaleComponent } from './breadcrumb-dossier-medicale.component';

describe('BreadcrumbDossierMedicaleComponent', () => {
  let component: BreadcrumbDossierMedicaleComponent;
  let fixture: ComponentFixture<BreadcrumbDossierMedicaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreadcrumbDossierMedicaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbDossierMedicaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
