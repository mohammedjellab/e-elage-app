import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThemeOptions } from '../theme-options';

@Component({
  selector: 'app-breadcrumb-dossier-medicale',
  templateUrl: './breadcrumb-dossier-medicale.component.html',
  styleUrls: ['./breadcrumb-dossier-medicale.component.scss']
})
export class BreadcrumbDossierMedicaleComponent implements OnInit {

  @Input() titleHeading: string;
  @Input() titleDescription: string;
  //event: EventEmitter<any> = new EventEmitter();

  @Output() event = new EventEmitter();
  constructor(public globals: ThemeOptions) {
  }


  ngOnInit() {

  }
  addPatient() {
    this.event.emit({ add_patient: true })
  }

  newConsultation() { }

}
