import { Injectable } from '@angular/core';

interface MenuItem {
    title: string;
    type: string;
    badge?: {
        class: string;
        text: string;
    };
    link?: string;
    active?: boolean;
    icon?: string;
    submenus?: MenuItem[];
}

@Injectable({
    providedIn: 'root'
})
export class SidebarMenuService {

    menus: MenuItem[] = [
  /**/{
    "title": "Nom de l'établissement",
    "type": "header"
  },
  {
    "title": "Mes patients",
    "type": "simple",
    "icon": "<i class=\"pe-7s-users\"></i>",
    "link": "/list-patient"
  },
  {
    "title": "Agenda",
    "type": "simple",
    "icon": "<i class=\"pe-7s-date\"></i>",
    "link": "/calendar"
  },
  {
    "title": "Salle d'attente",
    "type": "simple",
    "icon": "<i class=\"pe-7s-stopwatch\"></i>",
    "link": "/list-rdv"
  },
  {
    "title": "Mes interventions",
    "type": "simple",
    "icon": "<i class=\"pe-7s-check\"></i>",
    "link": "/mes-interventions"
  },
  {
    "title": "Reglement",
    "type": "simple",
    "icon": "<i class=\"pe-7s-cash\"></i>",
    "link": "/reglements"
  },

  {
    "title": "Statistique",
    "type": "simple",
    "icon": "<i class=\"pe-7s-graph3\"></i>",
    "link": "/statistique"
  },
  {
    "title": "Parametrage",
    "type": "dropdown",
    "icon": "<i class=\"pe-7s-config\"></i>",
    /*"badge": {
      "class": "badge badge-success",
      "text": "12"
    },*/
    "submenus": [
      {
        "title": "Praticien",
        "type": "simple",
        "link": "/setting-praticien"
      },
      {
        "title": "Cabinet",
        "type": "simple",
        "link": "/setting-cabinet"
      },
      {
        "title": "Patient",
        "type": "simple",
        "link": "/setting-patient"
      },
      {
        "title": "Dossier Médicale",
        "type": "simple",
        "link": "/setting-dossier-medicale"
      },
      {
        "title": "Agenda",
        "type": "simple",
        "link": "/setting-agenda"
      },
      /*{
        "title": "Type Utilisateur",
        "type": "simple",
        "link": "/user-type"
      },

      {
        "title": "TEst3",
        "type": "simple",
        "link": "/test3"
      },
      {
        "title": "Agenda config",
        "type": "simple",
        "link": "/agenda-config"
      },

      {
        "title": "Specialité",
        "type": "simple",
        "link": "/specialite"
      },
      {
        "title": "Arborescence",
        "type": "simple",
        "link": "/arborescence"
      },
      {
        "title": "Famille acte",
        "type": "simple",
        "link": "/famille-acte"
      },
      {
        "title": "Etablissment",
        "type": "simple",
        "link": "/centre-soins"
      },
      {
        "title": "Annuaire",
        "type": "simple",
        "link": "/annuaire"
      },
      {
        "title": "Salle",
        "type": "simple",
        "link": "/salle"
      },
      {
        "title": "Comorbidité",
        "type": "simple",
        "link": "/comorbidite"
      },
      {
        "title": "Motif Consultation",
        "type": "simple",
        "link": "/motif-consultation"
      },
      {
        "title": "Mutuelle",
        "type": "simple",
        "link": "/mutuelle"
      },*/
      /*{
        "title": "Examen Complémentaire",
        "type": "simple",
        "link": "/examen-complementaire"
      },
      {
        "title": "Utilisateurs",
        "type": "simple",
        "link": "/utilisateur"
      },*/
      /*{
        "title": "Modele de consultation",
        "type": "simple",
        "link": "/modele-consultation"
      },
      {
        "title": "Unité",
        "type": "simple",
        "link": "/unite-constante"
      },
      {
        "title": "Constante médicales",
        "type": "simple",
        "link": "/constante-medical"
      },
      {
        "title": "Antécédent",
        "type": "simple",
        "link": "/antecedent"
      },
      {
        "title": "Type Consultation",
        "type": "simple",
        "link": "/type-consultation"
      },*//*

      {
        "title": "Test",
        "type": "simple",
        "link": "/test"
      },
      {
        "title": "landing page",
        "type": "simple",
        "link": "/landing-page"
      }*/
    ]
  },
  {
    "title": "site web",
    "type": "simple",
    "icon": "<i class=\"pe-7s-graph3\"></i>",
    "link": "/elage-patient-acceuil"
  },

];

    constructor() { }

    getMenuList() {
        return this.menus;

    }

    getMenuItemByUrl(aMenus: MenuItem[], aUrl: string): MenuItem {
        for (const theMenu of aMenus) {
            if (theMenu.link && theMenu.link === aUrl) {
                return theMenu;
            }

            if (theMenu.submenus && theMenu.submenus.length > 0) {
                const foundItem = this.getMenuItemByUrl(theMenu.submenus, aUrl);
                if (foundItem) {
                    return foundItem;
                }
            }
        }

        return undefined;
    }

    toggleMenuItem(aMenus: MenuItem[], aCurrentMenu: MenuItem): MenuItem[] {
        return aMenus.map((aMenu: MenuItem) => {
            if (aMenu === aCurrentMenu) {
                aMenu.active = !aMenu.active;
            } else {
                aMenu.active = false;
            }

            if (aMenu.submenus && aMenu.submenus.length > 0) {
                aMenu.submenus = this.toggleMenuItem(aMenu.submenus, aCurrentMenu);

                if (aMenu.submenus.find(aChild => aChild.active)) {
                    aMenu.active = true;
                }
            }

            return aMenu;
        });
    }
}
