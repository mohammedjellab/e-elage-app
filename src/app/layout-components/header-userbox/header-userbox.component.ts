import { Router } from '@angular/router';
import { AuthService } from './../../services/auth';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-userbox',
  templateUrl: './header-userbox.component.html'
})
export class HeaderUserboxComponent implements OnInit {
  
  user;
  constructor(private auth: AuthService,private router: Router){

  }
  ngOnInit(){
    this.getUser();
  }

  logout(){
    let user = localStorage.getItem('user');
    this.auth.logout(user).subscribe(data =>{
      if(data){
        localStorage.setItem('logout',JSON.stringify(data));
        localStorage.removeItem('user');
        this.router.navigate(['pages-register']);
      }
    },err =>{

    });
    
  }

  


  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('local',this.user)
  }
}
