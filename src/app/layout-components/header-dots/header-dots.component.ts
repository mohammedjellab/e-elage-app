import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth';

@Component({
  selector: 'app-header-dots',
  templateUrl: './header-dots.component.html'
})
export class HeaderDotsComponent {

  constructor(private auth: AuthService,private router: Router){

  }

  logout(){
    let user = localStorage.getItem('user');
    this.auth.logout(user).subscribe(data =>{
      if(data){
        localStorage.setItem('logout',JSON.stringify(data));
        localStorage.removeItem('user');
        this.router.navigate(['pages-register']);
      }
    },err =>{

    });
    console.log('local',user)
  }
}
