import { Component, Input, OnChanges, OnInit, Renderer2, Inject } from '@angular/core';


import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-cal-test',
  templateUrl: './cal-test.component.html',
  styleUrls: ['./cal-test.component.scss']
})
export class CalTestComponent implements OnInit, OnChanges {
  @Input()
  public foreground = "black";

  @Input()
  public background = "white";
  color = "red";
  public get style(): string {
    return `
    .pdf-dynamic-css {
      font-size:33px;
      background-color: ${this.color}  /* doesn't interpolate the TS variable */
    }
    `;
  }
//fc-time-grid .fc-slats td
constructor(private renderer: Renderer2, @Inject(DOCUMENT) private document: any) {}

ngOnInit() {
  this.initialiseCss();
}

initialiseCss(){
  const styles = this.document.createElement('STYLE') as HTMLStyleElement;
  styles.id = 'dynamic-theme-css';
  styles.innerHTML = this.style;
  this.renderer.appendChild(this.document.head, styles);
}
ngOnChanges() {
  const styles = this.document.getElementById('dynamic-theme-css');
  if (styles) {
    styles.innerHTML = this.style;
  }
}
}
