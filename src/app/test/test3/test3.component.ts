import { Component, AfterViewChecked, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';

import { FullCalendarComponent } from '@fullcalendar/angular'; 
import { EventInput, Calendar } from '@fullcalendar/core'; 
import dayGridPlugin from '@fullcalendar/daygrid'; 
import timeGrigPlugin from '@fullcalendar/timegrid'; 
import interactionPlugin from '@fullcalendar/interaction'; 

import Tooltip from 'tooltip.js'; 
@Component({
  selector: 'app-test3',
  templateUrl: './test3.component.html',
  styleUrls: ['./test3.component.scss'],
  encapsulation: ViewEncapsulation.None
  
})


export class Test3Component implements OnInit {

 
  @ViewChild('fullcalendar',null) calendarComponent: FullCalendarComponent;
  
  calendarEvents: EventInput[] = [];

  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];

  calendarApi: Calendar; 
  initialized = false; 
  toolData;
  constructor() { }

  ngOnInit() {
  }
 
  ngAfterViewChecked() { 
    this.calendarApi = this.calendarComponent.getApi(); 
 
    if (this.calendarApi && !this.initialized) { 
      this.initialized = true; 
      this.loadEvents(); 
    } 
  } 
  
  loadEvents() {
    const event = {
      title: 'test',
      start: Date.now(),
      allDay: true
    };
    this.calendarEvents.push(event);
          this.calendarApi.removeAllEventSources(); 
      this.calendarApi.addEventSource(this.calendarEvents); 
  } 
 
  onDateClick(clickedDate: any) {
    console.log(clickedDate);
  }

  onEventClick(clickedEvent: any) {
    console.log(clickedEvent);
  }

  onEventRender(info: any) { 
    
    /*const tooltip = new Tooltip(info.el, { 
      title: info.event.title, 
      placement: 'top-end', 
      trigger: 'hover', 
      container: 'body',
       
    }); 
    console.log('onEventRender', tooltip); 
    return tooltip;*/
    
  } 

  mouseOver(event){
    let d = document.getElementsByClassName("fc-toolbar fc-header-toolbar")
    //fc-toolbar fc-header-toolbar

  }

}