import { AnnuaireModule } from './settings/annuaire/annuaire.module';
import { ComorbiditeModule } from './settings/comorbidite/comorbidite.module';
import { MotifConsultationModule } from './settings/motif-consultation/motif-consultation.module';
import { SpecialiteModule } from './settings/specialite/specialite.module';
import { CentreSoinModule } from './settings/centre-soin/centre-soin.module';
import { SalleModule } from './settings/salle/salle.module';
import { UserTypeModule } from './settings/user-type/user-type.module';
import { PagesLoginMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-login-mobile/pages-login-mobile.component';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThemeOptions } from './theme-options';

// Widgets

// NGX Bootstrap Core



// NGX Bootstrap Buttons

import { ButtonsModule } from 'ngx-bootstrap';

// NGX Bootstrap Collapse

import { CollapseModule } from 'ngx-bootstrap';

// NGX Bootstrap Timepicker

import { TimepickerModule } from 'ngx-bootstrap';

// NGX Bootstrap Typeahead

import { TypeaheadModule } from 'ngx-bootstrap';

// NGX Bootstrap Carousel

import { CarouselModule } from 'ngx-bootstrap';

// NGX Bootstrap Dropdown

import { BsDropdownModule } from 'ngx-bootstrap';

// NGX Bootstrap Datepicker

import { BsDatepickerModule } from 'ngx-bootstrap';

// NGX Bootstrap Modal

import { ModalModule } from 'ngx-bootstrap';

// NGX Bootstrap Pagination

import { PaginationModule } from 'ngx-bootstrap';

// NGX Bootstrap Progress bar

import { ProgressbarModule } from 'ngx-bootstrap';

// NGX Bootstrap Tabs

import { TabsModule } from 'ngx-bootstrap';

// NGX Bootstrap Popover

import { PopoverModule } from 'ngx-bootstrap';

// NGX Bootstrap Tooltip

import { TooltipModule } from 'ngx-bootstrap';

// NGX Bootstrap Accordion

import { AccordionModule } from 'ngx-bootstrap';

// NGX Bootstrap Alert

import { AlertModule } from 'ngx-bootstrap';

// Bootstrap Core



// ApexCharts for Angular

import { NgApexchartsModule } from 'ng-apexcharts';

// Apex Charts



// Perfect Scrollbar

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: false
};

// Ngx UI Switch

import { UiSwitchModule } from 'ngx-ui-switch';

// NG2 File Upload

import { FileUploadModule } from 'ng2-file-upload';

// NGX Dropzone for Angular

import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

// Formly Core

import { FormlyModule } from '@ngx-formly/core';

// Formly Bootstrap

import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

// Archwizard

import { ArchwizardModule } from 'angular-archwizard';

// AngularEditor

import { AngularEditorModule } from '@kolkov/angular-editor';

// Angular Circle Progress

import { NgCircleProgressModule } from 'ng-circle-progress';

// Angular Gauge Wrapper

import { GaugeModule } from 'angular-gauge';

// Angular Circle Progress

import { NgxGaugeModule } from 'ngx-gauge';

// Angular Ladda2 Buttons Loader

import { LaddaModule } from 'angular2-ladda';

// Angular FullWidth Calendar

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

// Date-fns



// NGX Slick Carousel

import { SlickCarouselModule } from 'ngx-slick-carousel';

// jQuery



// Slick Carousel



// NG2 Charts

import { ChartsModule } from 'ng2-charts';

// Chart.js Annotations



// Chart.js Datalabels
import { DataTablesModule } from 'angular-datatables';



// Chart.js



// NGX Context Menu

import { ContextMenuModule } from 'ngx-contextmenu';

// Angular Component Development Kit



// Angular CountUp

import { CountUpModule } from 'countup.js-angular2';

// Dragula for Angular

import { DragulaModule } from 'ng2-dragula';

// NG2 Table

import { Ng2TableModule } from 'ng2-table';

// NG2 Smart Table

import { Ng2SmartTableModule } from 'ng2-smart-table';

// NG2 Completer

import { Ng2CompleterModule } from 'ng2-completer';

// Angular Colorpicker

import { ColorPickerModule } from 'ngx-color-picker';

// NGX Clipboard

import { ClipboardModule } from 'ngx-clipboard';

// NG Select

import { NgSelectModule } from '@ng-select/ng-select';

// NGX Autosize

import { AutosizeModule } from 'ngx-autosize';

// Angular noUiSlider

import { NouisliderModule } from 'ng2-nouislider';

// noUiSlider



// Angular Flags Icons

import { NgxFlagIconCssModule } from 'ngx-flag-icon-css';

// SVG Flags Icons



// Angular Feather Icons

import { FeatherModule } from 'angular-feather';
import { Calendar, Activity, Bell, Settings, Search, Grid, Users, LifeBuoy, CloudDrizzle, Coffee, Box, Briefcase, Layers, Printer } from 'angular-feather/icons';
const icons = {
  Calendar,
  Activity,
  Bell,
  Settings,
  Search,
  Grid,
  Users,
  LifeBuoy,
  CloudDrizzle,
  Coffee,
  Box,
  Briefcase,
  Layers,
  Printer
};

// FontAwesome Regular SVG Icons

import { faSquare, faCheckCircle, faTimesCircle, faDotCircle, faThumbsUp, faComments, faFolderOpen, faTrashAlt, faFileImage, faFileArchive, faLifeRing, faCommentDots, faFolder, faKeyboard, faCalendarAlt, faEnvelope, faAddressCard, faMap, faObjectGroup, faImages, faUser, faLightbulb, faGem, faClock, faUserCircle, faQuestionCircle, faBuilding, faBell, faFileExcel, faFileAudio, faFileVideo, faFileWord, faFilePdf, faFileCode, faFileAlt, faEye, faChartBar } from '@fortawesome/free-regular-svg-icons';

// FontAwesome Solid SVG Icons

import { faChevronRight, faSitemap, faPrint, faMapMarkerAlt, faTachometerAlt, faAlignCenter, faExternalLinkAlt, faShareSquare, faInfoCircle, faSync, faQuoteRight, faStarHalfAlt, faShapes, faCarBattery, faTable, faCubes, faPager, faCameraRetro, faBomb, faNetworkWired, faBusAlt, faBirthdayCake, faEyeDropper, faUnlockAlt, faDownload, faAward, faPlayCircle, faReply, faUpload, faBars, faEllipsisV, faSave, faSlidersH, faCaretRight, faChevronUp, faPlus, faLemon, faChevronLeft, faTimes, faChevronDown, faFilm, faSearch, faEllipsisH, faCog, faArrowsAltH, faPlusCircle, faAngleRight, faAngleUp, faAngleLeft, faAngleDown, faArrowUp, faArrowDown, faArrowRight, faArrowLeft, faStar, faSignOutAlt, faLink } from '@fortawesome/free-solid-svg-icons';

// FontAwesome Brand SVG Icons

import { faFacebook, faTwitter, faAngular, faVuejs, faReact, faHtml5, faGoogle, faInstagram, faPinterest, faYoutube, faDiscord, faSlack, faDribbble, faGithub } from '@fortawesome/free-brands-svg-icons';

// Angular FontAwesome Icons

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';

// Angular FontAwesome Icons Core



// Ionicons



// Pe7 Icons



// Socicons Icons



// NG Spin Kit

import { NgSpinKitModule } from 'ng-spin-kit';

// NGX Skeleton Loader

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

// Angular Progressbar Core

import { NgProgressModule } from '@ngx-progressbar/core';

// Angular Progressbar router module

import { NgProgressRouterModule } from '@ngx-progressbar/router';

// NGX Spinner

import { NgxSpinnerModule } from 'ngx-spinner';

// Angular Google Maps

import { AgmCoreModule } from '@agm/core';

// Angular SweetAlerts2 Notifications

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

// SweetAlerts2 Notifications



// Angular Notifier

import { NotifierModule, NotifierOptions } from 'angular-notifier';
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'uifort',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

// NGX Pagination for Angular

import { NgxPaginationModule } from 'ngx-pagination';

// NGX Ratings for Angular

import { BarRatingModule } from 'ngx-bar-rating';

// Angular Infinite Scroll

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// Angular Elegant Trends Graphs

import { TrendModule } from 'ngx-trend';

// Angular Tree Component

import { TreeModule } from 'angular-tree-component';

// UUID



// Angular Tree Grid

import { AngularTreeGridModule } from 'angular-tree-grid';

// NGX Joyride

import { JoyrideModule } from 'ngx-joyride';

// Hamburgers navigation buttons



// NGX Image Cropper

import { ImageCropperModule } from 'ngx-image-cropper';

// Dual listbox

import { AngularDualListBoxModule } from 'angular-dual-listbox';

// Input Mask

import { TextMaskModule } from 'angular2-text-mask';

// Angular Leaflet maps

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

// Leaflet Maps

import { tileLayer, latLng } from 'leaflet';

// Layouts

import { LeftSidebarComponent } from './layout-blueprints/left-sidebar/left-sidebar.component';
import { CollapsedSidebarComponent } from './layout-blueprints/collapsed-sidebar/collapsed-sidebar.component';
import { MinimalLayoutComponent } from './layout-blueprints/minimal-layout/minimal-layout.component';
import { PresentationLayoutComponent } from './layout-blueprints/presentation-layout/presentation-layout.component';

// Layout components

import { HeaderComponent } from './layout-components/header/header.component';
import { HeaderDotsComponent } from './layout-components/header-dots/header-dots.component';
import { HeaderDrawerComponent } from './layout-components/header-drawer/header-drawer.component';
import { HeaderUserboxComponent } from './layout-components/header-userbox/header-userbox.component';
import { HeaderSearchComponent } from './layout-components/header-search/header-search.component';
import { HeaderMenuComponent } from './layout-components/header-menu/header-menu.component';
import { SidebarCollapsedComponent } from './layout-components/sidebar-collapsed/sidebar-collapsed.component';
import { SidebarComponent } from './layout-components/sidebar/sidebar.component';
import { SidebarHeaderComponent } from './layout-components/sidebar-header/sidebar-header.component';
import { SidebarUserboxComponent } from './layout-components/sidebar-userbox/sidebar-userbox.component';
import { SidebarMenuComponent } from './layout-components/sidebar-menu/sidebar-menu.component';
import { SidebarFooterComponent } from './layout-components/sidebar-footer/sidebar-footer.component';
import { PageTitleComponent } from './layout-components/page-title/page-title.component';
import { FooterComponent } from './layout-components/footer/footer.component';
import { ThemeConfiguratorComponent } from './layout-components/theme-configurator/theme-configurator.component';
import { PromoSectionComponent } from './layout-components/promo-section/promo-section.component';
import { ExampleWrapperSimpleComponent } from './layout-components/example-wrapper-simple/example-wrapper-simple.component';
import { ExampleWrapperSeamlessComponent } from './layout-components/example-wrapper-seamless/example-wrapper-seamless.component';
import { ExampleWrapperContainerComponent } from './layout-components/example-wrapper-container/example-wrapper-container.component';

// Example components







// Example pages






/*import { UserTypeComponent } from './settings/user-type/user-type.component';
import { SpecialiteComponent } from './settings/specialite/specialite.component';
import { ActeComponent } from './settings/acte/acte.component';
import { PaysComponent } from './settings/pays/pays.component';
import { ComorbiditeComponent } from './settings/comorbidite/comorbidite.component';
import { MutuelleComponent } from './settings/mutuelle/mutuelle.component';
import { FamilleActeComponent } from './settings/famille-acte/famille-acte.component';
import { ExamenComplementaireComponent } from './settings/examen-complementaire/examen-complementaire.component';
import { AntecedentComponent } from './settings/antecedent/antecedent.component';
import { AnnuaireComponent } from './settings/annuaire/annuaire.component';
import { CentreSoinComponent } from './settings/centre-soin/centre-soin.component';
import { MotifConsultationComponent } from './settings/motif-consultation/motif-consultation.component';
import { VilleComponent } from './settings/ville/ville.component';
import { ListRdvComponent } from './list-rdv/list-rdv.component';
import { TypeOrdonnanceComponent } from './settings/type-ordonnance/type-ordonnance.component';
import { TestComponent } from './test/test.component';

*/
import { RdvComponent } from './calendar/rdv/rdv.component';

import { DossierMedicalComponent } from './dossier-medicale/dossier-medical/dossier-medical.component';


import { NotificationComponent } from './notification/notification.component';
import { ListPatientComponent } from './list-patient/list-patient.component';


import { CalendarComponent } from './calendar/calendar.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { Test2Component } from './test2/test2.component';

/*import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';*/



import { DetailDocumentMedicalComponent } from './detail-document-medical/detail-document-medical.component';
import { ModeleConsultationComponent } from './settings/modele-consultation/modele-consultation.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { ModeleConsultComponent } from './settings/modele-consult/modele-consult.component';
import { ArticleComponent } from './settings/article/article.component';
import { AnalyseLaboComponent } from './settings/analyse-labo/analyse-labo.component';
import { ListExamenComplementaireComponent } from './dossier-medicale/list-examen-complementaire/list-examen-complementaire.component';
import { ListAnalyseLaboComponent } from './dossier-medicale/list-analyse-labo/list-analyse-labo.component';
import { ListArticleComponent } from './dossier-medicale/list-article/list-article.component';
import { ConstanteMedicalComponent } from './settings/constante-medical/constante-medical.component';
import { UniteConstanteComponent } from './settings/unite-constante/unite-constante.component';
import { SaisieConstanteMedicalComponent } from './fiche-patient/saisie-constante-medical/saisie-constante-medical.component';
import { ConsultationComponent } from './dossier-medicale/consultation/consultation.component';
import { ElageProAcceuilComponent } from './site-web/elage-pro/elage-pro-acceuil/elage-pro-acceuil.component';
import { ElagePatientAcceuilComponent } from './site-web/elage-patient/elage-patient-acceuil/elage-patient-acceuil.component';
import { PageTitleBreadcrumbComponent } from './page-title-breadcrumb/page-title-breadcrumb.component';
import { DetailDossierPatientComponent } from './fiche-patient/detail-dossier-patient/detail-dossier-patient.component';

import { UserComponent } from './settings/user/user.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MutuelleFaiteComponent } from './fiche-patient/mutuelle-faite/mutuelle-faite.component';
import { FooterElajePateintComponent } from './site-web/elage-patient/footer-elaje-pateint/footer-elaje-pateint.component';
import { NavHeaderElajePatientComponent } from './site-web/elage-patient/nav-header-elaje-patient/nav-header-elaje-patient.component';
import { HeaderAcceuilElajePateintComponent } from './site-web/elage-patient/header-acceuil-elaje-pateint/header-acceuil-elaje-pateint.component';
import { ChiffresClesAcceuilElajeeComponent } from './site-web/elage-patient/chiffres-cles-acceuil-elajee/chiffres-cles-acceuil-elajee.component';
import { TypeConsultationComponent } from './settings/type-consultation/type-consultation.component';
import { ArborescenceComponent } from './arborescence/arborescence.component';
import { PagesLoginComponent } from './authentification/login-pc/pages-login/pages-login.component';
import { PagesRecoverPasswordComponent } from './authentification/pages-recover-password/pages-recover-password.component';
import { PagesRegisterComponent } from './authentification/login-pc/pages-register/pages-register.component';
import { RegisterContentComponent } from './authentification/login-pc/pages-register/register-content/register-content.component';
import { LoginContent2Component } from './authentification/login-pc/pages-register/login-content2/login-content2.component';
import { LoginContent1Component } from './authentification/login-pc/pages-login/login-content1/login-content1.component';
import { PagesAcceuilLoginMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-acceuil-login-mobile.component';
import { PagesRegisterMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-register-mobile/pages-register-mobile.component';
import { FichePatientComponent } from './fiche-patient/fiche-patient.component';
import { ReglementConsultationComponent } from './fiche-patient/reglement-consultation/reglement-consultation.component';
import { AgendaConfigComponent } from './settings/agenda-config/agenda-config.component';
import { CalTestComponent } from './test/cal-test/cal-test.component';
import { Test3Component } from './test/test3/test3.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LieuConsultationComponent } from './calendar/lieu-consultation/lieu-consultation.component';
import { SettingPraticienComponent } from './settings/setting-praticien/setting-praticien.component';
import { SettingPatientComponent } from './settings/setting-patient/setting-patient.component';
import { SettingCabinetComponent } from './settings/setting-cabinet/setting-cabinet.component';
import { SettingAgendaComponent } from './settings/setting-agenda/setting-agenda.component';
import { SettingDossierMedicaleComponent } from './settings/setting-dossier-medicale/setting-dossier-medicale.component';
import { NavPraticienComponent } from './nav-praticien/nav-praticien.component';
import { SaisieTypeConsultationComponent } from './settings/type-consultation/saisie-type-consultation/saisie-type-consultation.component';
import { SaisieSalleComponent } from './settings/salle/saisie-salle/saisie-salle.component';
import { SaisieCentreSoinComponent } from './settings/centre-soin/saisie-centre-soin/saisie-centre-soin.component';
import { SaisieAntecedantComponent } from './settings/antecedent/saisie-antecedant/saisie-antecedant.component';
import { SaisieMutuelleComponent } from './settings/mutuelle/saisie-mutuelle/saisie-mutuelle.component';
import { SaisieFamilleActeComponent } from './settings/famille-acte/saisie-famille-acte/saisie-famille-acte.component';
import { SaisieSpecialiteComponent } from './settings/specialite/saisie-specialite/saisie-specialite.component';
import { MutuelleModule } from './settings/mutuelle/mutuelle.module';
import { SaisieMotifConsultationComponent } from './settings/motif-consultation/saisie-motif-consultation/saisie-motif-consultation.component';
import { FamilleActeModule } from './settings/famille-acte/famille-acte.module';
import { SaisieAnnuaireComponent } from './settings/annuaire/saisie-annuaire/saisie-annuaire.component';
import { SaisieComorbiditeComponent } from './settings/comorbidite/saisie-comorbidite/saisie-comorbidite.component';
import { SaisieConstanteComponent } from './settings/constante-medical/saisie-constante/saisie-constante.component';
import { SaisieExamenComplementaieComponent } from './settings/examen-complementaire/saisie-examen-complementaie/saisie-examen-complementaie.component';
import { AntecedentModule } from './settings/antecedent/antecedent.module';
import { SaisieUserTypeComponent } from './settings/user-type/saisie-user-type/saisie-user-type.component';
import { ReglementsComponent } from './reglements/reglements.component';
import { MesInterventionsComponent } from './mes-interventions/mes-interventions.component';
import { StatistiqueComponent } from './statistique/statistique.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BreadcrumbDossierMedicaleComponent } from './breadcrumb-dossier-medicale/breadcrumb-dossier-medicale.component';
import { HolidayComponent } from './settings/holiday/holiday.component';
import { SaisieHolidayComponent } from './settings/holiday/saisie-holiday/saisie-holiday.component';
import { LaboratoireComponent } from './pages/settings/container/laboratoire/laboratoire.component';
import { ListLaboratoireComponent } from './pages/settings/components/laboratoire/list-laboratoire/list-laboratoire.component';
import { AddLaboratoireComponent } from './pages/settings/components/laboratoire/add-laboratoire/add-laboratoire.component';
import { EditLaboratoireComponent } from './pages/settings/components/laboratoire/edit-laboratoire/edit-laboratoire.component';
import { DciComponent } from './pages/settings/container/dci/dci.component';
import { ListDciComponent } from './pages/settings/components/dci/list-dci/list-dci.component';
import { AddDciComponent } from './pages/settings/components/dci/add-dci/add-dci.component';
import { EditDciComponent } from './pages/settings/components/dci/edit-dci/edit-dci.component';
import { ClasseMedicamentComponent } from './pages/settings/container/classe-medicament/classe-medicament.component';
import { ListClasseMedicamentComponent } from './pages/settings/components/classe-medicament/list-classe-medicament/list-classe-medicament.component';
import { AddClasseMedicamentComponent } from './pages/settings/components/classe-medicament/add-classe-medicament/add-classe-medicament.component';
import { EditClasseMedicamentComponent } from './pages/settings/components/classe-medicament/edit-classe-medicament/edit-classe-medicament.component';
import { FilterAllPipe } from './core/pipes/filter-all.pipe';
import { MedicamentComponent } from './pages/settings/container/medicament/medicament.component';
import { ListMedicamentComponent } from './pages/settings/components/medicament/list-medicament/list-medicament.component';
import { AddMedicamentComponent } from './pages/settings/components/medicament/add-medicament/add-medicament.component';
import { EditMedicamentComponent } from './pages/settings/components/medicament/edit-medicament/edit-medicament.component';






@NgModule({
  declarations: [
    AppComponent,

    // Layout components
    LoginContent1Component,
    PagesRecoverPasswordComponent,
    PagesRegisterComponent,
    PagesLoginComponent,
    HeaderComponent,
    HeaderDotsComponent,
    HeaderDrawerComponent,
    HeaderUserboxComponent,
    HeaderSearchComponent,
    HeaderMenuComponent,
    SidebarCollapsedComponent,
    SidebarComponent,
    SidebarHeaderComponent,
    SidebarUserboxComponent,
    SidebarMenuComponent,
    SidebarFooterComponent,
    PageTitleComponent,
    FooterComponent,
    ThemeConfiguratorComponent,
    PromoSectionComponent,
    ExampleWrapperSimpleComponent,
    ExampleWrapperSeamlessComponent,
    ExampleWrapperContainerComponent,

    // Layouts

    LeftSidebarComponent,
    CollapsedSidebarComponent,
    MinimalLayoutComponent,
    PresentationLayoutComponent,

    // Example components






    // Example pages




    /*UserTypeComponent,
    SpecialiteComponent,
    ActeComponent,
    PaysComponent,
    ComorbiditeComponent,
    MutuelleComponent,
    FamilleActeComponent,
    ExamenComplementaireComponent,
    AnnuaireComponent,
    AntecedentComponent,
    CentreSoinComponent,
    MotifConsultationComponent,
    VilleComponent,
    ListRdvComponent,
    TypeOrdonnanceComponent,
    */
    RdvComponent,
    FichePatientComponent,
    DossierMedicalComponent,
    NotificationComponent,
    ListPatientComponent,
    RegisterContentComponent,
    CalendarComponent,
    Test2Component,
    PagesAcceuilLoginMobileComponent,
    PagesLoginMobileComponent,
    LoginContent2Component,
    PagesRegisterMobileComponent,
    DetailDocumentMedicalComponent,
    ModeleConsultationComponent,
    ModeleConsultComponent,
    ArticleComponent,
    AnalyseLaboComponent,
    ListExamenComplementaireComponent,
    ListAnalyseLaboComponent,
    ListArticleComponent,
    ConstanteMedicalComponent,
    UniteConstanteComponent,
    SaisieConstanteMedicalComponent,
    ConsultationComponent,
    ReglementConsultationComponent,
    ElageProAcceuilComponent,
    ElagePatientAcceuilComponent,
    PageTitleBreadcrumbComponent,
    DetailDossierPatientComponent,
    UserComponent,
    MutuelleFaiteComponent,
    FooterElajePateintComponent,
    NavHeaderElajePatientComponent,
    HeaderAcceuilElajePateintComponent,
    ChiffresClesAcceuilElajeeComponent,
    TypeConsultationComponent,
    ArborescenceComponent,
    AgendaConfigComponent,
    CalTestComponent,
    Test3Component,
    LieuConsultationComponent,
    SettingPraticienComponent,
    SettingPatientComponent,
    SettingCabinetComponent,
    SettingAgendaComponent,
    SettingDossierMedicaleComponent,
    NavPraticienComponent,
    SaisieTypeConsultationComponent,
    SaisieSalleComponent,
    SaisieCentreSoinComponent,
    SaisieAntecedantComponent,
    SaisieMutuelleComponent,
    SaisieFamilleActeComponent,
    SaisieSpecialiteComponent,
    SaisieMotifConsultationComponent,
    SaisieAnnuaireComponent,
    SaisieComorbiditeComponent,
    SaisieConstanteComponent,
    SaisieExamenComplementaieComponent,
    SaisieUserTypeComponent,
    ReglementsComponent,
    MesInterventionsComponent,
    StatistiqueComponent,
    PageNotFoundComponent,
    BreadcrumbDossierMedicaleComponent,
    HolidayComponent,
    SaisieHolidayComponent,
    LaboratoireComponent,
    ListLaboratoireComponent,
    AddLaboratoireComponent,
    EditLaboratoireComponent,
    DciComponent,
    ListDciComponent,
    AddDciComponent,
    EditDciComponent,
    ClasseMedicamentComponent,
    ListClasseMedicamentComponent,
    AddClasseMedicamentComponent,
    EditClasseMedicamentComponent,
    FilterAllPipe,
    MedicamentComponent,
    ListMedicamentComponent,
    AddMedicamentComponent,
    EditMedicamentComponent,

  ],
  imports: [
    UserTypeModule,
    SalleModule,
    CentreSoinModule,
    SpecialiteModule,
    MutuelleModule,
    FamilleActeModule,
    MotifConsultationModule,
    ComorbiditeModule,
    AntecedentModule,
    AnnuaireModule,
    CKEditorModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    TimepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    CarouselModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    NgApexchartsModule,
    PerfectScrollbarModule,
    UiSwitchModule,
    DropzoneModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    ArchwizardModule,
    AngularEditorModule,
    NgCircleProgressModule.forRoot(),
    GaugeModule.forRoot(),
    NgxGaugeModule,
    LaddaModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    SlickCarouselModule,
    ChartsModule,
    ContextMenuModule.forRoot(),
    CountUpModule,
    DragulaModule.forRoot(),
    Ng2TableModule,
    Ng2SmartTableModule,
    Ng2CompleterModule,
    ColorPickerModule,
    ClipboardModule,
    NgSelectModule,
    AutosizeModule,
    NouisliderModule,
    NgxFlagIconCssModule,
    FeatherModule.pick(icons),
    FontAwesomeModule,
    NgSpinKitModule,
    NgxSkeletonLoaderModule,
    NgProgressModule,
    NgProgressRouterModule,
    NgxSpinnerModule,
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyCoVRykl39EigHTQ0wnI0ISVGR3zpV4dDM' }),
    SweetAlert2Module.forRoot(),
    NotifierModule.withConfig(customNotifierOptions),
    NgxPaginationModule,
    BarRatingModule,
    InfiniteScrollModule,
    TrendModule,
    TreeModule.forRoot(),
    AngularTreeGridModule,
    JoyrideModule.forRoot(),
    ImageCropperModule,
    AngularDualListBoxModule,
    TextMaskModule,
    DataTablesModule,
    LeafletModule.forRoot(),
    FullCalendarModule,
    AngularFontAwesomeModule,
    Ng2SearchPipeModule,

  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
    ThemeOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faFacebook, faPrint, faAlignCenter, faMapMarkerAlt, faTachometerAlt, faExternalLinkAlt, faShareSquare, faSitemap, faInfoCircle, faLifeRing, faTwitter, faQuoteRight, faStarHalfAlt, faSync, faShapes, faCarBattery, faTable, faCubes, faPager, faAngular, faVuejs, faReact, faHtml5, faCheckCircle, faTimesCircle, faBomb, faNetworkWired, faBusAlt, faBirthdayCake, faEyeDropper, faThumbsUp, faCameraRetro, faUnlockAlt, faDownload, faUpload, faReply, faGoogle, faFileImage, faFolderOpen, faBars, faTrashAlt, faSave, faPlayCircle, faEllipsisV, faEllipsisH, faSlidersH, faFileArchive, faAward, faCaretRight, faInstagram, faPinterest, faYoutube, faDiscord, faSlack, faDribbble, faGithub, faPlus, faFolder, faTimes, faEnvelope, faAddressCard, faMap, faCalendarAlt, faImages, faFilm, faClock, faSearch, faChevronRight, faChevronUp, faChevronLeft, faChevronDown, faLink, faLightbulb, faGem, faCog, faDotCircle, faArrowsAltH, faComments, faCommentDots, faKeyboard, faObjectGroup, faUser, faUserCircle, faQuestionCircle, faBuilding, faBell, faFileExcel, faFileAudio, faFileVideo, faFileWord, faFilePdf, faFileCode, faFileAlt, faEye, faChartBar, faPlusCircle, faAngleRight, faAngleUp, faAngleLeft, faAngleDown, faArrowUp, faArrowDown, faArrowRight, faArrowLeft, faStar, faSignOutAlt, faLemon);
  }
}
