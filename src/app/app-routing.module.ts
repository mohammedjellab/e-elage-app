import { EditClasseMedicamentComponent } from './pages/settings/components/classe-medicament/edit-classe-medicament/edit-classe-medicament.component';
import { AddClasseMedicamentComponent } from './pages/settings/components/classe-medicament/add-classe-medicament/add-classe-medicament.component';
import { EditDciComponent } from './pages/settings/components/dci/edit-dci/edit-dci.component';
import { StatistiqueComponent } from './statistique/statistique.component';
import { MesInterventionsComponent } from './mes-interventions/mes-interventions.component';
import { ReglementsComponent } from './reglements/reglements.component';
import { SettingPatientComponent } from './settings/setting-patient/setting-patient.component';
import { SettingCabinetComponent } from './settings/setting-cabinet/setting-cabinet.component';
import { LieuConsultationComponent } from './calendar/lieu-consultation/lieu-consultation.component';
import { ArborescenceComponent } from './arborescence/arborescence.component';
import { Test2Component } from './test2/test2.component';

import { ConsultationComponent } from './dossier-medicale/consultation/consultation.component';
import { DetailDossierPatientComponent } from './fiche-patient/detail-dossier-patient/detail-dossier-patient.component';


import { ModeleConsultComponent } from './settings/modele-consult/modele-consult.component';



import { CalendarComponent } from './calendar/calendar.component';


import { ListRdvComponent } from './list-rdv/list-rdv.component';
import { RdvComponent } from './calendar/rdv/rdv.component';
import { DossierMedicalComponent } from './dossier-medicale/dossier-medical/dossier-medical.component';
import { ListPatientComponent } from './list-patient/list-patient.component';
/*
import { FamilleActeComponent } from './settings/famille-acte/famille-acte.component';
import { ActeComponent } from './settings/acte/acte.component';

import { CentreSoinModule } from './settings/centre-soin/centre-soin.module';
import { TestComponent } from './test/test.component';


import { SpecialiteComponent } from './settings/specialite/specialite.component';
import { CentreSoinComponent } from './settings/centre-soin/centre-soin.component';
import { PaysComponent } from './settings/pays/pays.component';
import { AnnuaireComponent } from './settings/annuaire/annuaire.component';
import { ComorbiditeComponent } from './settings/comorbidite/comorbidite.component';
import { SalleComponent } from './settings/salle/salle.component';
import { MutuelleComponent } from './settings/mutuelle/mutuelle.component';
import { VilleComponent } from './settings/ville/ville.component';


import { TypeOrdonnanceComponent } from './settings/type-ordonnance/type-ordonnance.component';
import { MotifConsultationComponent } from './settings/motif-consultation/motif-consultation.component';
import { ExamenComplementaireComponent } from './settings/examen-complementaire/examen-complementaire.component';
import { AntecedentComponent } from './settings/antecedent/antecedent.component';


*/
import { FichePatientComponent } from './fiche-patient/fiche-patient.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Pages





// Layouts

import { LeftSidebarComponent } from './layout-blueprints/left-sidebar/left-sidebar.component';
import { CollapsedSidebarComponent } from './layout-blueprints/collapsed-sidebar/collapsed-sidebar.component';
import { MinimalLayoutComponent } from './layout-blueprints/minimal-layout/minimal-layout.component';
import { PresentationLayoutComponent } from './layout-blueprints/presentation-layout/presentation-layout.component';

import { SpecialiteComponent } from './settings/specialite/specialite.component';
import { CentreSoinComponent } from './settings/centre-soin/centre-soin.component';
import { PaysComponent } from './settings/pays/pays.component';
import { AnnuaireComponent } from './settings/annuaire/annuaire.component';
import { ComorbiditeComponent } from './settings/comorbidite/comorbidite.component';
import { VilleComponent } from './settings/ville/ville.component';
import { SalleComponent } from './settings/salle/salle.component';
import { TypeOrdonnanceComponent } from './settings/type-ordonnance/type-ordonnance.component';
import { MotifConsultationComponent } from './settings/motif-consultation/motif-consultation.component';
import { MutuelleComponent } from './settings/mutuelle/mutuelle.component';
import { ExamenComplementaireComponent } from './settings/examen-complementaire/examen-complementaire.component';
import { AntecedentComponent } from './settings/antecedent/antecedent.component';

import { PageTitleComponent } from './layout-components/page-title/page-title.component';
import { ModeleConsultationComponent } from './settings/modele-consultation/modele-consultation.component';
import { ArticleComponent } from './settings/article/article.component';
import { AnalyseLaboComponent } from './settings/analyse-labo/analyse-labo.component';
import { ConstanteMedicalComponent } from './settings/constante-medical/constante-medical.component';
import { UniteConstanteComponent } from './settings/unite-constante/unite-constante.component';
import { ElagePatientAcceuilComponent } from './site-web/elage-patient/elage-patient-acceuil/elage-patient-acceuil.component';
import { ElageProAcceuilComponent } from './site-web/elage-pro/elage-pro-acceuil/elage-pro-acceuil.component';
import { UserComponent } from './settings/user/user.component';
import { MutuelleFaiteComponent } from './fiche-patient/mutuelle-faite/mutuelle-faite.component';
import { TypeConsultationComponent } from './settings/type-consultation/type-consultation.component';
import { PagesRecoverPasswordComponent } from './authentification/pages-recover-password/pages-recover-password.component';
import { PagesLoginComponent } from './authentification/login-pc/pages-login/pages-login.component';
import {  PagesLoginMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-login-mobile/pages-login-mobile.component';
import {  PagesRegisterMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-register-mobile/pages-register-mobile.component';
import { PagesRegisterComponent } from './authentification/login-pc/pages-register/pages-register.component';
import { RegisterContentComponent } from './authentification/login-pc/pages-register/register-content/register-content.component';
import { LoginContent2Component } from './authentification/login-pc/pages-register/login-content2/login-content2.component';
import { PagesAcceuilLoginMobileComponent } from './authentification/login-mobile/pages-acceuil-login-mobile/pages-acceuil-login-mobile.component';
import { SaisieConstanteMedicalComponent } from './fiche-patient/saisie-constante-medical/saisie-constante-medical.component';
import { ReglementConsultationComponent } from './fiche-patient/reglement-consultation/reglement-consultation.component';
import { AgendaConfigComponent } from './settings/agenda-config/agenda-config.component';
import { CalTestComponent } from './test/cal-test/cal-test.component';
import { Test3Component } from './test/test3/test3.component';
import { SettingPraticienComponent } from './settings/setting-praticien/setting-praticien.component';
import { SettingAgendaComponent } from './settings/setting-agenda/setting-agenda.component';
import { SettingDossierMedicaleComponent } from './settings/setting-dossier-medicale/setting-dossier-medicale.component';
import { SaisieTypeConsultationComponent } from './settings/type-consultation/saisie-type-consultation/saisie-type-consultation.component';
import { SaisieSalleComponent } from './settings/salle/saisie-salle/saisie-salle.component';
import { SaisieCentreSoinComponent } from './settings/centre-soin/saisie-centre-soin/saisie-centre-soin.component';
import { SaisieSpecialiteComponent } from './settings/specialite/saisie-specialite/saisie-specialite.component';
import { SaisieMutuelleComponent } from './settings/mutuelle/saisie-mutuelle/saisie-mutuelle.component';
import { SaisieMotifConsultationComponent } from './settings/motif-consultation/saisie-motif-consultation/saisie-motif-consultation.component';
import { SaisieFamilleActeComponent } from './settings/famille-acte/saisie-famille-acte/saisie-famille-acte.component';
import { SaisieAnnuaireComponent } from './settings/annuaire/saisie-annuaire/saisie-annuaire.component';
import { SaisieComorbiditeComponent } from './settings/comorbidite/saisie-comorbidite/saisie-comorbidite.component';
import { SaisieConstanteComponent } from './settings/constante-medical/saisie-constante/saisie-constante.component';
import { SaisieExamenComplementaieComponent } from './settings/examen-complementaire/saisie-examen-complementaie/saisie-examen-complementaie.component';
import { SaisieAntecedantComponent } from './settings/antecedent/saisie-antecedant/saisie-antecedant.component';
import { SaisieUserTypeComponent } from './settings/user-type/saisie-user-type/saisie-user-type.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SaisieHolidayComponent } from './settings/holiday/saisie-holiday/saisie-holiday.component';
import { HolidayComponent } from './settings/holiday/holiday.component';
import { AddLaboratoireComponent } from './pages/settings/components/laboratoire/add-laboratoire/add-laboratoire.component';
import { EditLaboratoireComponent } from './pages/settings/components/laboratoire/edit-laboratoire/edit-laboratoire.component';
import { AddDciComponent } from './pages/settings/components/dci/add-dci/add-dci.component';
import { AddMedicamentComponent } from './pages/settings/components/medicament/add-medicament/add-medicament.component';
import { EditMedicamentComponent } from './pages/settings/components/medicament/edit-medicament/edit-medicament.component';



const routes: Routes = [
  { path: '', component: PagesRegisterComponent },
 {
  path: '',
  component: MinimalLayoutComponent,
    children: [

      //siteWeb
      { path: 'elage-patient-acceuil', component: ElagePatientAcceuilComponent },
      { path: 'elage-pro-acceuil', component: ElageProAcceuilComponent },
      //authentification web
      { path: 'mot-de-passe-oublie', component: PagesRecoverPasswordComponent },
      { path: 'pages-register', component: PagesRegisterComponent },
      { path: 'pages-login', component: PagesLoginComponent },
      //authentification mobile
      { path: 'login-mobile', component: PagesAcceuilLoginMobileComponent },
      { path: 'login-mobile-login', component: PagesLoginMobileComponent },
      { path: 'login-mobile-create-account', component: PagesRegisterMobileComponent },

      //authentification content
      { path: 'login', component: LoginContent2Component },
      { path: 'create-account', component: RegisterContentComponent },

    ]
    },
     
    {
      path: '',
      component: LeftSidebarComponent,
      children: [
  
        ////personalisation elage
        { path: 'user-type', loadChildren: () => import('./settings/user-type/user-type.module').then(mod => mod.UserTypeModule) },
       
        { path: 'acte', loadChildren: () => import('./settings/acte/acte.module').then(mod => mod.ActeModule) },
        { path: 'type-consultation', component: TypeConsultationComponent },
        { path: 'reglements', component: ReglementsComponent },
        { path: 'setting-praticien', component: SettingPraticienComponent },
        { path: 'setting-cabinet', component: SettingCabinetComponent },
        { path: 'setting-patient', component: SettingPatientComponent },
        { path: 'setting-agenda', component: SettingAgendaComponent },
        { path: 'setting-dossier-medicale', component: SettingDossierMedicaleComponent },
        { path: 'agenda-config', component: AgendaConfigComponent },
        { path: 'fiche-patient', component: FichePatientComponent },
        { path: 'title', component: PageTitleComponent },
        { path: 'dossier-medical', component: DossierMedicalComponent },
        { path: 'calendar', component: CalendarComponent },
        { path: 'modele-consultation', component: ModeleConsultationComponent },
        { path: 'modele-consult', component: ModeleConsultComponent },
        { path: 'rdv-paiement', component: ReglementConsultationComponent },
        { path: 'saisie-type-consultation', component: SaisieTypeConsultationComponent },
        { path: 'article', component: ArticleComponent },
        { path: 'analyse-labo', component: AnalyseLaboComponent },
        { path: 'constante-medical', component:  ConstanteMedicalComponent},
        { path: 'unite-constante', component:  UniteConstanteComponent},
        { path: 'test2', component:  Test2Component},
        { path: 'arborescence', component:  ArborescenceComponent},
        { path: 'saisie-constante-medical', component:  SaisieConstanteMedicalComponent},
        { path: 'detail-dossier-patient', component:  DetailDossierPatientComponent},
        { path: 'saisie-specialite', component:  SaisieSpecialiteComponent},
        { path: 'saisie-mutuelle', component:  SaisieMutuelleComponent},
        { path: 'saisie-motif-consultation', component:  SaisieMotifConsultationComponent},
        { path: 'saisie-famille-acte', component:  SaisieFamilleActeComponent},
        { path: 'saisie-annuaire', component:  SaisieAnnuaireComponent},
        { path: 'saisie-comorbidite', component:  SaisieComorbiditeComponent},
        { path: 'saisie-constante', component:  SaisieConstanteComponent},
        { path: 'saisie-antecedant', component:  SaisieAntecedantComponent},
        { path: 'saisie-examen-complementaire', component:  SaisieExamenComplementaieComponent},
        { path: 'saisie-user-type', component:  SaisieUserTypeComponent},
        { path: 'saisie-annuaire', component:  SaisieAnnuaireComponent},
        { path: 'saisie-holiday', component:  SaisieHolidayComponent},
        { path: 'holiday', component:  HolidayComponent},
        { path: 'consultation', component:  ConsultationComponent},
        { path: 'lieu-consultation', component:  LieuConsultationComponent},
        //{ path: 'dossier-medical', loadChildren: () => import('./dossier-medical/dossier-medical.module').then(mod => mod.DossierMedicalModule) },
        //{ path: 'dossier-patient', loadChildren: () => import('./dossier-patient/dossier-patient.module').then(mod => mod.DossierPatientModule) },
        { path: 'ville', loadChildren: () => import('./settings/ville/ville.module').then(mod => mod.VilleModule) },
        { path: 'centre-soins', loadChildren: () => import('./settings/centre-soin/centre-soin.module').then(mod => mod.CentreSoinModule) },
        { path: 'famille-acte', loadChildren: () => import('./settings/famille-acte/famille-acte.module').then(mod => mod.FamilleActeModule) },
        { path: 'pays', loadChildren: () => import('./settings/pays/pays.module').then(mod => mod.PaysModule) },
        { path: 'comorbidite', loadChildren: () => import('./settings/comorbidite/comorbidite.module').then(mod => mod.ComorbiditeModule) },
        { path: 'mutuelle', loadChildren: () => import('./settings/mutuelle/mutuelle.module').then(mod => mod.MutuelleModule) },
        
        { path: 'type-ordonnance', loadChildren: () => import('./settings/type-ordonnance/type-ordonnance.module').then(mod => mod.TypeOrdonnanceModule) },
        { path: 'salle', loadChildren: () => import('./settings/salle/salle.module').then(mod => mod.SalleModule) },
        { path: 'list-patient', component: ListPatientComponent },
        { path: 'mes-interventions', component: MesInterventionsComponent },
        { path: 'dossier-medicale', component: DossierMedicalComponent },
        { path: 'utilisateur', component: UserComponent },
        { path: 'rdv', component: RdvComponent },
        { path: 'statistique', component: StatistiqueComponent },
        { path: 'test2', component: Test2Component },
        { path: 'test3', component: CalTestComponent },
        { path: 'test4', component: Test3Component },
        { path: 'saisie-salle', component: SaisieSalleComponent },
        { path: 'saisie-centre-soin', component: SaisieCentreSoinComponent },
        { path: 'mutuelle-faite', component: MutuelleFaiteComponent },
        { path: 'list-rdv', loadChildren: () => import('./list-rdv/list-rdv.module').then(mod => mod.ListRdvModule) },
        { path: 'examen-complementaire', loadChildren: () => import('./settings/examen-complementaire/examen-complementaire.module').then(mod => mod.ExamenComplementaireModule) },
        { path: 'antecedent', loadChildren: () => import('./settings/antecedent/antecedent.module').then(mod => mod.AntecedentModule) },
        { path: 'motif-consultation', loadChildren: () => import('./settings/motif-consultation/motif-consultation.module').then(mod => mod.MotifConsultationModule) },
        { path: 'annuaire', loadChildren: () => import('./settings/annuaire/annuaire.module').then(mod => mod.AnnuaireModule) },
        { path: 'laboratoire/add', component:  AddLaboratoireComponent},
        { path: 'laboratoire/edit/{id}', component:  EditLaboratoireComponent},
        { path: 'dci/add', component:  AddDciComponent},
        { path: 'dci/edit/{id}', component:  EditDciComponent},
        { path: 'classemedicament/add', component:  AddClasseMedicamentComponent},
        { path: 'classemedicament/edit/{id}', component:  EditClasseMedicamentComponent},
        { path: 'medicament/add', component:  AddMedicamentComponent},
        { path: 'medicament/edit/{id}', component:  EditMedicamentComponent},
  
  
        { path: '**', component: PageNotFoundComponent }
  
      ]
      },
      //{ path: '**', component: PageNotFoundComponent }
      
  //{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
