import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavPraticienComponent } from './nav-praticien.component';

describe('NavPraticienComponent', () => {
  let component: NavPraticienComponent;
  let fixture: ComponentFixture<NavPraticienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavPraticienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavPraticienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
