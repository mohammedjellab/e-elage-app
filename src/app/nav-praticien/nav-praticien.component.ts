import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-nav-praticien',
  templateUrl: './nav-praticien.component.html',
  styleUrls: ['./nav-praticien.component.scss']
})
export class NavPraticienComponent implements OnInit {

   @Output() event = new EventEmitter();
  @Input() category;
  pages = [
    //praticien
    {
      "title": "Types d'utilisateur",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/user-type",
      "category": "Praticien"
    },
    {
      "title": "Spécialité",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/specialite",
      "category": "Praticien"
    },
    {
      "title": "Annuaire",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/annuaire",
      "category": "Praticien"
    },
    //Agenda
    {
      "title": "Agenda config",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/agenda-config",
      "category": "Agenda"
    },
    {
      "title": "Famille d'acte",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/famille-acte",
      "category": "Agenda"
    },
    {
      "title": "Type Consultation",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/type-consultation",
      "category": "Agenda"
    },
    //Cabinet
    {
      "title": "Salle",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/salle",
      "category": "Cabinet"
    },
    {
      "title": "Etablissment",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/centre-soins",
      "category": "Cabinet"
    },
    {
      "title": "Utilisateur",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/utilisateur",
      "category": "Cabinet"
    },

    //patient

    {
      "title": "Ville",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/ville",
      "category": "Patient"
    },
    {
      "title": "Mutuelle",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/mutuelle",
      "category": "Patient"
    },
    //Dossier médicale
    {
      "title": "Comorbidité",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/comorbidite",
      "category": "Dossier médicale"
    },

    {
      "title": "Motif de Consultation",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/motif-consultation",
      "category": "Dossier médicale"
    },
    {
      "title": "Modèle de consultation",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/modele-consultation",
      "category": "Dossier médicale"
    },
    /*{
      "title": "Unité",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/unite-constante",
      "category": "Dossier médicale"
    },*/
    {
      "title": "Antécédent",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/antecedent",
      "category": "Dossier médicale"
    },
    {
      "title": "Laboratoires",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/laboratoires",
      "category": "Dossier médicale"
    },
    {
      "title": "DCIs",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/dcis",
      "category": "Dossier médicale"
    },
    {
      "title": "Classes Médicales",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/classemedicales",
      "category": "Dossier médicale"
    },
    {
      "title": "Médicaments",
      "type": "simple",
      "icon": "<i class=\"pe-7s-news-paper\"></i>",
      "link": "/medicaments",
      "category": "Dossier médicale"
    }
  ]
  constructor() { }

  ngOnInit() {
    console.log('ccc',this.category);
    this.pages = this.pages.filter(page =>page.category== this.category)
  }

  pageClicked(page){
    this.event.emit(page);
  }

}
