import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleBreadcrumbComponent } from './page-title-breadcrumb.component';

describe('PageTitleBreadcrumbComponent', () => {
  let component: PageTitleBreadcrumbComponent;
  let fixture: ComponentFixture<PageTitleBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
