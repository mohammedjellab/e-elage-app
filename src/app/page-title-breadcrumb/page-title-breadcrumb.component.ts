import { Component, OnInit, Input,EventEmitter, Output } from '@angular/core';
import { ThemeOptions } from '../theme-options';

@Component({
  selector: 'app-page-title-breadcrumb',
  templateUrl: './page-title-breadcrumb.component.html',
  styleUrls: ['./page-title-breadcrumb.component.scss']
})
export class PageTitleBreadcrumbComponent  {
  @Input() titleHeading: string;
  @Input() titleDescription: string;
  //event: EventEmitter<any> = new EventEmitter();

  @Output() event = new EventEmitter();
  constructor(public globals: ThemeOptions) {
  }



  addPatient(){
    this.event.emit({add_patient: true})
  }

}
