import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingDossierMedicaleComponent } from './setting-dossier-medicale.component';

describe('SettingDossierMedicaleComponent', () => {
  let component: SettingDossierMedicaleComponent;
  let fixture: ComponentFixture<SettingDossierMedicaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingDossierMedicaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingDossierMedicaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
