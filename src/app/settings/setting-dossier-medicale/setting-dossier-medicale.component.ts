import { Component, OnInit, OnDestroy } from '@angular/core';
import { LienService } from 'src/app/services/lien.service';

@Component({
  selector: 'app-setting-dossier-medicale',
  templateUrl: './setting-dossier-medicale.component.html',
  styleUrls: ['./setting-dossier-medicale.component.scss']
})
export class SettingDossierMedicaleComponent implements OnInit, OnDestroy {

  category = {
    page: 'Dossier médicale'
  }
  page = {
    "title": null,
    "type": null,
    "icon": null,
    "link": null,
    "category": null
  }
  constructor(private lienService: LienService) { }

  ngOnInit() {
    this.lienService.removePaddingSetting();
  }

  ngOnDestroy(){
    this.lienService.addPaddingSetting();

  }
  onFavoriteChanged(page){
    this.page = page;
  }

}
