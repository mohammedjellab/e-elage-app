import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingPraticienComponent } from './setting-praticien.component';

describe('SettingPraticienComponent', () => {
  let component: SettingPraticienComponent;
  let fixture: ComponentFixture<SettingPraticienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingPraticienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingPraticienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
