import { LienService } from 'src/app/services/lien.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-setting-praticien',
  templateUrl: './setting-praticien.component.html',
  styleUrls: ['./setting-praticien.component.scss']
})
export class SettingPraticienComponent implements OnInit, OnDestroy {

  category = {
    page: 'Praticien'
  }
  page = {
    "title": null,
    "type": null,
    "icon": null,
    "link": null,
    "category": null
  }
  constructor(private lienService: LienService) { }

  ngOnInit() {
    this.lienService.removePaddingSetting();
  }

  ngOnDestroy(){
    this.lienService.addPaddingSetting();

  }
  onFavoriteChanged(page){
    this.page = page;
  }
}
