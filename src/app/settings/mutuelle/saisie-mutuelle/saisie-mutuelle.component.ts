import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Mutuelle } from 'src/app/models/mutuelle';
import { MutuelleService } from './../../../services/mutuelle.service';
@Component({
  selector: 'app-saisie-mutuelle',
  templateUrl: './saisie-mutuelle.component.html',
  styleUrls: ['./saisie-mutuelle.component.scss']
})
export class SaisieMutuelleComponent implements OnInit {

  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  myMutuelle: Mutuelle ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  mutuelles;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() mutuelle;
  user;
  constructor(private mutuelleService: MutuelleService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyMutuelle(this.mutuelle);
    console.log('mutuelles', this.mutuelle);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.mutuelleService.persist(this.myMutuelle).subscribe(data =>{
      this.event.emit({mutuelle: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({mutuelle: null});
  }
  findAll(){
    this.mutuelleService.findAll().subscribe(data =>{
      this.mutuelles = data['hydra:member'];
    }, err =>{

    })
  }
  update(){
    console.log('ssssalle',this.myMutuelle)
    this.mutuelleService.update(this.myMutuelle).subscribe(data =>{
      this.event.emit({mutuelle: data});
    }, err =>{

    });
  }
  remplirmyMutuelle(f){
    this.myMutuelle = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.mutuelle){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyMutuelle(mutuelle){
    console.log('edit', mutuelle)
    if(mutuelle){
      this.myMutuelle = {
        id: mutuelle.id,
        libelle: mutuelle.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myMutuelle = {
        libelle: ''
      }
    }
  }

  
  edit(mutuelle){
    this.editForm =false;
    console.log('edit', mutuelle)
     this.myMutuelle = {
      id: mutuelle.id,
      libelle: mutuelle.libelle
    }
  }

}
