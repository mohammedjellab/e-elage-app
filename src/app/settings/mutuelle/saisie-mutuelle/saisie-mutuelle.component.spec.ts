import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieMutuelleComponent } from './saisie-mutuelle.component';

describe('SaisieMutuelleComponent', () => {
  let component: SaisieMutuelleComponent;
  let fixture: ComponentFixture<SaisieMutuelleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieMutuelleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieMutuelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
