import { MutuelleService } from './../../services/mutuelle.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Mutuelle } from './../../models/mutuelle';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SaisieMutuelleComponent } from './saisie-mutuelle/saisie-mutuelle.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-mutuelle',
  templateUrl: './mutuelle.component.html',
  styleUrls: ['./mutuelle.component.scss']
})
export class MutuelleComponent implements OnInit {

  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  myMutuelle: Mutuelle ={
    libelle: '',
    id: null
  }
  mutuelles;
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private mutuelleService: MutuelleService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.mutuelleService.persist(this.myMutuelle).subscribe(data =>{
      this.mutuelles = [data,...this.mutuelles];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.mutuelleService.findAllMutuelle().subscribe(data =>{
      this.mutuelles = data;
      this.loadedData = true;
      console.log('mutuelles', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssmutuelle',this.myMutuelle)
    this.mutuelleService.update(this.myMutuelle).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(mutuelle){
    let initialState = {
      mutuelle: mutuelle
    }
    this.bsModalRef = this.modalService.show(SaisieMutuelleComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(mutuelle){
    this.editForm =false;
    console.log('edit', mutuelle)
     this.myMutuelle = {
      id: mutuelle.id,
      libelle: mutuelle.libelle
    }
  }*/
  //modifier un acte
  edit(mutuelle){
    //this.loadedData = false;
    this.callModal(mutuelle);
    console.log('acte',mutuelle)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.mutuelle;
      if(list){
          this.mutuelles.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.mutuelles = [res.mutuelle, ...this.mutuelles];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(mutuelle){
    console.log('unite',mutuelle)
    this.mutuelleService.delete(mutuelle.id).subscribe(data =>{
      this.mutuelles =this.mutuelles.filter(data =>data.id !=mutuelle.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }




}
