import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MutuelleComponent } from './mutuelle.component';

describe('MutuelleComponent', () => {
  let component: MutuelleComponent;
  let fixture: ComponentFixture<MutuelleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutuelleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MutuelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
