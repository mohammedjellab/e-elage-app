import { NgSpinKitModule } from 'ng-spin-kit';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { MutuelleRoutingModule } from './mutuelle-routing-module';
import { MutuelleComponent } from './mutuelle.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    MutuelleRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgSpinKitModule,
    AlertModule.forRoot(),
  ],
  exports: [
    MutuelleComponent
  ],
  declarations: [
    MutuelleComponent
  ],
  providers: [
  ],
})
export class MutuelleModule { }