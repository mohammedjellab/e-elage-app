import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingAgendaComponent } from './setting-agenda.component';

describe('SettingAgendaComponent', () => {
  let component: SettingAgendaComponent;
  let fixture: ComponentFixture<SettingAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
