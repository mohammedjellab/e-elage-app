import { Component, OnInit, OnDestroy } from '@angular/core';
import { LienService } from 'src/app/services/lien.service';

@Component({
  selector: 'app-setting-agenda',
  templateUrl: './setting-agenda.component.html',
  styleUrls: ['./setting-agenda.component.scss']
})
export class SettingAgendaComponent implements OnInit , OnDestroy{

  category = {
    page: 'Agenda'
  }
  page = {
    "title": null,
    "type": null,
    "icon": null,
    "link": null,
    "category": null
  }
  constructor(private lienService: LienService) { }

  ngOnInit() {
    this.lienService.removePaddingSetting();
  }

  ngOnDestroy(){
    this.lienService.addPaddingSetting();

  }
  onFavoriteChanged(page){
    this.page = page;
  }

}
