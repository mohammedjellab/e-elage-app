import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyseLaboComponent } from './analyse-labo.component';

describe('AnalyseLaboComponent', () => {
  let component: AnalyseLaboComponent;
  let fixture: ComponentFixture<AnalyseLaboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyseLaboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyseLaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
