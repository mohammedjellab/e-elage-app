import { NgSpinKitModule } from 'ng-spin-kit';
import { AnnuaireRoutingModule } from './annuaire-routing-module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AnnuaireComponent } from './annuaire.component';



@NgModule({
  imports: [
    CommonModule,
    AnnuaireRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule

  ],
  exports: [
    AnnuaireComponent
  ],
  declarations: [
    AnnuaireComponent
  ],
  providers: [
  ],
})
export class AnnuaireModule { }