import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieAnnuaireComponent } from './saisie-annuaire.component';

describe('SaisieAnnuaireComponent', () => {
  let component: SaisieAnnuaireComponent;
  let fixture: ComponentFixture<SaisieAnnuaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieAnnuaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieAnnuaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
