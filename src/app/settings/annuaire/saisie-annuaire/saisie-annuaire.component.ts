import { AnnuaireService } from './../../../services/annuaire.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Annuaire } from 'src/app/models/annuaire';

@Component({
  selector: 'app-saisie-annuaire',
  templateUrl: './saisie-annuaire.component.html',
  styleUrls: ['./saisie-annuaire.component.scss']
})
export class SaisieAnnuaireComponent implements OnInit {

  
  myAnnuaire: Annuaire ={
    nom: '',
    prenom: '',
    tel: '',
    gsm: '',
    email: '',
    typeRep: '',
    fax: '',
    id:null,
    centreSoin: null
  }
  annuaires;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() annuaire;
  user;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  constructor(private annuaireService: AnnuaireService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyAnnuaire(this.annuaire);
    console.log('annuaires', this.annuaire);


    
  }
  //fermer modal
  closeModal(){
    this.event.emit({annuaire: null});
  }
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.annuaireService.persist(this.myAnnuaire).subscribe(data =>{
      this.event.emit({annuaire: data});
    },err =>{

    });
  }

 
  update(){
    console.log('ssssalle',this.myAnnuaire)
    this.annuaireService.update(this.myAnnuaire).subscribe(data =>{
      this.event.emit({annuaire: data});
    }, err =>{

    });
  }
  remplirMyAnnuaire(f){
    this.myAnnuaire = {
      nom: f.value.nom,
      prenom: f.value.prenom,
      tel: f.value.tel,
      gsm: f.value.gsm,
      fax: f.value.fax,
      typeRep: f.value.typeRep,
      id: f.value.id,
      centreSoin: this.user.centreSoin,
      email: f.value.email,
    }
    if(this.annuaire){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyAnnuaire(annuaire){
    console.log('edit', annuaire)
    if(annuaire){
      this.myAnnuaire = {
        nom: annuaire.nom,
        prenom: annuaire.prenom,
        tel: annuaire.tel,
        gsm: annuaire.gsm,
        fax: annuaire.fax,
        typeRep: annuaire.typeRep,
        id: annuaire.id,
        centreSoin: this.user.centreSoin,
        email: annuaire.email,
        //centreSoin: this.user.centreSoin
      }
    }else{
      //this.myAnnuaire = 
    }
  }

  
  edit(annuaire){
    this.editForm =false;
    console.log('edit', annuaire)
     this.myAnnuaire = {
        nom: annuaire.nom,
        prenom: annuaire.prenom,
        tel: annuaire.tel,
        gsm: annuaire.gsm,
        fax: annuaire.fax,
        typeRep: annuaire.typeRep,
        id: annuaire.id,
        centreSoin: this.user.centreSoin,
        email: annuaire.email,
    }
  }


}
