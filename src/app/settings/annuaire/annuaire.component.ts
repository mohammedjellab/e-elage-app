import { Annuaire } from './../../models/annuaire';
import { AnnuaireService } from './../../services/annuaire.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieAnnuaireComponent } from './saisie-annuaire/saisie-annuaire.component';

@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.component.html',
  styleUrls: ['./annuaire.component.scss']
})
export class AnnuaireComponent implements OnInit {

  myAnnuaire: Annuaire ={
    nom: '',
    prenom: '',
    tel: '',
    gsm: '',
    email: '',
    typeRep: '',
    fax: '',
    id:null,
    centreSoin: null
  }
  annuaires;
  

  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt


  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private annuaireService: AnnuaireService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
 
  persist(){
    this.annuaireService.persist(this.myAnnuaire).subscribe(data =>{
      this.annuaires = [data,...this.annuaires];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.annuaireService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.annuaires = data;
      this.loadedData = true;
      console.log('annuaires', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssannuaire',this.myAnnuaire)
    this.annuaireService.update(this.myAnnuaire).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(annuaire){
    let initialState = {
      annuaire: annuaire
    }
    this.bsModalRef = this.modalService.show(SaisieAnnuaireComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(annuaire){
    this.editForm =false;
    console.log('edit', annuaire)
     this.myAnnuaire = {
      id: annuaire.id,
      libelle: annuaire.libelle
    }
  }*/
  //modifier un acte
  edit(annuaire){
    //this.loadedData = false;
    this.callModal(annuaire);
    console.log('acte',annuaire)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.annuaire;
      if(list){
          this.annuaires.filter(data => {
            if(list.id == data.id){
              data.nom = list.nom
              data.prenom = list.prenom
              data.email = list.email
              data.tel = list.tel
              data.typeRep = list.typeRep
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.annuaires = [res.annuaire, ...this.annuaires];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(annuaire){
    console.log('unite',annuaire)
    this.annuaireService.delete(annuaire.id).subscribe(data =>{
      this.annuaires =this.annuaires.filter(data =>data.id !=annuaire.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }



}
