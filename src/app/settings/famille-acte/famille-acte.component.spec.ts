import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilleActeComponent } from './famille-acte.component';

describe('FamilleActeComponent', () => {
  let component: FamilleActeComponent;
  let fixture: ComponentFixture<FamilleActeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilleActeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilleActeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
