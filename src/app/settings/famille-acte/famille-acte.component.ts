import { FamilleActeService } from './../../services/famille-acte.service';
import { FamilleActe } from './../../models/familleActe';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieFamilleActeComponent } from './saisie-famille-acte/saisie-famille-acte.component';

@Component({
  selector: 'app-famille-acte',
  templateUrl: './famille-acte.component.html',
  styleUrls: ['./famille-acte.component.scss']
})
export class FamilleActeComponent implements OnInit {



  familles: FamilleActe[] =[];
  
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  myFamille: FamilleActe ={
    libelle: '',
    id: null,
    centreSoin: ''
  }
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private familleService: FamilleActeService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.familleService.persist(this.myFamille).subscribe(data =>{
      this.familles = [data,...this.familles];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.familleService.findAllFamilleActe().subscribe(data =>{
      this.familles = data;
      this.loadedData = true;
      console.log('familles', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssfamille',this.myFamille)
    this.familleService.update(this.myFamille).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(famille){
    let initialState = {
      famille: famille
    }
    this.bsModalRef = this.modalService.show(SaisieFamilleActeComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(famille){
    this.editForm =false;
    console.log('edit', famille)
     this.myFamille = {
      id: famille.id,
      libelle: famille.libelle
    }
  }*/
  //modifier un acte
  edit(famille){
    //this.loadedData = false;
    this.callModal(famille);
    console.log('acte',famille)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.famille;
      if(list){
          this.familles.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.familles = [res.famille, ...this.familles];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(famille){
    console.log('unite',famille)
    this.familleService.delete(famille.id).subscribe(data =>{
      this.familles =this.familles.filter(data =>data.id !=famille.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }


}
