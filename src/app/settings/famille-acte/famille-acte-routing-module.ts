import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FamilleActeComponent } from './famille-acte.component';


const routes: Routes = [
  { path: 'famille-acte', component: FamilleActeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilleActeRoutingModule { }