import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FamilleActeRoutingModule } from './famille-acte-routing-module';
import { FamilleActeComponent } from './famille-acte.component';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSpinKitModule } from 'ng-spin-kit';



@NgModule({
  imports: [
    CommonModule,
    FamilleActeRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule

  ],
  exports: [
    FamilleActeComponent
  ],
  declarations: [
    FamilleActeComponent
  ],
  providers: [
  ],
})
export class FamilleActeModule { }