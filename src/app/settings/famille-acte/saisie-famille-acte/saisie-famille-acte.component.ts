import { FamilleActeService } from './../../../services/famille-acte.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FamilleActe } from 'src/app/models/familleActe';

@Component({
  selector: 'app-saisie-famille-acte',
  templateUrl: './saisie-famille-acte.component.html',
  styleUrls: ['./saisie-famille-acte.component.scss']
})
export class SaisieFamilleActeComponent implements OnInit {

  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  myFamille: FamilleActe ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  familles;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() famille;
  user;
  constructor(private familleService: FamilleActeService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyFamille(this.famille);
    console.log('familles', this.famille);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.familleService.persist(this.myFamille).subscribe(data =>{
      this.event.emit({famille: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({famille: null});
  }

  update(){
    console.log('ssssalle',this.myFamille)
    this.familleService.update(this.myFamille).subscribe(data =>{
      this.event.emit({famille: data});
    }, err =>{

    });
  }
  remplirMyFamille(f){
    this.myFamille = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.famille){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyFamille(famille){
    console.log('edit', famille)
    if(famille){
      this.myFamille = {
        id: famille.id,
        libelle: famille.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myFamille = {
        libelle: ''
      }
    }
  }

  
  edit(famille){
    this.editForm =false;
    console.log('edit', famille)
     this.myFamille = {
      id: famille.id,
      libelle: famille.libelle
    }
  }

}
