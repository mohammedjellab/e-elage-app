import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieFamilleActeComponent } from './saisie-famille-acte.component';

describe('SaisieFamilleActeComponent', () => {
  let component: SaisieFamilleActeComponent;
  let fixture: ComponentFixture<SaisieFamilleActeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieFamilleActeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieFamilleActeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
