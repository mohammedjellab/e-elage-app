import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { LienService } from 'src/app/services/lien.service';

@Component({
  selector: 'app-setting-cabinet',
  templateUrl: './setting-cabinet.component.html',
  styleUrls: ['./setting-cabinet.component.scss']
})
export class SettingCabinetComponent implements OnInit,OnDestroy {

  category = {
    page: 'Cabinet'
  }
  page = {
      "title": null,
      "type": null,
      "icon": null,
      "link": null,
      "category": null
  }
  constructor(private lienService: LienService) { }

  ngOnInit() {
    this.lienService.removePaddingSetting();
  }

  ngOnDestroy(){
    this.lienService.addPaddingSetting();
  }

  onFavoriteChanged(page){
    this.page = page;
  }

}
