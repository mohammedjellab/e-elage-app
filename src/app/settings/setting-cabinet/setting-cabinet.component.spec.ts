import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingCabinetComponent } from './setting-cabinet.component';

describe('SettingCabinetComponent', () => {
  let component: SettingCabinetComponent;
  let fixture: ComponentFixture<SettingCabinetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingCabinetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingCabinetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
