import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieSpecialiteComponent } from './saisie-specialite.component';

describe('SaisieSpecialiteComponent', () => {
  let component: SaisieSpecialiteComponent;
  let fixture: ComponentFixture<SaisieSpecialiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieSpecialiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieSpecialiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
