import { SpecialiteService } from './../../../services/specialite.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Specialite } from 'src/app/models/lspecialite';


@Component({
  selector: 'app-saisie-specialite',
  templateUrl: './saisie-specialite.component.html',
  styleUrls: ['./saisie-specialite.component.scss']
})
export class SaisieSpecialiteComponent implements OnInit {
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  mySpecialite: Specialite ={
    libelle: '',
    id: null,
    centreSoin: null
  }
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() specialite;
  user;
  constructor(private specialiteService: SpecialiteService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmySpecialite(this.specialite);
    console.log('specialite', this.specialite);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.specialiteService.persist(this.mySpecialite).subscribe(data =>{
      this.event.emit({specialite: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({specialite: null});
  }
  
  update(){
    console.log('specialite',this.mySpecialite)
    this.specialiteService.update(this.mySpecialite).subscribe(data =>{
      this.event.emit({specialite: data});
    }, err =>{

    });
  }
  remplirMySpecialite(f){
    this.mySpecialite = {
      libelle: f.value.libelle,
      id: f.value.id,
      centreSoin: this.user.centreSoin
    }
    if(this.specialite){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmySpecialite(specialite){
    console.log('edit', specialite)
    if(specialite){
      this.mySpecialite = {
        id: specialite.id,
        libelle: specialite.libelle,
        centreSoin: this.user.centreSoin
      }
    }else{
      this.mySpecialite = {
        libelle: ''
      }
    }
  }

 
  edit(specialite){
    //this.editForm =false;
    console.log('edit', specialite)
     this.mySpecialite = {
      id: specialite.id,
      libelle: specialite.libelle
    }
  }
}
