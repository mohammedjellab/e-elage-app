import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialiteComponent } from './specialite.component';


const routes: Routes = [
  { path: 'specialite', component: SpecialiteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialiteRoutingModule { }