import { NgSpinKitModule } from 'ng-spin-kit';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UserTypeModule } from './../user-type/user-type.module';
import { SpecialiteRoutingModule } from './specialite-routing-module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SpecialiteComponent } from './specialite.component';



@NgModule({
  imports: [
    CommonModule,
    SpecialiteRoutingModule,
    UserTypeModule,
    FormsModule,
    FontAwesomeModule,
    NgSpinKitModule
  ],
  exports: [
    SpecialiteComponent
  ],
  declarations: [
    SpecialiteComponent
  ],
  providers: [
  ],
})
export class SpecialiteModule { }