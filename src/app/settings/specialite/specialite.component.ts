import { Specialite } from './../../models/lspecialite';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieSpecialiteComponent } from './saisie-specialite/saisie-specialite.component';
import { SpecialiteService } from './../../services/specialite.service';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-specialite',
  templateUrl: './specialite.component.html',
  styleUrls: ['./specialite.component.scss']
})
export class SpecialiteComponent implements OnInit {
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  mySpecialite: Specialite ={
    libelle: '',
    id: null
  }
  specialites;
  specialites_search$: Observable<Object>;
  private searchTerms = new Subject<string>();

  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  searchText;
  constructor(private specialiteService: SpecialiteService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();
  }

  //recherche 
  //rechercher un un rdv par nom, prenom, tel ou cin
  searchSpecialite(event){
    /*let value =event.target.value
    let spec = this.specialites;
    if(value){
      let temp = []
      this.specialites = this.specialites.filter(events  => {
        
        console.log('events title',events)
        if(events.libelle){
          if(events.libelle.includes(value)  ){
            console.log('events title',events)
            temp.push(events);
          }
        }
      });
      this.specialites = temp;

    }else{
      this.specialites = spec;
    }
    console.log('calendarEvents after',spec)*/
  }

  persist(){
    this.specialiteService.persist(this.mySpecialite).subscribe(data =>{
      this.specialites = [data,...this.specialites];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.specialiteService.findAll().subscribe(data =>{
      this.specialites = data;
      this.loadedData = true;
      console.log('salles', data);
    }, err =>{

    })
  }
  update(){
    console.log('ssssalle',this.mySpecialite)
    this.specialiteService.update(this.mySpecialite).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  show(salle){
    /*this.editForm =false;
    this.specialiteService.showPerso(salle.id).subscribe( data =>{
      //this.mySalle = data;
      console.log('salle',data)
    },err =>{

    });*/
  }
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(specialite){
    let initialState = {
      specialite: specialite
    }
    this.bsModalRef = this.modalService.show(SaisieSpecialiteComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(salle){
    this.editForm =false;
    console.log('edit', salle)
     this.mySalle = {
      id: salle.id,
      libelle: salle.libelle
    }
  }*/
  //modifier un acte
  edit(specialite){
    //this.loadedData = false;
    this.callModal(specialite);
    console.log('acte',specialite)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.specialite;
      if(list){
          this.specialites.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.specialites = [res.specialite, ...this.specialites];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(specialite){
    console.log('unite',specialite)
    this.specialiteService.delete(specialite.id).subscribe(data =>{
      this.specialites =this.specialites.filter(data =>data.id !=specialite.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }
    
}
