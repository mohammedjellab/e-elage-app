import { Component, OnInit, OnDestroy } from '@angular/core';
import { LienService } from 'src/app/services/lien.service';

@Component({
  selector: 'app-setting-patient',
  templateUrl: './setting-patient.component.html',
  styleUrls: ['./setting-patient.component.scss']
})
export class SettingPatientComponent implements OnInit, OnDestroy {

  category = {
    page: 'Patient'
  }
  page = {
    "title": null,
    "type": null,
    "icon": null,
    "link": null,
    "category": null
  }
  constructor(private lienService: LienService) { }

  ngOnInit() {
    this.lienService.removePaddingSetting(
      
    );
  }
  ngOnDestroy(){
    this.lienService.addPaddingSetting();

  }
  onFavoriteChanged(page){
    this.page = page;
  }


}
