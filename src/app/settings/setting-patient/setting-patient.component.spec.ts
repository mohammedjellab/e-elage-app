import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingPatientComponent } from './setting-patient.component';

describe('SettingPatientComponent', () => {
  let component: SettingPatientComponent;
  let fixture: ComponentFixture<SettingPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
