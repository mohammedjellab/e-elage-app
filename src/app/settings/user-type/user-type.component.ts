import { UserType } from './../../models/userType';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieUserTypeComponent } from './saisie-user-type/saisie-user-type.component';
import { UserTypeService } from 'src/app/services/user-type.service';

@Component({
  selector: 'app-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.scss']
})
export class UserTypeComponent implements OnInit {
  myUserType: UserType ={
    libelle: '',
    active: true,
    id: null
    //centreSoin: "/api/centre_soins/1"
  }
  test='';
  typeUsers;
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private userTypeService: UserTypeService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.userTypeService.persist(this.myUserType).subscribe(data =>{
      this.typeUsers = [data,...this.typeUsers];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.userTypeService.findAll().subscribe(data =>{
      this.typeUsers = data;
      this.loadedData = true;
      console.log('typeUsers', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssuserType',this.myUserType)
    this.userTypeService.update(this.myUserType).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(userType){
    let initialState = {
      userType: userType
    }
    this.bsModalRef = this.modalService.show(SaisieUserTypeComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(userType){
    this.editForm =false;
    console.log('edit', userType)
     this.myUserType = {
      id: userType.id,
      libelle: userType.libelle
    }
  }*/
  //modifier un acte
  edit(userType){
    //this.loadedData = false;
    this.callModal(userType);
    console.log('acte',userType)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.userType;
      if(list){
          this.typeUsers.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }else{
        this.bsModalRef.hide();
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      console.log('res',res)
      if(res.userType){
        this.typeUsers = [res.userType, ...this.typeUsers];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }else{
        this.bsModalRef.hide();
      }
    });
  }

  delete(userType){
    console.log('unite',userType)
    this.userTypeService.delete(userType.id).subscribe(data =>{
      this.typeUsers =this.typeUsers.filter(data =>data.id !=userType.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }


}
