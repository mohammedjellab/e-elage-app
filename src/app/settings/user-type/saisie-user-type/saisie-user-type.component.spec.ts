import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieUserTypeComponent } from './saisie-user-type.component';

describe('SaisieUserTypeComponent', () => {
  let component: SaisieUserTypeComponent;
  let fixture: ComponentFixture<SaisieUserTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieUserTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
