import { UserTypeService } from 'src/app/services/user-type.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { UserType } from 'src/app/models/userType';
@Component({
  selector: 'app-saisie-user-type',
  templateUrl: './saisie-user-type.component.html',
  styleUrls: ['./saisie-user-type.component.scss']
})
export class SaisieUserTypeComponent implements OnInit {

  myUserType: UserType ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  userTypes;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() userType;
  user;
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];

  constructor(private userTypeService: UserTypeService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyUserType(this.userType);
    console.log('userTypes', this.userType);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.userTypeService.persist(this.myUserType).subscribe(data =>{
      this.event.emit({userType: data});
    },err =>{

    });
  }


  update(){
    console.log('ssssalle',this.myUserType)
    this.userTypeService.update(this.myUserType).subscribe(data =>{
      this.event.emit({userType: data});
    }, err =>{

    });
  }
  remplirMyUserType(f){
    this.myUserType = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.userType){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyUserType(userType){
    console.log('edit', userType)
    if(userType){
      this.myUserType = {
        id: userType.id,
        libelle: userType.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myUserType = {
        libelle: ''
      }
    }
  }

  //fermer modal
  closeModal(){
    this.event.emit({userType: null});
  }
  edit(userType){
    this.editForm =false;
    console.log('edit', userType)
     this.myUserType = {
      id: userType.id,
      libelle: userType.libelle
    }
  }
}
