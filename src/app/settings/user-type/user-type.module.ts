import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSpinKitModule } from 'ng-spin-kit';
import { UserTypeRoutingModule } from './user-type-routing-module';
import { UserTypeComponent } from './user-type.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    UserTypeRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgSpinKitModule,
  ],
  exports: [
    UserTypeComponent
  ],
  declarations: [
    UserTypeComponent
  ],
  providers: [
  ],
})
export class UserTypeModule { }