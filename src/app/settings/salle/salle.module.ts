import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SalleComponent } from './salle.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SalleRoutingModule } from './salle-routing-module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgSpinKitModule } from 'ng-spin-kit';



@NgModule({
  imports: [
    CommonModule,
    SalleRoutingModule,
    FormsModule,
    FontAwesomeModule,
    PerfectScrollbarModule,
    NgSpinKitModule
  ],
  exports: [
    SalleComponent
  ],
  declarations: [
    SalleComponent
  ],
  providers: [
  ],
})
export class SalleModule { }
