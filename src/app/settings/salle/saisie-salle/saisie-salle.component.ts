import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Salle } from 'src/app/models/salle';
import { SalleService } from 'src/app/services/salle.service';

@Component({
  selector: 'app-saisie-salle',
  templateUrl: './saisie-salle.component.html',
  styleUrls: ['./saisie-salle.component.scss']
})
export class SaisieSalleComponent implements OnInit {

 
  mySalle: Salle ={
    libelle: '',
    id: null,
    centreSoin: null
  }
  salles;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() salle;
  user;
  constructor(private salleService: SalleService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initMySalle(this.salle);
    console.log('salles', this.salle);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.salleService.persist(this.mySalle).subscribe(data =>{
      this.event.emit({salle: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({salle: null});
  }
  findAll(){
    this.salleService.findAll().subscribe(data =>{
      this.salles = data['hydra:member'];
    }, err =>{

    })
  }
  update(){
    console.log('ssssalle',this.mySalle)
    this.salleService.update(this.mySalle).subscribe(data =>{
      this.event.emit({salle: data});
    }, err =>{

    });
  }
  remplirMySalle(f){
    this.mySalle = {
      libelle: f.value.libelle,
      id: f.value.id,
      centreSoin: this.user.centreSoin
    }
    if(this.salle){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initMySalle(salle){
    console.log('edit', salle)
    if(salle){
      this.mySalle = {
        id: salle.id,
        libelle: salle.libelle,
        centreSoin: this.user.centreSoin
      }
    }else{
      this.mySalle = {
        libelle: ''
      }
    }
  }

  show(salle){
    this.editForm =false;
    this.salleService.showPerso(salle.id).subscribe( data =>{
      //this.mySalle = data;
      console.log('salle',data)
    },err =>{

    });
  }
  edit(salle){
    this.editForm =false;
    console.log('edit', salle)
     this.mySalle = {
      id: salle.id,
      libelle: salle.libelle
    }
  }
}
