import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieSalleComponent } from './saisie-salle.component';

describe('SaisieSalleComponent', () => {
  let component: SaisieSalleComponent;
  let fixture: ComponentFixture<SaisieSalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieSalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieSalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
