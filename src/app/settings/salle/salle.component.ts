import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { SalleService } from './../../services/salle.service';
import { Component, OnInit } from '@angular/core';
import { Salle } from 'src/app/models/salle';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieSalleComponent } from './saisie-salle/saisie-salle.component';

@Component({
  selector: 'app-salle',
  templateUrl: './salle.component.html',
  styleUrls: ['./salle.component.scss']
})
export class SalleComponent implements OnInit {
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  mySalle: Salle ={
    libelle: '',
    id: null
  }
  salles;
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private salleService: SalleService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.salleService.persist(this.mySalle).subscribe(data =>{
      this.salles = [data,...this.salles];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.salleService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.salles = data;
      this.loadedData = true;
      console.log('salles', data);
    }, err =>{

    })
  }
  update(){
    console.log('ssssalle',this.mySalle)
    this.salleService.update(this.mySalle).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  show(salle){
    this.editForm =false;
    this.salleService.showPerso(salle.id).subscribe( data =>{
      //this.mySalle = data;
      console.log('salle',data)
    },err =>{

    });
  }
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(salle){
    let initialState = {
      salle: salle
    }
    this.bsModalRef = this.modalService.show(SaisieSalleComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(salle){
    this.editForm =false;
    console.log('edit', salle)
     this.mySalle = {
      id: salle.id,
      libelle: salle.libelle
    }
  }*/
  //modifier un acte
  edit(salle){
    //this.loadedData = false;
    this.callModal(salle);
    console.log('acte',salle)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.salle;
      if(list){
          this.salles.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              //this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.salles = [res.salle, ...this.salles];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(salle){
    console.log('unite',salle)
    this.salleService.delete(salle.id).subscribe(data =>{
      this.salles =this.salles.filter(data =>data.id !=salle.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }

}
