import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FileUploader } from 'ng2-file-upload';
import { DefaultService } from './../../services/default.service';
import { CentreSoinService } from './../../services/centre-soin.service';
import { CentreSoin } from './../../models/centreSoin';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieCentreSoinComponent } from './saisie-centre-soin/saisie-centre-soin.component';
const uploadAPI = 'http://localhost:4000/api/upload';
@Component({
  selector: 'app-centre-soin',
  templateUrl: './centre-soin.component.html',
  styleUrls: ['./centre-soin.component.scss']
})
export class CentreSoinComponent implements OnInit {
  faPlus=faPlus;
  myCentreSoin: CentreSoin ={
    etablissement: '',
    code: '',
    active: true,
    activite: '',
    tel: '',
    adresse: '',
    etatConvention: '',
    ice: '',
    inpe: '',
    logo: ''
  }
  faTrashAlt = faTrashAlt;
  centreSoins;
  user;
  uploader: FileUploader = new FileUploader({url: uploadAPI,itemAlias: 'file'})
  bsModalRef: BsModalRef;
  editForm: boolean = true;
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  loadedData: boolean = false;
  constructor(
    private centreSoinService: CentreSoinService,
    private defaultService: DefaultService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.getUser();
    this.findAll();
  }

   //recuperer l'utilisateur connecté
   getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  upload(){
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) =>{
      console.log('FileUpload:uploaded successfully:', item, status, response);
      alert('Your file has been uploaded successfully');
    }
  }
  persist(){
    console.log('ddd','persist')

    //this.deleteAttributeEmpty(this.myCentreSoin);
    this.centreSoinService.persist(this.myCentreSoin).subscribe( data=>{
      this.centreSoins = [data, ...this.centreSoins];
    }, err =>{

    });
  }

  findAll(){
    this.centreSoinService.findAll().subscribe(data =>{
      this.centreSoins = data;
      this.loadedData = true;
      console.log('ddd',data)
    }, err =>{

    });
  }

  deleteAttributeEmpty(arr){
    $.each(arr, function(key,value){
      if(value ==="" || value ===null){
        console.log('eee',arr[key])
        delete arr[key];
      }
    });
  }

    //modifier un acte
  edit(centre){
    //this.loadedData = false;
    this.callModal(centre);
    this.bsModalRef.content.event.subscribe(list => {
      list = list.centre;
      if(list){
          this.centreSoins.filter(data => {
            if(list.id == data.id){
              data.etablissement = list.etablissement
              data.code = list.code
              data.activite = list.activite
              data.ice = list.ice
              data.inpe = list.inpe
              data.adresse = list.adresse
              this.bsModalRef.hide();
              //this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }
  //appeller modal qui contient la page saisie type consultation
  callModal(centre){
    let initialState = {
      centre: centre
    }
    this.bsModalRef = this.modalService.show(SaisieCentreSoinComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }
  //ajouter un acte
  add(){
    console.log('add');
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.centreSoins = [res.centre, ...this.centreSoins];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(centre){
    console.log('unite',centre)
    this.centreSoinService.delete(centre.id).subscribe(data =>{
      this.centreSoins =this.centreSoins.filter(data =>data.id !=centre.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }
}
