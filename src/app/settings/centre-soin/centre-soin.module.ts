import { CentreSoinRoutingModule } from './centre-soin-routing-module';
import { CentreSoinComponent } from './centre-soin.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSpinKitModule } from 'ng-spin-kit';

@NgModule({
  imports: [
    CommonModule,
    CentreSoinRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule
  ],
  exports: [
    CentreSoinComponent
  ],
  declarations: [
    CentreSoinComponent
  ],
  providers: [
  ],
})
export class CentreSoinModule { }