import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieCentreSoinComponent } from './saisie-centre-soin.component';

describe('SaisieCentreSoinComponent', () => {
  let component: SaisieCentreSoinComponent;
  let fixture: ComponentFixture<SaisieCentreSoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieCentreSoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieCentreSoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
