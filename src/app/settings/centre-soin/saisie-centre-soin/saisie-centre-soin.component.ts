import { DefaultService } from './../../../services/default.service';
import { CentreSoinService } from './../../../services/centre-soin.service';
import { CentreSoin } from './../../../models/centreSoin';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FileUploader } from 'ng2-file-upload';

import { Component, EventEmitter, Input, OnInit } from '@angular/core';
const uploadAPI = 'http://localhost:4000/api/upload';
@Component({
  selector: 'app-saisie-centre-soin',
  templateUrl: './saisie-centre-soin.component.html',
  styleUrls: ['./saisie-centre-soin.component.scss']
})
export class SaisieCentreSoinComponent implements OnInit {

  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  myCentreSoin: CentreSoin ={
    etablissement: '',
    code: '',
    active: true,
    activite: '',
    tel: '',
    adresse: '',
    etatConvention: '',
    ice: '',
    inpe: '',
    logo: ''
  }
  faTrashAlt = faTrashAlt;
  centreSoins;
  uploader: FileUploader = new FileUploader({url: uploadAPI,itemAlias: 'file'})
  @Input() centre;
  private event:EventEmitter<any> = new EventEmitter();
  user;
  editForm: boolean = true;

  constructor(
    private centreSoinService: CentreSoinService,
    private defaultService: DefaultService
  ) { }
  



 
  ngOnInit() {
    this.getUser();
    this.initMySalle(this.centre);
    console.log('salles', this.centre);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  upload(){
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) =>{
      console.log('FileUpload:uploaded successfully:', item, status, response);
      alert('Your file has been uploaded successfully');
    }
  }



  deleteAttributeEmpty(arr){
    $.each(arr, function(key,value){
      if(value ==="" || value ===null){
        console.log('eee',arr[key])
        delete arr[key];
      }
    });
  }
  ////////////////////////////////////
  
 
  //fermer modal
  closeModal(){
    this.event.emit({centre: null});
  }

  persist(){
    this.centreSoinService.persist(this.myCentreSoin).subscribe(data =>{
      this.event.emit({centre: data});
    },err =>{

    });
  }


  
  update(){
    console.log('ssssalle',this.myCentreSoin)
    this.centreSoinService.update(this.myCentreSoin).subscribe(data =>{
      this.event.emit({centre: data});
    }, err =>{

    });
  }
  remplirMyCentre(f){
    console.log('')
    this.myCentreSoin = {
        id: f.value.id,
        etablissement: f.value.etablissement,
        code: f.value.code,
        active: f.value.active,
        activite: f.value.activite,
        tel: f.value.tel,
        adresse: f.value.adresse,
        etatConvention: f.value.etatConvention,
        ice: f.value.ice,
        inpe: f.value.inpe,
        logo: f.value.logo,
    }
    if(this.centre){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initMySalle(centre){
    console.log('edit', centre)
    if(centre){
      this.myCentreSoin = {
        id: centre.id,
        etablissement: centre.etablissement,
        code: centre.code,
        active: centre.active,
        activite: centre.activite,
        tel: centre.tel,
        adresse: centre.adresse,
        etatConvention: centre.etatConvention,
        ice: centre.ice,
        inpe: centre.inpe,
        logo: centre.logo,
      }
    }else{
      this.myCentreSoin = {
        etablissement: '',
        code: '',
        active: false,
        activite: '',
        tel: '',
        adresse: '',
        etatConvention: '',
        ice: '',
        inpe: '',
        logo: '',
      }
    }
  }

  

}
