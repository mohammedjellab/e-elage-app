import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentreSoinComponent } from './centre-soin.component';

describe('CentreSoinComponent', () => {
  let component: CentreSoinComponent;
  let fixture: ComponentFixture<CentreSoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentreSoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentreSoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
