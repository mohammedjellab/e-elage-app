import { NgSpinKitModule } from 'ng-spin-kit';
import { MotifConsultationRoutingModule } from './motif-consultation-routing-module';
import { MotifConsultationComponent } from './motif-consultation.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    MotifConsultationRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule

  ],
  exports: [
    MotifConsultationComponent
  ],
  declarations: [
    MotifConsultationComponent
  ],
  providers: [
  ],
})
export class MotifConsultationModule { }