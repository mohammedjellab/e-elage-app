import { MotifConsultationService } from './../../services/motif-consultation.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MotifConsultation } from 'src/app/models/motifConsultation';
import { SaisieMotifConsultationComponent } from './saisie-motif-consultation/saisie-motif-consultation.component';

@Component({
  selector: 'app-motif-consultation',
  templateUrl: './motif-consultation.component.html',
  styleUrls: ['./motif-consultation.component.scss']
})
export class MotifConsultationComponent implements OnInit {

  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  myMotif: MotifConsultation ={
    libelle: '',
    id: null
  }
  motifs;
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private motifConsultationService: MotifConsultationService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAllByCentre();

  }
  persist(){
    this.motifConsultationService.persist(this.myMotif).subscribe(data =>{
      this.motifs = [data,...this.motifs];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAllByCentre(){
    this.motifConsultationService.findAll().subscribe(data =>{
      this.motifs = data;
      this.loadedData = true;
      console.log('motifs', data);
    }, err =>{

    })
  }
 


  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(motif){
    let initialState = {
      motif: motif
    }
    console.log('Initial State ------ ', initialState);
    this.bsModalRef = this.modalService.show(SaisieMotifConsultationComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(motifs){
    this.editForm =false;
    console.log('edit', motifs)
     this.myMotif = {
      id: motifs.id,
      libelle: motifs.libelle
    }
  }*/
  //modifier un acte
  edit(motif){
    //this.loadedData = false;
    this.callModal(motif);
    console.log('acte',motif)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.motif;
      if(list){
          this.motifs.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.motifs = [res.motif, ...this.motifs];
        console.log('------------ Add Motif ---------------');
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(motifs){
    console.log('unite',motifs)
    this.motifConsultationService.delete(motifs.id).subscribe(data =>{
      this.motifs =this.motifs.filter(data =>data.id !=motifs.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }

}
