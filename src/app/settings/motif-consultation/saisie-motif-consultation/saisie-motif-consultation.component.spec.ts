import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieMotifConsultationComponent } from './saisie-motif-consultation.component';

describe('SaisieMotifConsultationComponent', () => {
  let component: SaisieMotifConsultationComponent;
  let fixture: ComponentFixture<SaisieMotifConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieMotifConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieMotifConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
