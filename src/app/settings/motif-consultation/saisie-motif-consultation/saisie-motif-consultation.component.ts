import { MotifConsultationService } from './../../../services/motif-consultation.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { MotifConsultation } from 'src/app/models/motifConsultation';

@Component({
  selector: 'app-saisie-motif-consultation',
  templateUrl: './saisie-motif-consultation.component.html',
  styleUrls: ['./saisie-motif-consultation.component.scss']
})
export class SaisieMotifConsultationComponent implements OnInit {

 
  myMotif: MotifConsultation ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  motifs;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() motif;
  user;
  constructor(private motifConsultationService: MotifConsultationService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initMySalle(this.motif);
    console.log('motifs', this.motif);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.motifConsultationService.persist(this.myMotif).subscribe(data =>{
      console.log('---------- Persist Motif -------------');
      this.event.emit({motif: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({motif: null});
    console.log('-------------- Close Modal -------------');
  }
  findAll(){
    this.motifConsultationService.findAll().subscribe(data =>{
      this.motifs = data['hydra:member'];
    }, err =>{

    })
  }
  update(){
    console.log('sssmotif',this.myMotif)
    this.motifConsultationService.update(this.myMotif).subscribe(data =>{
      this.event.emit({motif: data});
      console.log('------------- Update Motif -------------');
    }, err =>{

    });
  }
  remplirMyMotif(f){
    this.myMotif = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.motif){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initMySalle(motif){
    console.log('edit', motif)
    if(motif){
      this.myMotif = {
        id: motif.id,
        libelle: motif.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myMotif = {
        libelle: ''
      }
    }
  }


  edit(motif){
    this.editForm =false;
    console.log('edit', motif)
     this.myMotif = {
      id: motif.id,
      libelle: motif.libelle
    }
  }
}
