import { ActeRoutingModule } from './acte-routing-module';
import { ActeComponent } from './acte.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  imports: [
    CommonModule,
    ActeRoutingModule,
    FormsModule,
    Ng2SearchPipeModule,
  ],
  exports: [
    ActeComponent
  ],
  declarations: [
    ActeComponent
  ],
  providers: [
  ],
})
export class ActeModule { }