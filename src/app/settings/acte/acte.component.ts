import { LienService } from './../../services/lien.service';
import { FamilleActeService } from './../../services/famille-acte.service';
import { DureeService } from './../../services/duree.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Acte } from './../../models/acte';
import { ActeService } from './../../services/acte.service';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-acte',
  templateUrl: './acte.component.html',
  styleUrls: ['./acte.component.scss']
})
export class ActeComponent implements OnInit {

  prefix = {
    "speciaite": "/api/specialites/",
    "users": "/api/users/",
    "centreSoin": "/api/centre_soins/1",
    "ville":"/api/villes/",
    "pays": "/api/pays/",
    "familleActe": "/api/famille_actes/"
  };

  myActe: Acte ={
    libelle: '',
    active: true,
    prix: null,
    centreSoin: this.prefix.centreSoin,
    couleur: '',
    duree: null,
    familleActe: ''
  }
  familles$: Observable<Object>;
  private searchTerms = new Subject<string>();
  actes;
  durees;
  heroes = [
    { id: 11, name: 'Mr. Nice', country: 'India' },
    { id: 12, name: 'Narco', country: 'USA' },
    { id: 13, name: 'Bombasto', country: 'UK' },
    { id: 14, name: 'Celeritas', country: 'Canada' },
    { id: 15, name: 'Magneta', country: 'Russia' },
    { id: 16, name: 'RubberMan', country: 'China' },
    { id: 17, name: 'Dynama', country: 'Germany' },
    { id: 18, name: 'Dr IQ', country: 'Hong Kong' },
    { id: 19, name: 'Magma', country: 'South Africa' },
    { id: 20, name: 'Tornado', country: 'Sri Lanka' },
  ];
  dtOptions: DataTables.Settings = {};
  user;
  selectedFamille;
  settings = {
    columns: {
        id: {
            title: 'ID'
        },
        name: {
            title: 'Full Name'
        },
        username: {
            title: 'User Name'
        },
        email: {
            title: 'Email'
        }
    }
  };

  data = [
    {
        id: 1,
        name: "Leanne Graham",
        username: "Bret",
        email: "Sincere@april.biz"
    },
    // ... other rows here
    {
        id: 11,
        name: "Nicholas DuBuque",
        username: "Nicholas.Stanton",
        email: "Rey.Padberg@rosamond.biz"
    }
  ];
  searchText;
  constructor(private acteService: ActeService,
              private router: Router,
              private lienService:LienService,
              private dureeService: DureeService,
              private familleActeService: FamilleActeService,
              private route: ActivatedRoute) { }

  ngOnInit(){
    //this.lienService.removePadding();

    this.searchFami();
    this.getDurree();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
    this.route.queryParams.subscribe(params =>{
      this.user = params;
    })

    this.getAllActeWithFamille();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 20,
     
    }
  }
  searchFamille(term: string): void {
    console.log('test',term)
    if(term){
      this.searchTerms.next(term);
    }
  }

  searchFami(){
    console.log('searchFamille');
    this.familles$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(
        (term: string) =>this.familleActeService.searchFamille(term)
      )
    );
  }

  persist(){
    //this.myActe.prix = +this.myActe.prix;
    this.myActe.duree = +this.myActe.duree;
    this.acteService.persist(this.myActe).subscribe(acte =>{
      this.actes=[acte,...this.actes];
      console.log('actes',acte);

    });
  }

  /*findAllActive(){
    this.acteService.findAllActive().subscribe(acte =>{
      this.actes=acte['hydra:member'];
      //this.ambulances =[med,...this.ambulances];

      console.log('actes',this.actes);
    })
  }*/
  getDurree(){
    this.dureeService.findAll().subscribe(data =>{
      this.durees = data['hydra:member'];
      console.log('this.durees ',this.durees );
      
    }, err =>{});
  }
 
  selectFamille(famille){
    this.myActe.familleActe = this.prefix.familleActe+famille.id
    this.selectedFamille = famille.libelle;
    this.clearFamilles();
  }

  clearFamilles(){
    this.familles$ = null;
  }
  getAllActeWithFamille(){
    this.acteService.joinWithFamille().subscribe(acte =>{
      this.actes=acte;
      //this.ambulances =[med,...this.ambulances];

      console.log('actes',acte);
    })
  }
  
}
