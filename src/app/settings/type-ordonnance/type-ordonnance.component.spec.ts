import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeOrdonnanceComponent } from './type-ordonnance.component';

describe('TypeOrdonnanceComponent', () => {
  let component: TypeOrdonnanceComponent;
  let fixture: ComponentFixture<TypeOrdonnanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeOrdonnanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeOrdonnanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
