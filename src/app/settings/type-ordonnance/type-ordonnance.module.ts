import { TypeOrdonnanceRoutingModule } from './type-ordonnance-routing-module';
import { TypeOrdonnanceComponent } from './type-ordonnance.component';
import { AlertModule } from 'ngx-bootstrap';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    TypeOrdonnanceRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
  ],
  exports: [
    TypeOrdonnanceComponent
  ],
  declarations: [
    TypeOrdonnanceComponent
  ],
  providers: [
  ],
})
export class TypeOrdonnanceModule { }