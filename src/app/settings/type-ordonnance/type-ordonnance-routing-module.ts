import { TypeOrdonnanceComponent } from './type-ordonnance.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'type-ordonnance', component: TypeOrdonnanceComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeOrdonnanceRoutingModule { }