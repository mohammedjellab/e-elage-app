import { AgendaConfigService } from './../../services/agenda-config.service';
import { Component, OnInit } from '@angular/core';
import { AgendaConfig } from 'src/app/models/agendaConfig';

@Component({
  selector: 'app-agenda-config',
  templateUrl: './agenda-config.component.html',
  styleUrls: ['./agenda-config.component.scss']
})
export class AgendaConfigComponent implements OnInit {

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'la configuration a été modifié avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet unité déjà existe',
      show: false
    },
    { 
      type: 'info',
      msg: 'la configuration  a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'la configuration  a été supprimé avec succès.',
      show: false
    }
  ];

  myConfig: AgendaConfig = {
    id: null,
    firstDay: null,
    zoom: null,
    modeAffichage: null,
    heureOuverture: null,
    heureDebutOuvrable: null,
    heureFermeture: null,
    heureFinOuvrable: null,
    granularite: null,
  }
  config;
  editForm: boolean = true;
  loadedData: boolean = false;
  savedData: boolean = true;
  user;
  constructor(private agendaConfigService: AgendaConfigService) { 

  }

  ngOnInit() {
    this.getUser();
    this.findAgendaConfig();
    
  }
  activateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  desactivateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  
  /*persist(){
    this.desactivateLoader();
    this.constanteMedicalService.persist(this.myConstante).subscribe(data =>{
      this.constantes = [data, ...this.constantes];
      this.activateLoader();
      this.initMyConstante();
      this.defaultAlerts[0].show = true;
    },err =>{
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.defaultAlerts[1].show = true;
      }
    });
  }
  initMyConstante(){
    this.myConfig = {
      id: null,
      firstDay: '',
      zoom: null,
      modeAffichage: '',
      heureOverture: null,
      heureDebutOuvrable: null,
      heureFermeture: null,
      granularite: null,
      heureFinOuvrable: null,
    }
  }*/
  findAgendaConfig(){
    this.agendaConfigService.findByCentreSoin(this.user.centreSoin).subscribe(data =>{
      this.myConfig = data;
      this.loadedData = true;
      console.log('constantes', data);
    }, err =>{

    })
  }



  remplirConfig(f){
    this.myConfig = {
      firstDay: f.value.firstDay,
      modeAffichage: f.value.modeAffichage,
      id: f.value.id,
      heureDebutOuvrable: f.value.heureDebutOuvrable,
      heureFermeture: f.value.heureFermeture,
      granularite: f.value.granularite,
      heureOuverture: f.value.heureOuverture,
      zoom: f.value.zoom,
      heureFinOuvrable: f.value.heureFinOuvrable
    }

      this.update();
    
  }

  edit(unite){
    console.log('edit',unite)
    //this.myConstante = unite;
    //console.log('edit',this.myConstante.id)
    this.editForm =false;
  }
  update(){
    this.desactivateLoader();
    console.log('eeeeee update',this.myConfig);
    this.agendaConfigService.update(this.myConfig).subscribe(data =>{
      this.desactivateLoader();
      this.defaultAlerts[2].show = true;
      this.editForm = true;
    },err =>{

    })
  }

  


}
