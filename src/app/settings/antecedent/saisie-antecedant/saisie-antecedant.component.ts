import { AntecedentService } from './../../../services/antecedent.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Antecedent } from 'src/app/models/antecedent';

@Component({
  selector: 'app-saisie-antecedant',
  templateUrl: './saisie-antecedant.component.html',
  styleUrls: ['./saisie-antecedant.component.scss']
})
export class SaisieAntecedantComponent implements OnInit {
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  myAntecedent: Antecedent ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() antecedant;
  user;
  constructor(private antecedentService: AntecedentService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyMutuelle(this.antecedant);
    console.log('antecedant', this.antecedant);


    
  }
  //fermer modal
  closeModal(){
    this.event.emit({antecedant: null});
  }
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.antecedentService.persist(this.myAntecedent).subscribe(data =>{
      this.event.emit({antecedant: data});
    },err =>{

    });
  }


  update(){
    console.log('ssssalle',this.myAntecedent)
    this.antecedentService.update(this.myAntecedent).subscribe(data =>{
      this.event.emit({antecedant: data});
    }, err =>{

    });
  }
  remplirMyAntecedent(f){
    this.myAntecedent = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.antecedant){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyMutuelle(antecedant){
    console.log('edit', antecedant)
    if(antecedant){
      this.myAntecedent = {
        id: antecedant.id,
        libelle: antecedant.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myAntecedent = {
        libelle: ''
      }
    }
  }

  
  edit(antecedant){
    this.editForm =false;
    console.log('edit', antecedant)
     this.myAntecedent = {
      id: antecedant.id,
      libelle: antecedant.libelle
    }
  }

}
