import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieAntecedantComponent } from './saisie-antecedant.component';

describe('SaisieAntecedantComponent', () => {
  let component: SaisieAntecedantComponent;
  let fixture: ComponentFixture<SaisieAntecedantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieAntecedantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieAntecedantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
