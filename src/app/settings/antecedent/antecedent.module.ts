import { NgSpinKitModule } from 'ng-spin-kit';
import { AntecedentRoutingModule } from './antecedent-routing-module';
import { AntecedentComponent } from './antecedent.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    AntecedentRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule
  ],
  exports: [
    AntecedentComponent
  ],
  declarations: [
    AntecedentComponent
  ],
  providers: [
  ],
})
export class AntecedentModule { }