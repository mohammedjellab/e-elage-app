import { AntecedentService } from './../../services/antecedent.service';
import { Antecedent } from './../../models/antecedent';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieAntecedantComponent } from './saisie-antecedant/saisie-antecedant.component';

@Component({
  selector: 'app-antecedent',
  templateUrl: './antecedent.component.html',
  styleUrls: ['./antecedent.component.scss']
})
export class AntecedentComponent implements OnInit {

  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  myAntecedant: Antecedent ={
    libelle: '',
    id: null
  }
  antecedants;
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private antecedentService: AntecedentService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.antecedentService.persist(this.myAntecedant).subscribe(data =>{
      this.antecedants = [data,...this.antecedants];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.antecedentService.findAllAntecedent().subscribe(data =>{
      this.antecedants = data;
      this.loadedData = true;
      console.log('antecedants', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssantecedant',this.myAntecedant)
    this.antecedentService.update(this.myAntecedant).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(antecedant){
    let initialState = {
      antecedant: antecedant
    }
    this.bsModalRef = this.modalService.show(SaisieAntecedantComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(antecedant){
    this.editForm =false;
    console.log('edit', antecedant)
     this.myAntecedant = {
      id: antecedant.id,
      libelle: antecedant.libelle
    }
  }*/
  //modifier un acte
  edit(antecedant){
    //this.loadedData = false;
    this.callModal(antecedant);
    console.log('acte',antecedant)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.antecedant;
      if(list){
          this.antecedants.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        console.log('res.antecedant'+res,res.antecedant)
        this.antecedants = [res.antecedant, ...this.antecedants];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(antecedant){
    console.log('unite',antecedant)
    this.antecedentService.delete(antecedant.id).subscribe(data =>{
      this.antecedants =this.antecedants.filter(data =>data.id !=antecedant.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }




}
