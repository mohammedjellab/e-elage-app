import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieTypeConsultationComponent } from './saisie-type-consultation.component';

describe('SaisieTypeConsultationComponent', () => {
  let component: SaisieTypeConsultationComponent;
  let fixture: ComponentFixture<SaisieTypeConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieTypeConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieTypeConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
