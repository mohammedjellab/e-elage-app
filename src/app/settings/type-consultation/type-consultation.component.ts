import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { faFolderOpen, faTrashAlt, faAddressCard } from '@fortawesome/free-regular-svg-icons';
import { faPalette, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FamilleActeService } from './../../services/famille-acte.service';
import { DureeService } from './../../services/duree.service';
import { TypeConsultationService } from './../../services/type-consultation.service';
import {  TypeConsultationGlobal } from '../../models/typeConsultationGlobal';
import { Component, OnInit } from '@angular/core';
import { TypeConsultationLocal } from 'src/app/models/typeConsultationLocal';
import { SaisieTypeConsultationComponent } from './saisie-type-consultation/saisie-type-consultation.component';

@Component({
  selector: 'app-type-consultation',
  templateUrl: './type-consultation.component.html',
  styleUrls: ['./type-consultation.component.scss']
})
export class TypeConsultationComponent implements OnInit {

  faPallette = faPalette;
  faFolderOpen = faFolderOpen;
  faTrashAlt=faTrashAlt;
  faPlus=faPlus
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];

  myTypeConsultationGlobal: TypeConsultationGlobal ={
    id_global:null,
    libelle: '',


  }
  myTypeConsultationLocal: TypeConsultationLocal ={
    id_local:null,
    id_global:null,
    active: true,
    tarif: null,
    centreSoin: null,
    couleur: '',
    duree: null,
    familleActe: null

  }
  constantes;
  editForm: boolean = true;
  loadedData: boolean = false;
  savedData: boolean = true;
  unites;
  durees;
  familles;
  user;

  colors =[
    {
      name: 'color-1',
      color: '#FF5C58'
    },
    {
      name: 'color-2',
      color: '#664E88'
    },
    {
      name: 'color-3',
      color: '#008d8b'

    },
    {
      name: 'color-4',
      color: '#FFB319'

    },
    {
      name: 'color-5',
      color: '#EB92BE'
    },
    {
      name: 'color-6',
      color: '#C36839'
    },
    {
      name: 'color-7',
      color: '#cb6d6d'
    },
    {
      name: 'color-8',
      color: '#7C83FD'
    },
    {
      name: 'color-9',
      color: '#B590CA'
    },
    {
      name: 'color-10',
      color: '#0A81AB'
    },
    {
      name: 'color-11',
      color: '#B590CA'
    },
    {
      name: 'color-12',
      color: '#0A81AB'
    },
    {
      name: 'color-13',
      color: '#926E6F'
    },
    {
      name: 'color-14',
      color: '#CE1F6A'
    },
    {
      name: 'color-15',
      color: '#8fac00'
    },
    {
      name: 'color-16',
      color: '#0E49B5'
    },
    {
      name: 'color-17',
      color: '#e44343'
    },
    {
      name: 'color-18',
      color: '#664E88'
    },
    {
      name: 'color-19',
      color: '#09015F'
    },
    {
      name: 'color-20',
      color: '#EB5E0B'
    },
    {
      name: 'color-21',
      color: '#440A67'
    },
    {
      name: 'color-22',
      color: '#71c075'
    },
    {
      name: 'color-23',
      color: '#7e7e81'
    },
    {
      name: 'color-24',
      color: '#025955'
    },
    {
      name: 'color-25',
      color: '#d39f5e'
    },
  ]

  color = {
    name: 'color-25',
    color: '#d39f5e'
  };
  colorPalette: boolean = false;
  bsModalRef: BsModalRef;

  constructor(private typeConsultationService: TypeConsultationService,
              private modalService: BsModalService,
              private dureeService: DureeService, private familleActeService: FamilleActeService) {

  }

  ngOnInit(){
    this.getDurree();
    this.getFamilleActe();
    this.getUser();
    this.findAllByCentre();
  }

  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  activateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  desactivateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  
  getDurree(){
    this.dureeService.findAll().subscribe(data =>{
      this.durees = data['hydra:member'];
      console.log('this.durees ',this.durees );

    }, err =>{});
  }

  getFamilleActe(){
    this.familleActeService.findAllFamilleActe().subscribe(data =>{
      this.familles = data;
      console.log('this.durees ',this.durees );

    }, err =>{});
  }
  
  findTypeConsultationByLibelle(libelle){
    this.typeConsultationService.findTypeConsultationByLibelle(libelle).subscribe(data =>{
      console.log('findTypeConsultationByLibelle',data.id_global)
      this.myTypeConsultationLocal.id_global=data.id_global;
      console.log('findTypeConsultationByLibelle',this.myTypeConsultationLocal)

    })
  }

  
  findAllByCentre(){
    this.typeConsultationService.findAllByCentre(this.user.centreSoin).subscribe(data =>{
      this.constantes = data;
      this.loadedData = true;
      console.log('constantes', data);
    }, err =>{

    })
  }



 

  //modifier un acte
  edit(acte){
    //this.loadedData = false;
    this.callModal(acte);
    this.bsModalRef.content.event.subscribe(list => {
      list = list.acte;
      if(list){
          this.constantes.filter(data => {
            if(list.id_local == data.id_local){
              data.couleur = list.couleur
              data.duree_name = list.duree_name
              data.duree = list.duree
              data.familleActe = list.familleActe
              data.tarif = list.tarif
              this.bsModalRef.hide();
              //this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }else{
        this.bsModalRef.hide();
      }
    });
  }
  //appeller modal qui contient la page saisie type consultation
  callModal(acte){
    let initialState = {
      acte: acte
    }
    this.bsModalRef = this.modalService.show(SaisieTypeConsultationComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }
  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res.acte){
        this.constantes = [res.acte, ...this.constantes];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }else{
        this.bsModalRef.hide();
      }
    });
  }
/*
  updateGlobal(){
    this.desactivateLoader();
    console.log('edit',this.myTypeConsultationLocal)
    this.typeConsultationService.updateGlobal(this.myTypeConsultationGlobal).subscribe(data =>{
      if(data){
         console.log('constantess', data);
        this.myTypeConsultationLocal.id_global=data.id_global;
        this.updateLocal();
        this.activateLoader();
        this.initMyConstante();

      }

    },err =>{
      //si le type existe dans la table global rechercher id de type globale et inserer dans la table locale
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.typeConsultationService.findTypeConsultationByLibelle(this.myTypeConsultationGlobal.libelle).subscribe(data =>{
          this.myTypeConsultationLocal.id_global=data.id_global;
          console.log('errrr', this.myTypeConsultationLocal);
          this.defaultAlerts[1].show = true;
          this.updateLocal();

        })

      }
    });
  }
  persistGlobal(){

    this.desactivateLoader();
    this.typeConsultationService.persistGlobal(this.myTypeConsultationGlobal).subscribe(data =>{
      if(data){
         console.log('constantess', data);
        this.myTypeConsultationLocal.id_global=data.id_global;
        this.persistLocal();
        this.activateLoader();
        this.initMyConstante();
        this.defaultAlerts[0].show = true;
      }

    },err =>{
      //si le type existe dans la table global rechercher id de type globale et inserer dans la table locale
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.typeConsultationService.findTypeConsultationByLibelle(this.myTypeConsultationGlobal.libelle).subscribe(data =>{
          this.myTypeConsultationLocal.id_global=data.id_global;
          console.log('errrr', this.myTypeConsultationLocal);
          this.defaultAlerts[1].show = true;
          this.persistLocal();

        })

      }
    });
  }
  initMyConstante(){
    this.myTypeConsultationGlobal = {
      id_global:null,
      libelle: '',
    }
    this.myTypeConsultationLocal ={
      id_local:null,
      id_global:null,
      active: true,
      tarif: null,
      centreSoin: null,
      couleur: '',
      duree: null,
      familleActe: null
    }
  }
  remplirTypeConsultation(f){
    //this.editForm =true;
    this.myTypeConsultationGlobal = {
      libelle: f.value.libelle,
      id_global: f.value.id_global,
    }
    this.myTypeConsultationLocal = {
      id_global: f.value.id_global,
      id_local: f.value.id_local,
      active: true,
      tarif: f.value.tarif,
      couleur: f.value.couleur,
      duree: f.value.duree,
      familleActe: f.value.familleActe,
      centreSoin: this.user.centreSoin
    }
    //si user a choisi depuis palette et apres je la rénitialise pour
    if(this.colorPalette){
      this.myTypeConsultationLocal.couleur = this.color.color;
    }
    this.colorPalette = false;
    console.log('constantes', f);
    if(!this.myTypeConsultationLocal.couleur){
      this.myTypeConsultationLocal.couleur = this.color.color
    }
    if(this.editForm){
      this.persistGlobal();
    }else{
      this.updateGlobal();
    }
  }

  updateLocal(){
    this.desactivateLoader();

    this.typeConsultationService.updateLocal(this.myTypeConsultationLocal).subscribe(list =>{
       

      this.activateLoader();
      this.initMyConstante();
      this.defaultAlerts[2].show = true;
    },err =>{
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.defaultAlerts[1].show = true;
      }
    });
  }
  persistLocal(){
    this.desactivateLoader();
    this.typeConsultationService.persistLocal(this.myTypeConsultationLocal).subscribe(data =>{
      this.constantes = [data, ...this.constantes];
      this.activateLoader();
      this.initMyConstante();
      this.defaultAlerts[0].show = true;
    },err =>{
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.defaultAlerts[1].show = true;
      }
    });
  }
  choisirColor(color){
    this.colorPalette = true;
    console.log('this.user',color);
    this.color = color;

  }*/
  delete(type){
    console.log('unite',type)
    this.typeConsultationService.deleteLocal(type.id_local).subscribe(data =>{
      this.constantes =this.constantes.filter(data =>data.id !=type.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }




}
