import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieHolidayComponent } from './saisie-holiday.component';

describe('SaisieHolidayComponent', () => {
  let component: SaisieHolidayComponent;
  let fixture: ComponentFixture<SaisieHolidayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieHolidayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieHolidayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
