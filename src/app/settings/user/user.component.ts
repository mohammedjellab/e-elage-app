import { User } from './../../models/User';
import { UserService } from './../../services/user.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { SalleService } from './../../services/salle.service';
import { Component, OnInit } from '@angular/core';
import { Salle } from 'src/app/models/salle';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  MyUser: User ={
    typeUser: '',
    specialite: '',
    lastname: '',
    firstname: '',
    tel: '',
    adresse: '',
    password: '',
    username: '',
    email: '',
    centreSoin: '',
    cin: ''
  }
  users;
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private userService: UserService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.userService.persist(this.MyUser).subscribe(data =>{
      this.users = [data,...this.users];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.userService.findALL().subscribe(data =>{
      this.users = data;
      this.loadedData = true;
      console.log('users', data);
    }, err =>{

    })
  }

  /*update(){
    console.log('ssssalle',this.MyUser)
    this.userService.update(this.MyUser).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }*/

  


  //appeller modal qui contient la page saisie type consultation
  callModal(salle){
    let initialState = {
      salle: salle
    }
    //this.bsModalRef = this.modalService.show(SaisieSalleComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(salle){
    this.editForm =false;
    console.log('edit', salle)
     this.MyUser = {
      id: salle.id,
      libelle: salle.libelle
    }
  }*/
  //modifier un acte
  edit(salle){
    this.loadedData = false;
    this.callModal(salle);
    console.log('acte',salle)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.salle;
      if(list){
          this.users.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle

              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.users = [res.salle, ...this.users];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  /*delete(salle){
    console.log('unite',salle)
    this.userService.delete(salle.id).subscribe(data =>{
      this.users =this.users.filter(data =>data.id !=salle.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }*/
}
