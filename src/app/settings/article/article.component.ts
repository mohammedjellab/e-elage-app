import { SalleService } from './../../services/salle.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  salles;
  constructor(private salleService: SalleService) { }

  ngOnInit() {
    this.getPosts().then((response)=>{
      console.log('response', response);

    }).catch((err)=>{
      console.log('err', err);

    });
    
  }

  getPosts(){
    return new Promise((resolve,reject)=>{
      this.salleService.getPosts().subscribe(data =>{
        
        //this.loadedData = true;
        console.log('salles', data[0].id);
        this.getUsers(data[0].id)
        resolve (data);
      }, err =>{
        reject(err)
      })
    })
   
  }

  getUsers(user){
    this.salleService.getUsers(user).subscribe(data =>{
      this.salles = data;
      //this.loadedData = true;
      console.log('salles', data);
    }, err =>{

    })
  }

  

}
