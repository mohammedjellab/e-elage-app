import { PaysRoutingModule } from './pays-routing-module';
import { PaysComponent } from './pays.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    PaysRoutingModule,
    FormsModule
  ],
  exports: [
    PaysComponent
  ],
  declarations: [
    PaysComponent
  ],
  providers: [
  ],
})
export class PaysModule { }