import { ModeleConsultation } from './../../models/modeleConsultation';
import { ModeleConsulatationService } from './../../services/modele-consulatation.service';
import { Component, OnInit, ViewChild } from '@angular/core';
//import { CKEditorComponent } from 'ng2-ckeditor';

@Component({
  selector: 'app-modele-consultation',
  templateUrl: './modele-consultation.component.html',
  styleUrls: ['./modele-consultation.component.scss']
})
export class ModeleConsultationComponent  {

  name = 'ng2-ckeditor';
  ckeConfig ;//CKEDITOR.config;
  mycontent: string;
  log: string = '';
  event;
  myModele:ModeleConsultation = {
    body: null,
    centreSoin: null,
    couleur: null,
    police: null,
    taille: null,
    isGras: null,
    format: null
  }
  centreSoin: number = 1;
  @ViewChild("myckeditor",null) ckeditor;
  modeleConsultation;
  constructor(private modeleConsulatationService: ModeleConsulatationService) {
    this.mycontent = `<p>My html content</p>`;
  }

  ngOnInit() {
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true,
      removePlugins: 'exportpdf'
    };
    this.getModeleConsultationByCentreSoin();
  }

  onChange($event: any): void {
    console.log("onChange");
    //this.log += new Date() + "<br />";
  }

  onPaste($event: any): void {
    console.log("onPaste");
    //this.log += new Date() + "<br />";
  }


  onFocus(event){
    this.event = event;
    var selection = event.editor.getSelection();
    var ranges = selection.getRanges();
    var range = ranges[0];

    var newRange = event.editor.createRange();

    var moveToEnd = true;
    newRange.moveToElementEditablePosition(range.endContainer, moveToEnd);

    var newRanges = [newRange];
    selection.selectRanges(newRanges);

    //
  }

  addDate(){
    this.event.editor.insertHtml("$date");
  }
  remplirModele(f){
    console.log('myckeditor',f)
    this.myModele={
      body: f.value.myckeditor,
      centreSoin: this.centreSoin,
      couleur: f.value.couleur,
      police: f.value.police,
      taille: f.value.taille,
      isGras: f.value.isGras,
      format: f.value.format
    }
  }
  saveModele(f){
    this.modeleConsulatationService.update(this.myModele).subscribe(data =>{

    })
  }

  getModeleConsultationByCentreSoin(){
    this.modeleConsulatationService.findByCentreSoin(this.centreSoin).subscribe(data=>{
      this.modeleConsultation = data
    })
  }
  
}
