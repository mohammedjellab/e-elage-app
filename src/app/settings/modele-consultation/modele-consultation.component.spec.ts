import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeleConsultationComponent } from './modele-consultation.component';

describe('ModeleConsultationComponent', () => {
  let component: ModeleConsultationComponent;
  let fixture: ComponentFixture<ModeleConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeleConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeleConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
