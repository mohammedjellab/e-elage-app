import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstanteMedicalComponent } from './constante-medical.component';

describe('ConstanteMedicalComponent', () => {
  let component: ConstanteMedicalComponent;
  let fixture: ComponentFixture<ConstanteMedicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstanteMedicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstanteMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
