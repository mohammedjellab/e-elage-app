import { UniteService } from './../../services/unite.service';
import { Component, OnInit } from '@angular/core';
import { ConstanteMedical } from 'src/app/models/constanteMedical';
import { ConstanteMedicalService } from 'src/app/services/constante-medical.service';

@Component({
  selector: 'app-constante-medical',
  templateUrl: './constante-medical.component.html',
  styleUrls: ['./constante-medical.component.scss']
})
export class ConstanteMedicalComponent implements OnInit {

  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre unité a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet unité déjà existe',
      show: false
    },
    { 
      type: 'info',
      msg: 'Votre unité a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre unité a été supprimé avec succès.',
      show: false
    }
  ];

  myConstante: ConstanteMedical ={
    id: null,
    libelle: '',
    abreviation: '',
    couleur: '',
    borneMax: null,
    borneMin: null,
    unite: null,
  }
  constantes;
  editForm: boolean = true;
  
  loadedData: boolean = false;
  savedData: boolean = true;
  unites;
  constructor(private constanteMedicalService: ConstanteMedicalService,private uniteService: UniteService) { 

  }

  ngOnInit() {
    this.findAllUnite();
    this.findAll();
    
  }
  activateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  desactivateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  
  persist(){
    this.desactivateLoader();
    this.constanteMedicalService.persist(this.myConstante).subscribe(data =>{
      this.constantes = [data, ...this.constantes];
      this.activateLoader();
      this.initMyConstante();
      this.defaultAlerts[0].show = true;
    },err =>{
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.defaultAlerts[1].show = true;
      }
    });
  }
  initMyConstante(){
    this.myConstante = {
      id: null,
      libelle: '',
      abreviation: '',
      couleur: '',
      borneMax: null,
      borneMin: null,
      unite: null,
    }
  }
  findAll(){
    this.constanteMedicalService.findAllConstante().subscribe(data =>{
      this.constantes = data;
      this.loadedData = true;
      console.log('constantes', data);
    }, err =>{

    })
  }



  persistConstante(f){
    this.myConstante = {
      abreviation: f.value.abreviation,
      libelle: f.value.libelle,
      id: f.value.id,
      couleur: f.value.couleur,
      borneMax: f.value.borneMax,
      borneMin: f.value.borneMin,
      unite: f.value.unite
    }
    if(this.editForm){
      this.persist();
    }else{
      this.update();
    }
  }

  edit(constante){
    this.myConstante = constante;
    this.editForm =false;
  }
  update(){
    this.desactivateLoader();
    console.log('eeeeee update',this.myConstante);
    this.constanteMedicalService.update(this.myConstante).subscribe(data =>{
      //this.findAll();
      console.log('datadatadata',data.id)
      this.desactivateLoader();
      this.defaultAlerts[2].show = true;
      this.editForm = true;
      this.initMyConstante();
    },err =>{
//
    })
  }
  
  getElementByIndex(){
    const targetIdx = this.constantes.map(item => item.id).indexOf(14);
    //this.createnode[targetIdx] = this.updateWith;
  }

  delete(unite){
    console.log('unite',unite)
    this.constanteMedicalService.delete(unite.id).subscribe(data =>{
      this.constantes =this.constantes.filter(data =>data.id !=unite.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }

  findAllUnite(){
    this.uniteService.findAll().subscribe(data =>{
      this.unites = data;
      this.loadedData = true;
      console.log('unites', data);
    }, err =>{

    })
  }
}
