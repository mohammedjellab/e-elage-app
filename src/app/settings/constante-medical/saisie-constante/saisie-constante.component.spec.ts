import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieConstanteComponent } from './saisie-constante.component';

describe('SaisieConstanteComponent', () => {
  let component: SaisieConstanteComponent;
  let fixture: ComponentFixture<SaisieConstanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieConstanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieConstanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
