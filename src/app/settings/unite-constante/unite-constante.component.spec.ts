import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniteConstanteComponent } from './unite-constante.component';

describe('UniteConstanteComponent', () => {
  let component: UniteConstanteComponent;
  let fixture: ComponentFixture<UniteConstanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniteConstanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniteConstanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
