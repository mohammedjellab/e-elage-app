import { UniteService } from './../../services/unite.service';
import { Component, OnInit } from '@angular/core';
import { Unite } from './../../models/unite';
@Component({
  selector: 'app-unite-constante',
  templateUrl: './unite-constante.component.html',
  styleUrls: ['./unite-constante.component.scss']
})
export class UniteConstanteComponent implements OnInit {
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre unité a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet unité déjà existe',
      show: false
    },
    { 
      type: 'info',
      msg: 'Votre unité a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre unité a été supprimé avec succès.',
      show: false
    }
  ];

  myConstante: Unite ={
    id: null,
    libelle: '',
    abreviation: ''
  }
  unites;
  editForm: boolean = true;
  
  loadedData: boolean = false;
  savedData: boolean = true;
  constructor(private uniteService: UniteService) { 

  }

  ngOnInit() {
    this.findAll();
    
  }
  activateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  desactivateLoader(){
    this.savedData=true;
    this.loadedData = true;
  }
  
  persist(){
    this.desactivateLoader();
    this.uniteService.persist(this.myConstante).subscribe(data =>{
      this.unites = [data, ...this.unites];
      this.activateLoader();
      this.initMyUnite();
      this.defaultAlerts[0].show = true;
    },err =>{
      this.activateLoader();
      if(err.error.class=="Doctrine\\DBAL\\Exception\\UniqueConstraintViolationException"){
        this.defaultAlerts[1].show = true;
      }
    });
  }
  initMyUnite(){
    this.myConstante = {
      id: null,
      libelle: '',
      abreviation: ''
    }
  }
  findAll(){
    this.uniteService.findAll().subscribe(data =>{
      this.unites = data;
      this.loadedData = true;
      console.log('unites', data);
    }, err =>{

    })
  }



  persistConstante(f){
    this.myConstante = {
      abreviation: f.value.abreviation,
      libelle: f.value.libelle,
      id: f.value.id
    }
    if(this.editForm){
      this.persist();
    }else{
      this.update();
    }
  }

  edit(unite){
    console.log('edit',unite)
    this.myConstante = unite;
    console.log('edit',this.myConstante.id)
    this.editForm =false;
  }
  update(){
    this.desactivateLoader();
    console.log('eeeeee update',this.myConstante);
    this.uniteService.update(this.myConstante).subscribe(data =>{
      this.findAll();
      this.desactivateLoader();
      this.defaultAlerts[2].show = true;
      this.editForm = true;
      this.initMyUnite();
    },err =>{

    })
  }

  delete(unite){
    console.log('unite',unite)
    this.uniteService.delete(unite.id).subscribe(data =>{
      this.unites =this.unites.filter(data =>data.id !=unite.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }

}
