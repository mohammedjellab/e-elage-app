import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComorbiditeComponent } from './comorbidite.component';

describe('ComorbiditeComponent', () => {
  let component: ComorbiditeComponent;
  let fixture: ComponentFixture<ComorbiditeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComorbiditeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComorbiditeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
