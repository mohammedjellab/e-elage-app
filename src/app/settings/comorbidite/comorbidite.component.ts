import { ComorbiditeService } from './../../services/comorbidite.service';
import { Comorbidite } from './../../models/comorbidite';
import { MutuelleService } from './../../services/mutuelle.service';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Mutuelle } from './../../models/mutuelle';
//import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SaisieComorbiditeComponent } from './saisie-comorbidite/saisie-comorbidite.component';


@Component({
  selector: 'app-comorbidite',
  templateUrl: './comorbidite.component.html',
  styleUrls: ['./comorbidite.component.scss']
})
export class ComorbiditeComponent implements OnInit {

  comorbidites;
  
  faPlus=faPlus;
  faSearch=faSearch;
  faTrashAlt=faTrashAlt

  myComorbidite: Comorbidite ={
    libelle: '',
    id: null
  }
  editForm: boolean = true;
  bsModalRef: BsModalRef;
  loadedData: boolean = false;
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  user;
  constructor(private comorbiditeService: ComorbiditeService, private modalService: BsModalService) {

  }

  ngOnInit(){
    this.getUser();
    this.findAll();

  }
  persist(){
    this.comorbiditeService.persist(this.myComorbidite).subscribe(data =>{
      this.comorbidites = [data,...this.comorbidites];
    },err =>{

    });
  }
  //recuperer l'utilisateur connecté
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }

  findAll(){
    this.comorbiditeService.findAll().subscribe(data =>{
      this.comorbidites = data;
      this.loadedData = true;
      console.log('comorbidites', data);
    }, err =>{

    })
  }
  update(){
    console.log('sssmutuelle',this.myComorbidite)
    this.comorbiditeService.update(this.myComorbidite).subscribe(data =>{
      this.findAll();
    }, err =>{

    });
  }

  
  
  
  //appeller modal qui contient la page saisie type consultation
  callModal(mutuelle){
    let initialState = {
      mutuelle: mutuelle
    }
    this.bsModalRef = this.modalService.show(SaisieComorbiditeComponent,{initialState, class: 'modal-xl'});
    this.editForm =false;
  }

  /*edit(mutuelle){
    this.editForm =false;
    console.log('edit', mutuelle)
     this.myComorbidite = {
      id: mutuelle.id,
      libelle: mutuelle.libelle
    }
  }*/
  //modifier un acte
  edit(mutuelle){
    //this.loadedData = false;
    this.callModal(mutuelle);
    console.log('acte',mutuelle)
    this.bsModalRef.content.event.subscribe(list => {
      list = list.mutuelle;
      if(list){
          this.comorbidites.filter(data => {
            if(list.id == data.id){
              data.libelle = list.libelle
            
              this.bsModalRef.hide();
              this.loadedData = true;
              this.defaultAlerts[2].show = true;
            }
          })
      }
    });
  }

  //ajouter un acte
  add(){
    this.callModal(null);
    this.bsModalRef.content.event.subscribe(res => {
      if(res){
        this.comorbidites = [res.mutuelle, ...this.comorbidites];
        this.bsModalRef.hide();
        this.defaultAlerts[0].show = true;
      }
    });
  }

  delete(mutuelle){
    console.log('unite',mutuelle)
    this.comorbiditeService.delete(mutuelle.id).subscribe(data =>{
      this.comorbidites =this.comorbidites.filter(data =>data.id !=mutuelle.id)
      this.defaultAlerts[3].show = true;
    },err =>{

    })
  }



}
