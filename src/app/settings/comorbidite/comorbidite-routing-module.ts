import { ComorbiditeComponent } from './comorbidite.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'comorbidite', component: ComorbiditeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComorbiditeRoutingModule { }