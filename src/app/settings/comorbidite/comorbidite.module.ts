import { NgSpinKitModule } from 'ng-spin-kit';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ComorbiditeComponent } from './comorbidite.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComorbiditeRoutingModule } from './comorbidite-routing-module';



@NgModule({
  imports: [
    CommonModule,
    ComorbiditeRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgSpinKitModule
  ],
  exports: [
    ComorbiditeComponent
  ],
  declarations: [
    ComorbiditeComponent
  ],
  providers: [
  ],
})
export class ComorbiditeModule { }