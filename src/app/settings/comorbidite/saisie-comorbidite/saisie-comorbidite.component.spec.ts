import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieComorbiditeComponent } from './saisie-comorbidite.component';

describe('SaisieComorbiditeComponent', () => {
  let component: SaisieComorbiditeComponent;
  let fixture: ComponentFixture<SaisieComorbiditeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieComorbiditeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieComorbiditeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
