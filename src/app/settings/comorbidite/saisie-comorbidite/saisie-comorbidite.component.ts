import { ComorbiditeService } from './../../../services/comorbidite.service';
import { Comorbidite } from './../../../models/comorbidite';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-saisie-comorbidite',
  templateUrl: './saisie-comorbidite.component.html',
  styleUrls: ['./saisie-comorbidite.component.scss']
})
export class SaisieComorbiditeComponent implements OnInit {
  defaultAlerts: any[] = [
    {
      type: 'success',
      msg: 'Votre acte a été créer avec succès.',
      show: false
    },
    {
      type: 'danger',
      msg: 'Cet acte déjà existe',
      show: false
    },
    {
      type: 'info',
      msg: 'Votre acte a été modifié avec succès.',
      show: false
    },
    {
      type: 'warning',
      msg: 'Votre acte a été supprimé avec succès.',
      show: false
    }
  ];
  myComorbidite: Comorbidite ={
    libelle: '',
    id: null,
    //centreSoin: null
  }
  mutuelles;
  editForm: boolean = true;
  public event: EventEmitter<any> = new EventEmitter();
  @Input() mutuelle;
  user;
  constructor(private comorbiditeService: ComorbiditeService) { 

  }

  ngOnInit() {
    this.getUser();
    this.initmyComorbidite(this.mutuelle);
    console.log('mutuelles', this.mutuelle);


    
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  persist(){
    this.comorbiditeService.persist(this.myComorbidite).subscribe(data =>{
      this.event.emit({mutuelle: data});
    },err =>{

    });
  }
  //fermer modal
  closeModal(){
    this.event.emit({mutuelle: null});
  }
  findAll(){
    this.comorbiditeService.findAll().subscribe(data =>{
      this.mutuelles = data['hydra:member'];
    }, err =>{

    })
  }
  update(){
    console.log('ssssalle',this.myComorbidite)
    this.comorbiditeService.update(this.myComorbidite).subscribe(data =>{
      this.event.emit({mutuelle: data});
    }, err =>{

    });
  }
  remplirMyComorbidite(f){
    this.myComorbidite = {
      libelle: f.value.libelle,
      id: f.value.id,
      //centreSoin: this.user.centreSoin
    }
    if(this.mutuelle){
      this.update();
    }else{
      this.persist();
    }
  }
  
  initmyComorbidite(mutuelle){
    console.log('edit', mutuelle)
    if(mutuelle){
      this.myComorbidite = {
        id: mutuelle.id,
        libelle: mutuelle.libelle,
        //centreSoin: this.user.centreSoin
      }
    }else{
      this.myComorbidite = {
        libelle: ''
      }
    }
  }

  
  edit(mutuelle){
    this.editForm =false;
    console.log('edit', mutuelle)
     this.myComorbidite = {
      id: mutuelle.id,
      libelle: mutuelle.libelle
    }
  }


}
