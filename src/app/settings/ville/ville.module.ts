import { VilleRoutingModule } from './ville-routing-module';
import { VilleComponent } from './ville.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    VilleRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,

  ],
  exports: [
    VilleComponent
  ],
  declarations: [
    VilleComponent
  ],
  providers: [
  ],
})
export class VilleModule { }