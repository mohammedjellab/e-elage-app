import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modele-consult',
  templateUrl: './modele-consult.component.html',
  styleUrls: ['./modele-consult.component.scss']
})
export class ModeleConsultComponent implements OnInit {

  name = 'Angular 4';
  config = {
   uiColor: '#ffffff',
   toolbarGroups: [
   { name: 'document', groups: ['mode', 'document', 'doctools'] },
   { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
   { name: 'paragraph', groups: ['align'] }],
   skin: 'kama',
   resize_enabled: false,
   sourceAreaTabSize: 8,
   allowedContent: true,
   extraPlugins: 'sourcedialog',
   removePlugins: 'elementspath,save,magicline',
   colorButton_foreStyle: {
      element: 'font',
      attributes: { 'color': '#(color)' }
   },
   height: 188,
   removeDialogTabs: 'image:advanced;link:advanced',
   removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
   format_tags: 'p;h1;h2;h3;pre;div'
}
event;
  constructor() { }

  ngOnInit() {
  }

  onFocus(event){
    this.event = event;
    var selection = event.editor.getSelection();
    var ranges = selection.getRanges();
    var range = ranges[0];

    var newRange = event.editor.createRange();

    var moveToEnd = true;
    newRange.moveToElementEditablePosition(range.endContainer, moveToEnd);

    var newRanges = [newRange];
    selection.selectRanges(newRanges);

    //
  }

  add(){
    this.event.editor.insertHtml("<span>Hello World!</span>");
  }
}
