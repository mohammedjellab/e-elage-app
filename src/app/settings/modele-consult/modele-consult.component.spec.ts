import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeleConsultComponent } from './modele-consult.component';

describe('ModeleConsultComponent', () => {
  let component: ModeleConsultComponent;
  let fixture: ComponentFixture<ModeleConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeleConsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeleConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
