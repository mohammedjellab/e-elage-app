import { ExamenComplementaire } from './../../models/examenComplementaire';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'examen-complementaire', component: ExamenComplementaire },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenComplemantaireRoutingModule { }