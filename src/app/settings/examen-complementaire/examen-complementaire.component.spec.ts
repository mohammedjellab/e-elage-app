import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenComplementaireComponent } from './examen-complementaire.component';

describe('ExamenComplementaireComponent', () => {
  let component: ExamenComplementaireComponent;
  let fixture: ComponentFixture<ExamenComplementaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenComplementaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenComplementaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
