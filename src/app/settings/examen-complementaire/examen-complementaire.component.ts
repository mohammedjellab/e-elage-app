import { ExamenComplementaire } from '../../models/examenComplementaire';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ExamenComplementaireService } from 'src/app/services/examen-complementaire.service';

@Component({
  selector: 'app-examen-complementaire',
  templateUrl: './examen-complementaire.component.html',
  styleUrls: ['./examen-complementaire.component.scss']
})
export class ExamenComplementaireComponent implements OnInit {

  myExamen: ExamenComplementaire ={
    libelle: ''
  }
  examenComplementaires;
  editForm: boolean = true;
  constructor(private examenComplementaireService: ExamenComplementaireService) { 

  }

  ngOnInit(){
    this.findAll();
  }
  findAll(){
    this.examenComplementaireService.findAll().subscribe(data =>{
      console.log('hydr',data)
      this.examenComplementaires =data['hydra:member']
    })
  }
  remplirData(form: NgForm){
    console.log('hydr',form)

    this.myExamen = {
      libelle: form.value.libelle
    }
    this.persist();
  }
  persist(){
    console.log('this.myMutuelle',this.myExamen)

    this.examenComplementaireService.persist(this.myExamen).subscribe( data =>{
      this.examenComplementaires = [data, ...this.examenComplementaires]
    },err =>{

    })
  }

  edit(mutuelle){
    this.myExamen = mutuelle;
    this.editForm =false;
  }
  update(){
    console.log('eeeeee update');
    this.examenComplementaireService.update(this.myExamen).subscribe(data =>{
      this.examenComplementaires = data['hydra:member'];
    },err =>{

    })
  }



}
