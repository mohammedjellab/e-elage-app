import { ExamenComplementaireComponent } from './examen-complementaire.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExamenComplemantaireRoutingModule } from './examen-complemantaire-routing-module';



@NgModule({
  imports: [
    CommonModule,
    ExamenComplemantaireRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,

  ],
  exports: [
    ExamenComplementaireComponent
  ],
  declarations: [
    ExamenComplementaireComponent
  ],
  providers: [
  ],
})
export class ExamenComplementaireModule { }