import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieExamenComplementaieComponent } from './saisie-examen-complementaie.component';

describe('SaisieExamenComplementaieComponent', () => {
  let component: SaisieExamenComplementaieComponent;
  let fixture: ComponentFixture<SaisieExamenComplementaieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieExamenComplementaieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieExamenComplementaieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
