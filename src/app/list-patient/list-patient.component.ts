import { LienService } from './../services/lien.service';
import { ThemeOptions } from './../theme-options';
import { NgForm } from '@angular/forms';
import { DefaultService } from './../services/default.service';
import { UserService } from './../services/user.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { DossierPatientService } from './../services/dossier-patient.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
@Component({
  selector: 'app-list-patient',
  templateUrl: './list-patient.component.html',
  styleUrls: ['./list-patient.component.scss']
})
export class ListPatientComponent implements OnInit {

  patients;
  patient;
  medecins;
  searchs = {
    lastname: '',
    firstname: '',
    date: '',
    praticien: '',
    telephone: null,
    num_dossier: null,
    centre: 1
  };
  private searchTerms = new Subject<string>();
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private dossierPatientService: DossierPatientService,
              private userService: UserService,
              private defaultService: DefaultService,
              public globals: ThemeOptions,
              private lienService: LienService,
              private router: Router) { }

  ngOnInit() {
    //this.searchPat();
    this.getMedecinByEtablissment();
    //this.findAll();
    this.initSearch();
    console.log('dattata',this.getNowDate());
  }

  initSearch(){
    this.searchs = {
      lastname: '',
      firstname: '',
      date: null,//this.getNowDate(),
      praticien: '',
      telephone: null,
      num_dossier: null,
      centre: 1
    }
  }

  /*findAll(){
    this.dossierPatientService.findAll().subscribe( data =>{
      this.patients = data;
      console.log('data',data)
    },err =>{

    });
  }*/
  /*search(term: string): void {
    console.log('searchPat',term)

      this.searchTerms.next(term);

  }

  searchPat(){
    console.log('searchPat')
      this.patients$ =  this.searchTerms.pipe(
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),

        // ignore new term if same as previous term
        distinctUntilChanged(),

        // switch to new search observable each time the term changes
        switchMap((term: string) => this.dossierPatientService.searchPatient(term)),
      );

  }*/
  remplirSearch(f: NgForm){

    this.searchs = {
      lastname: f.value.lastname,
      firstname: f.value.firstname,
      date: f.value.date,
      praticien: f.value.praticien,
      telephone: f.value.telephone,
      num_dossier: f.value.num_dossier,
      centre: 1
    }
    console.log('remplirSearch',f)
    console.log('remplirSearch',this.searchs)
    this.searchPatient();
  }
  searchPatient(){
    this.dossierPatientService.searchPatients(this.searchs).subscribe(data =>{
      this.patients = data
      console.log('data',data);
    }, err =>{

    })
  }

  goToPage(patient,page){
    this.lienService.goToPage(patient,page);
  }

  selectPatient(patient,page){
    console.log('patient',patient)
    this.patient = "patient";
    this.router.navigate(['rdv'], { queryParams: patient});
    //this.router.navigate(['fiche-patient'], { queryParams: patient});
  }
  selectPatients(patient){
    //this.selectPatient = patient.lpatient_id;
    this.selectPatient = patient;
    this.event.emit({patient: patient})
    console.log('patient',this.selectPatient)

    /*this.infoComplementaireAgenda ={
      firstname: patient.firstname,
      lastname: patient.lastname,
      civilite: patient.civilite,
      patient: patient.lpatient_id

    }*/

    //lgModal.hide()
    //this.router.navigate(['rdv'], { queryParams: patient});
    //this.router.navigate(['fiche-patient'], { queryParams: patient});
  }

  getMedecinByEtablissment(){
    this.userService.getMedecinByEtablissment(1).subscribe(data =>{
      this.medecins = data
      console.log('data',data);
    }, err =>{

    })
  }

  //recuperer  date
  getNowDate(){
    let d = this.defaultService.getNowDate();
    console.log('getNowDate',d)
    return d;
  }



}
