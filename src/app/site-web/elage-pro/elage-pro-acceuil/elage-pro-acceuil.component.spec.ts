import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElageProAcceuilComponent } from './elage-pro-acceuil.component';

describe('ElageProAcceuilComponent', () => {
  let component: ElageProAcceuilComponent;
  let fixture: ComponentFixture<ElageProAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElageProAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElageProAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
