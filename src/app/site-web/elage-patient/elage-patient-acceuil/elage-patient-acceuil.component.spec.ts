import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElagePatientAcceuilComponent } from './elage-patient-acceuil.component';

describe('ElagePatientAcceuilComponent', () => {
  let component: ElagePatientAcceuilComponent;
  let fixture: ComponentFixture<ElagePatientAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElagePatientAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElagePatientAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
