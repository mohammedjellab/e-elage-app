import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavHeaderElajePatientComponent } from './nav-header-elaje-patient.component';

describe('NavHeaderElajePatientComponent', () => {
  let component: NavHeaderElajePatientComponent;
  let fixture: ComponentFixture<NavHeaderElajePatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavHeaderElajePatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavHeaderElajePatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
