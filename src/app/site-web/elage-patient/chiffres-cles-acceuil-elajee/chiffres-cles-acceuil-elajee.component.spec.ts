import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiffresClesAcceuilElajeeComponent } from './chiffres-cles-acceuil-elajee.component';

describe('ChiffresClesAcceuilElajeeComponent', () => {
  let component: ChiffresClesAcceuilElajeeComponent;
  let fixture: ComponentFixture<ChiffresClesAcceuilElajeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiffresClesAcceuilElajeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiffresClesAcceuilElajeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
