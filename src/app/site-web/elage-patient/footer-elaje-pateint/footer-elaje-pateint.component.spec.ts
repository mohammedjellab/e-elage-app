import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterElajePateintComponent } from './footer-elaje-pateint.component';

describe('FooterElajePateintComponent', () => {
  let component: FooterElajePateintComponent;
  let fixture: ComponentFixture<FooterElajePateintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterElajePateintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterElajePateintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
