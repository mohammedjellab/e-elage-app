import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderAcceuilElajePateintComponent } from './header-acceuil-elaje-pateint.component';

describe('HeaderAcceuilElajePateintComponent', () => {
  let component: HeaderAcceuilElajePateintComponent;
  let fixture: ComponentFixture<HeaderAcceuilElajePateintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderAcceuilElajePateintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderAcceuilElajePateintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
