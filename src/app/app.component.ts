import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet><ng-progress></ng-progress>'
})
export class AppComponent implements OnInit{

  previousUrl: string = null;
  currentUrl: string = null;
  user;
  constructor(private router: Router) { }

  ngOnInit(){
    //console.log('route',this.router.getCurrentNavigation.name);
    //localStorage.setItem('route','');
    let logout = localStorage.getItem('logout');
    let currentUrl = localStorage.getItem('currentUrl');
    /*console.log('currentUrl sto',currentUrl)

    console.log('currentUrl sto',currentUrl)
    if(!this.user){
      this.router.navigate(['pages-register']);
    }else{
    }*/
    //this.router.navigate([currentUrl]);

    this.getLastRoute();
    this.getUser();
  }
  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    //console.log('this.user',this.user);
  }
  getLastRoute(){
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
  ).subscribe((event: NavigationEnd) => {
     this.previousUrl = this.currentUrl;
     this.currentUrl = event.url;
     localStorage.setItem('currentUrl',this.currentUrl);
     //console.log('previousUrl',this.previousUrl)
     //console.log('currentUrl',this.currentUrl)
  });
  }

}
