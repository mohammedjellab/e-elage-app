import { Router } from '@angular/router';
import { UserService } from './../services/user.service';
import { NgForm } from '@angular/forms';
import { DefaultService } from './../services/default.service';
import { AgendaService } from './../services/agenda.service';
import { TypeConsultationService } from './../services/type-consultation.service';
import { SalleService } from './../services/salle.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-rdv',
  templateUrl: './list-rdv.component.html',
  styleUrls: ['./list-rdv.component.scss']
})
export class ListRdvComponent implements OnInit {



  salles ;
  user;
  typeConsultations;
  typeConsultationsNbres;
  centre =1;
  listRdv;
  search = {
    salle:'',
    term: '',
    date: new Date(),
    date2: this.getDateFormat(new Date()),
    praticien: '',
    en_salle: null,
    type_journee: null,
    centre: 1
  }
  medecins;
  loadedData: boolean = false;
  constructor(private salleService: SalleService,
              private agendaService: AgendaService,
              private userService: UserService,
              private defaultService: DefaultService,
              private router: Router,
              private typeConsultationService: TypeConsultationService) { }

  ngOnInit() {
    this.getUser();
    this.getSallesByCentre();
    //this.nbreRdvByTypeConsultation();
    this.initSearch();
    this.getListConsultation();
    this.getMedecinByEtablissment();
  }
  getMedecinByEtablissment(){
    this.userService.getMedecinByEtablissment(1).subscribe(data =>{
      this.medecins = data
      console.log('data',data);
    }, err =>{

    })
  }

  getUser(){
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('this.user',this.user);
  }
  initSearch(){
    this.search = {
      salle:'',
      term: '',
      date: new Date(),
      date2: this.getDateFormat(new Date()),
      praticien: '',
      en_salle: null,
      type_journee: 'matin',
      centre: 1
    }
  }
  remplirSearch(f: NgForm){

    let date: Date;
    date = f.value.date;
    let d2= new Date(date);
    console.log('d',typeof date);
    let d = this.getDateFormat(d2);
    this.search = {
      salle: f.value.salle,
      term: f.value.term,
      date: f.value.date,
      date2: d,
      praticien: f.value.praticien,
      en_salle: f.value.telephone,
      type_journee: this.search.type_journee,
      centre: 1
    }
    let today = new Date();
    console.log('form',f.value)
    
    
    this.getListConsultation();
  }
  getTypeOfDay(value){
    this.listRdv = [];
    this.search.type_journee = value;
    this.getListConsultation();
    console.log('matin',value)
  }

  // recuperer les salles par centre 
  getSallesByCentre(){
    this.salleService.getSallesByCentre(this.search.centre).subscribe(data =>{
      this.salles = data
    })
  }

  //nbre de rdv par type consultation
  nbreRdvByTypeConsultation(){
    this.agendaService.nbreRdvByTypeConsultation(this.search.centre).subscribe(data =>{
      this.typeConsultationsNbres = data;
      console.log('consultat',data)
    });
  }

  //recuperer list consultation par recherche
  getListConsultation(){
    this.agendaService.searchConsultation(this.search).subscribe(data =>{
      this.listRdv = data;
      this.loadedData = true;
    });
  }


  //recuperer  date 
  getNowDate(){
    return this.defaultService.getNowDate();
  }
  getDateFormat(d){
    return this.defaultService.getDateFormat(d);
  }


  
  cancelRdv(){
    
  }

  /*refreshList(f){
    this.remplirSearch(f);
  }*/

  goToPage(value,patient){
    console.log('patient',patient)
    //this.router.navigate(['rdv'], { queryParams: patient});
    if(value=="patient"){
      this.router.navigate(['fiche-patient'], { queryParams: patient});

    }else if(value=="medical"){
      this.router.navigate(['dossier-medical'], { queryParams: patient});

    }
  }

}
