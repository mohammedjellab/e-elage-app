import { ExampleWrapperSimpleComponent } from './../layout-components/example-wrapper-simple/example-wrapper-simple.component';
import { ListRdvRoutingModule } from './list-rdv-routing-module';
import { ListRdvComponent } from './list-rdv.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertModule, TabsModule } from 'ngx-bootstrap';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { CountUpModule } from 'countup.js-angular2';


import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgSpinKitModule } from 'ng-spin-kit';

@NgModule({
  imports: [
    CommonModule,
    ListRdvRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    NgSpinKitModule,
    TabsModule.forRoot(),
    ModalModule,
    BsDatepickerModule,
    CountUpModule
  ],
  exports: [
    ListRdvComponent
  ],
  declarations: [
    ListRdvComponent,
  ],
  providers: [
  ],
})
export class ListRdvModule { }