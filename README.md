<p align="center">
    <a href="https://uifort.com" title="UiFort.com">
        <img width=256px height=256px src="https://demo.uifort.com/github-static-assets/ui-fort-logo.png" alt="UiFort">
    </a>
</p>
<h1 align="center">
    <a href="https://uifort.com/template/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro">Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO</a>
    <br>
    <a href="https://twitter.com/intent/tweet?url=https://uifort.com/template/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro&text=Bamburgh is crafted to perfection making it the most developer friendly &amp; easy to use Bootstrap admin dashboard template.">
        <img src="https://img.shields.io/twitter/url/http/shields.io.svg?style=social" />
    </a>
    <a href="https://twitter.com/uifort1">
        <img src="https://img.shields.io/twitter/follow/uifort1.svg?style=social&label=Follow" />
    </a>
</h1>
<div align="center">

  ![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
  [![Status](https://img.shields.io/badge/status-active-success.svg)]() 
  [![GitHub Issues open](https://img.shields.io/github/issues/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro.svg)](https://github.com/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro/issues)
  [![GitHub issues closed](https://img.shields.io/github/issues-closed-raw/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro.svg?maxAge=2592000)](https://github.com/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro/issues?q=is%3Aissue+is%3Aclosed)
  [![GitHub Pull Requests](https://img.shields.io/github/issues-pr/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro.svg)](https://github.com/uifort/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro/pulls)
  [![License](https://img.shields.io/badge/license-UiFort-blue.svg)](/LICENSE)

</div>

<h5 align="center">350+ custom-made, beautiful components, widgets, pages, dashboards and applications.</h5>

<h4 align="center">It&#39;s the most complete frontend development solution that will help you build powerful &amp; scalable web applications or beautiful presentation websites.</h4>

<br />

<img src="https://demo.uifort.com/github-static-assets/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro.jpg" alt="Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO">

---

<h2 align="center">
    Bamburgh admin dashboard template is available for multiple frameworks
</h2>

<p align="center">Click the buttons below to visit their respective presentation page:</p>

<table align="center">
    <tr>
        <td>
            <a align="center" href="https://uifort.com/template/bamburgh-react-admin-dashboard-reactstrap-pro" title="Click to visit the presentation page for Bamburgh React version">
                <b align="center">Bamburgh for React</b>
                <br><br>
                <img src="https://demo.uifort.com/github-static-assets/framework-logo/react-framework-logo.png" alt="Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO">
                <br>
                <b align="center">Click for details</b>
            </a>
        </td>
        <td>
            <a align="center" href="https://uifort.com/template/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro" title="Click to visit the presentation page for Bamburgh Angular version">
                <b align="center">Bamburgh for Angular</b>
                <br><br>
                <img src="https://demo.uifort.com/github-static-assets/framework-logo/angular-framework-logo.png" alt="Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO">
                <br>
                <b align="center">Click for details</b>
            </a>
        </td>
        <td>
            <b align="center">Bamburgh for Vue.js</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/vue-framework-logo.png" alt="Bamburgh for Vue.js">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td>
            <b align="center">Bamburgh for HTML5/jQuery</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/html-framework-logo.png" alt="Bamburgh for HTML5/jQuery">
            <br>
            <i align="center">Not yet available</i>
        </td>
    </tr>
    <tr>
        <td align="center">
            <b align="center">Bamburgh for Laravel</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/laravel-framework-logo.png" alt="Bamburgh for Laravel">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td align="center">
            <b align="center">Bamburgh for Node.js</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/nodejs-framework-logo.png" alt="Bamburgh for Node.js">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td align="center">
            <b align="center">Bamburgh for ASP.NET</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/asp-framework-logo.png" alt="Bamburgh for ASP.NET">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td align="center">
            <b align="center">Bamburgh for Nuxt.js</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/nuxtjs-framework-logo.png" alt="Bamburgh for Nuxt.js">
            <br>
            <i align="center">Not yet available</i>
        </td>
    </tr>
    <tr>
        <td>
            <b align="center">Bamburgh for React Native</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/react-native-framework-logo.png" alt="Bamburgh for React Native">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td>
            <b align="center">Bamburgh for Flutter</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/flutter-framework-logo.png" alt="Bamburgh for Flutter">
            <br>
            <i align="center">Not yet available</i>
        </td>
        <td align="center">
            <b align="center">Bamburgh for Next.js</b>
            <br><br>
            <img src="https://demo.uifort.com/github-static-assets/framework-logo/nextjs-framework-logo.png" alt="Bamburgh for Next.js">
            <br>
            <i align="center">Not yet available</i>
        </td>
    </tr>
</table>

---

<h2>
    Online Documentation
</h2>

<p>For each of our products we create an extensive documentation so that developers can easily navigate, understand and use all the features that, in this case, Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO has to offer!</p>
<p>To view the available online documentation files please visit the following link:</p>
<b>
    <a href="https://docs.uifort.com/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro-docs" title="Click to view the online documentation for Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO">
        Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO Documentation
    </a>
</b>

---

<h2>
    Live Preview
</h2>

<p>You can view a live preview instance here:</p>
<b>
    <a href="https://demo.uifort.com/bamburgh-angular-admin-dashboard-ngx-bootstrap-pro-demo" title="Click to view the live preview for Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO">
        Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO Live Preview
    </a>
</b>

---

<h2>
    FREE Version
</h2>

<p>Bamburgh admin dashboard template also comes with a slimmed down, FREE version. This contains a limited set of components and features.</p>
<p>Bamburgh admin dashboard template FREE is open source and was released under an <b>MIT license</b>!</p>
<p>
    You can read more details about the FREE version by following the links bellow:
</p>
<b>
    <a href="https://uifort.com/template/bamburgh-angular-admin-dashboard-ngx-bootstrap-free" title="Click to view Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO admin dashboard template FREE presentation page">
        Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO FREE Presentation Page
    </a>
    <br><br>
    <a href="https://demo.uifort.com/bamburgh-angular-admin-dashboard-ngx-bootstrap-free-demo" title="Click to view Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO admin dashboard template FREE live preview">
        Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO FREE Live Preview
    </a>
    <br><br>
    <a href="https://docs.uifort.com/bamburgh-angular-admin-dashboard-ngx-bootstrap-free-docs" title="Click to view Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO admin dashboard template FREE documentation">
        Bamburgh Angular Admin Dashboard with NGX Bootstrap PRO FREE Documentation
    </a>
</b>

---

<h2>
    Browser Support
</h2>
<p>
    We're supporting the last two versions of the following browsers:
</p>
<p>
<img src="https://demo.uifort.com/github-assets/browsers/chrome.png" width="64" height="64"> <img src="https://demo.uifort.com/github-assets/browsers/firefox.png" width="64" height="64"> <img src="https://demo.uifort.com/github-assets/browsers/edge.png" width="64" height="64"> <img src="https://demo.uifort.com/github-assets/browsers/safari.png" width="64" height="64"> <img src="https://demo.uifort.com/github-assets/browsers/opera.png" width="64" height="64">
</p>

---

<h2>
    Support or Questions
</h2>
<p>
    You can contact us either via our contact page ([UiFort Contact](https://uifort.com/contact-us)) or you could chat with us using the integrated chat widget from our live previews or homepage.
</p>
<b>
    <a href="https://uifort.com/support-center" title="Click to view the UiFort Support Center">
        UiFort Support Center
    </a>
</b>
---

<h2>
    Issue Reporting
</h2>

<p>We use GitHub Issues as the official bug tracker for all our products. Here are some advices for anyone who wants to report an issue:</p>

1. Make sure that you are using the latest version of this product. To do that you could check the [CHANGELOG](./CHANGELOG.md).
2. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed.
3. Some issues may be browser specific, so specifying in what browser you encountered the issue might help.

---

### Social Media

<p>Follow us on any of our social media accounts to find out when we release new products or updates.</p>
<p>We send out discount coupons from time to time to our subscribers.</p>
<p><b>Facebook: </b><a href="https:&#x2F;&#x2F;www.facebook.com&#x2F;UiFort">https:&#x2F;&#x2F;www.facebook.com&#x2F;UiFort</a></p>
<p><b>Twitter: </b><a href="https:&#x2F;&#x2F;twitter.com&#x2F;uifort1">https:&#x2F;&#x2F;twitter.com&#x2F;uifort1</a></p>
<p><b>Instagram: </b><a href="https:&#x2F;&#x2F;www.instagram.com&#x2F;uifort1">https:&#x2F;&#x2F;www.instagram.com&#x2F;uifort1</a></p>
<p><b>Discord: </b><a href="https:&#x2F;&#x2F;discord.gg&#x2F;mddFBQX">https:&#x2F;&#x2F;discord.gg&#x2F;mddFBQX</a></p>
<p><b>Dribbble: </b><a href="https:&#x2F;&#x2F;dribbble.com&#x2F;UiFort">https:&#x2F;&#x2F;dribbble.com&#x2F;UiFort</a></p>
<p><b>Github: </b><a href="https:&#x2F;&#x2F;github.com&#x2F;uifort">https:&#x2F;&#x2F;github.com&#x2F;uifort</a></p>

# e-elage-app



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mohammedjellab/e-elage-app.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://gitlab.com/mohammedjellab/e-elage-app/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:3732ee9b9ba1669a5daae9c8a2e6ff10?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


